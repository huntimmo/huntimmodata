package com.huntimmo.data.dto.common;

import com.google.common.base.Function;
import com.huntimmo.data.common.Constantes;
import com.huntimmo.data.util.AmountUtils;


public class MontantDto {
	private double montant;
    private String codeUnite;
    private String devise;

	public MontantDto(double montant, String codeUnite, String devise) {
		super();
		// gestion des cas où les valeurs +/-9.99999999999999E14  et +/-9.99999999999999E11 en retour de BDE --> données non récupérées 
		if(AmountUtils.isAmountNullFromBde(montant)){
			this.montant = Double.NaN;
		}
		else{
			if(codeUnite == null || codeUnite.trim().isEmpty()) {
				this.montant = montant / 1000;
			} else if(Constantes.MONTANT_UNITE_MILLION.equals(codeUnite)) {
				this.montant = montant * 1000;
			} else {
				this.montant = montant;
			}
		}
		this.codeUnite = codeUnite;
		this.devise = devise;
	}

	public MontantDto(double montant, String devise) {
		super();
		this.montant = montant;
		this.devise = devise;
	}

	public MontantDto(double montant) {
		super();
		if(AmountUtils.isAmountNullFromBde(montant)){
			this.montant = Double.NaN;
		}
		else{
			this.montant = montant;
		}
	}

	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public String getCodeUnite() {
		return codeUnite;
	}
	public void setCodeUnite(String codeUnite) {
		this.codeUnite = codeUnite;
	}
	public String getDevise() {
		return devise;
	}
	public void setDevise(String devise) {
		this.devise = devise;
	}

//	/**
//	 * Fonction permettant de créer un objet MontantDto depuis un objet MontantType
//	 */
//	public static Function<MontantType, MontantDto> fromMontantType = new Function<MontantType, MontantDto>() {
//		@Override
//		public MontantDto apply(MontantType input) {
//			return new MontantDto(input.getMontant(), input.getCodeUnite(), input.getDevise());
//		}
//	};
}
