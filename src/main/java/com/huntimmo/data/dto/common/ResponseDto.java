package com.huntimmo.data.dto.common;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author EKW
 * 
 *         API Response Object.
 *
 */
public class ResponseDto<T> {

	// Data of this resource.
	private T data;

	// Response code
	private String code;

	// Message
	private String message;

	public ResponseDto() {
		super();
	}
	
	/**
	 * @param data
	 */
	public ResponseDto(T data) {
		super();
		this.data = data;
	}

	/**
	 * @param data
	 * @param code
	 * @param message
	 */
	public ResponseDto(T data, String code, String message) {
		super();
		this.data = data;
		this.code = code;
		this.message = message;
	}
	
	public ResponseDto(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public ResponseDto(String message) {
		super();
		this.message = message;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}
	
	@JsonIgnore
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
