package com.huntimmo.data.dto.sirene;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Records {

	private Fields fields;

	public Fields getFields() {
		return fields;
	}

	public void setFields(Fields fields) {
		this.fields = fields;
	} 
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
