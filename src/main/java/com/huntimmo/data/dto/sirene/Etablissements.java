package com.huntimmo.data.dto.sirene;

public class Etablissements {

	private String siren;
	private String nic;
	private String siret;
	private String l1_normalisee;
	private String l2_normalisee;
	private String l3_normalisee;
	private String l4_normalisee;
	private String l5_normalisee;
	private String l6_normalisee;
	private String l7_normalisee;
	private String numero_voie;
	private String type_voie;
	private String libelle_voie;
	private String code_postal;
	private String cedex;
	private String region;
	private String libelle_region;
	private String departement;
	private String libelle_commune;
	private String is_siege;
	private String enseigne;
	private String activite_principale;
	private String libelle_activite_principale;
	private String tranche_effectif_salarie;
	private String libelle_tranche_effectif_salarie;
	private String date_creation;
	private String date_debut_activite;
	private String nom_raison_sociale;
	private String nom;
	private String prenom;
	private String civilite;
	private String nic_siege;
	private String categorie_entreprise;
	private String longitude;
	private String latitude;
	private String geo_score;
	private String geo_type;
	private String geo_adresse;
	private String geo_id;
	private String geo_ligne;
	
	public String getSiren() {
		return siren;
	}
	public void setSiren(String siren) {
		this.siren = siren;
	}
	public String getNic() {
		return nic;
	}
	public void setNic(String nic) {
		this.nic = nic;
	}
	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}
	public String getL1_normalisee() {
		return l1_normalisee;
	}
	public void setL1_normalisee(String l1_normalisee) {
		this.l1_normalisee = l1_normalisee;
	}
	public String getL2_normalisee() {
		return l2_normalisee;
	}
	public void setL2_normalisee(String l2_normalisee) {
		this.l2_normalisee = l2_normalisee;
	}
	public String getL3_normalisee() {
		return l3_normalisee;
	}
	public void setL3_normalisee(String l3_normalisee) {
		this.l3_normalisee = l3_normalisee;
	}
	public String getL4_normalisee() {
		return l4_normalisee;
	}
	public void setL4_normalisee(String l4_normalisee) {
		this.l4_normalisee = l4_normalisee;
	}
	public String getL5_normalisee() {
		return l5_normalisee;
	}
	public void setL5_normalisee(String l5_normalisee) {
		this.l5_normalisee = l5_normalisee;
	}
	public String getL6_normalisee() {
		return l6_normalisee;
	}
	public void setL6_normalisee(String l6_normalisee) {
		this.l6_normalisee = l6_normalisee;
	}
	public String getL7_normalisee() {
		return l7_normalisee;
	}
	public void setL7_normalisee(String l7_normalisee) {
		this.l7_normalisee = l7_normalisee;
	}
	public String getNumero_voie() {
		return numero_voie;
	}
	public void setNumero_voie(String numero_voie) {
		this.numero_voie = numero_voie;
	}
	public String getType_voie() {
		return type_voie;
	}
	public void setType_voie(String type_voie) {
		this.type_voie = type_voie;
	}
	public String getLibelle_voie() {
		return libelle_voie;
	}
	public void setLibelle_voie(String libelle_voie) {
		this.libelle_voie = libelle_voie;
	}
	public String getCode_postal() {
		return code_postal;
	}
	public void setCode_postal(String code_postal) {
		this.code_postal = code_postal;
	}
	public String getCedex() {
		return cedex;
	}
	public void setCedex(String cedex) {
		this.cedex = cedex;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getLibelle_region() {
		return libelle_region;
	}
	public void setLibelle_region(String libelle_region) {
		this.libelle_region = libelle_region;
	}
	public String getDepartement() {
		return departement;
	}
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	public String getLibelle_commune() {
		return libelle_commune;
	}
	public void setLibelle_commune(String libelle_commune) {
		this.libelle_commune = libelle_commune;
	}
	public String getIs_siege() {
		return is_siege;
	}
	public void setIs_siege(String is_siege) {
		this.is_siege = is_siege;
	}
	public String getEnseigne() {
		return enseigne;
	}
	public void setEnseigne(String enseigne) {
		this.enseigne = enseigne;
	}
	public String getActivite_principale() {
		return activite_principale;
	}
	public void setActivite_principale(String activite_principale) {
		this.activite_principale = activite_principale;
	}
	public String getLibelle_activite_principale() {
		return libelle_activite_principale;
	}
	public void setLibelle_activite_principale(String libelle_activite_principale) {
		this.libelle_activite_principale = libelle_activite_principale;
	}
	public String getTranche_effectif_salarie() {
		return tranche_effectif_salarie;
	}
	public void setTranche_effectif_salarie(String tranche_effectif_salarie) {
		this.tranche_effectif_salarie = tranche_effectif_salarie;
	}
	public String getLibelle_tranche_effectif_salarie() {
		return libelle_tranche_effectif_salarie;
	}
	public void setLibelle_tranche_effectif_salarie(String libelle_tranche_effectif_salarie) {
		this.libelle_tranche_effectif_salarie = libelle_tranche_effectif_salarie;
	}
	public String getDate_creation() {
		return date_creation;
	}
	public void setDate_creation(String date_creation) {
		this.date_creation = date_creation;
	}
	public String getDate_debut_activite() {
		return date_debut_activite;
	}
	public void setDate_debut_activite(String date_debut_activite) {
		this.date_debut_activite = date_debut_activite;
	}
	public String getNom_raison_sociale() {
		return nom_raison_sociale;
	}
	public void setNom_raison_sociale(String nom_raison_sociale) {
		this.nom_raison_sociale = nom_raison_sociale;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getCivilite() {
		return civilite;
	}
	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}
	public String getNic_siege() {
		return nic_siege;
	}
	public void setNic_siege(String nic_siege) {
		this.nic_siege = nic_siege;
	}
	public String getCategorie_entreprise() {
		return categorie_entreprise;
	}
	public void setCategorie_entreprise(String categorie_entreprise) {
		this.categorie_entreprise = categorie_entreprise;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getGeo_score() {
		return geo_score;
	}
	public void setGeo_score(String geo_score) {
		this.geo_score = geo_score;
	}
	public String getGeo_type() {
		return geo_type;
	}
	public void setGeo_type(String geo_type) {
		this.geo_type = geo_type;
	}
	public String getGeo_adresse() {
		return geo_adresse;
	}
	public void setGeo_adresse(String geo_adresse) {
		this.geo_adresse = geo_adresse;
	}
	public String getGeo_id() {
		return geo_id;
	}
	public void setGeo_id(String geo_id) {
		this.geo_id = geo_id;
	}
	public String getGeo_ligne() {
		return geo_ligne;
	}
	public void setGeo_ligne(String geo_ligne) {
		this.geo_ligne = geo_ligne;
	}
	
	

}
