//  package com.natixis.fd3.dto.sirene;
//
//import org.apache.commons.lang3.builder.ToStringBuilder;
//
//public class Fields {
//
//	private String siren;
//	private String siret;
//	private String denominationunitelegale;
//	private String categoriejuridiqueunitelegale;
//	private String libnj;
//	private String nic;
//	private String activiteprincipaleetablissement;
//	private String adresseetablissement;
//	private String complementadresseetablissement; //pour remplacer l4_normalisee
//	private String complementadresse2etablissement;//pour remplacer l6_normalisee 
//	private String l6_normalisee;
//	private String l7_normalisee;
//	private String codepostaletablissement;
//	private String libellecommuneetablissement;
//	private String classeetablissement;
//	public String getClasseetablissement() {
//		return classeetablissement;
//	}
//
//	public void setclasseetablissement(String classeetablissement) {
//		this.classeetablissement = classeetablissement;
//	}
//	
//	public String getDatecreationetablissement() {
//		return datecreationetablissement;
//	}
//
//	public void setDatecreationetablissement(String datecreationetablissement) {
//		this.datecreationetablissement = datecreationetablissement;
//	}
//
//	private String datecreationetablissement;
//	private String natetab;
//	private String nomunitelegale;
//	private String divisionetablissement;
//
//	public String getSiren() {
//		return siren;
//	}
//
//	public void setSiren(String siren) {
//		this.siren = siren;
//	}
//
//	public String getSiret() {
//		return siret;
//	}
//
//	public void setSiret(String siret) {
//		this.siret = siret;
//	}
//
//	public String getLibnj() {
//		return libnj;
//	}
//
//	public void setLibnj(String libnj) {
//		this.libnj = libnj;
//	}
//
//	public String getNic() {
//		return nic;
//	}
//
//	public void setNic(String nic) {
//		this.nic = nic;
//	}
//
//	public String getL6_normalisee() {
//		return l6_normalisee;
//	}
//
//	public void setL6_normalisee(String l6_normalisee) {
//		this.l6_normalisee = l6_normalisee;
//	}
//
//	/**
//	 * @return the l7_normalisee
//	 */
//	public String getL7_normalisee() {
//		return l7_normalisee;
//	}
//
//	/**
//	 * @param l7_normalisee the l7_normalisee to set
//	 */
//	public void setL7_normalisee(String l7_normalisee) {
//		this.l7_normalisee = l7_normalisee;
//	}
//
//	public String getNatetab() {
//		return natetab;
//	}
//
//	public void setNatetab(String natetab) {
//		this.natetab = natetab;
//	}
//
//
//	public String getDenominationunitelegale() {
//		return denominationunitelegale;
//	}
//
//	public void setDenominationunitelegale(String denominationunitelegale) {
//		this.denominationunitelegale = denominationunitelegale;
//	}
//
//	public String getCategoriejuridiqueunitelegale() {
//		return categoriejuridiqueunitelegale;
//	}
//
//	public void setCategoriejuridiqueunitelegale(String categoriejuridiqueunitelegale) {
//		this.categoriejuridiqueunitelegale = categoriejuridiqueunitelegale;
//	}
//
//	public String getActiviteprincipaleetablissement() {
//		return activiteprincipaleetablissement;
//	}
//
//	public void setActiviteprincipaleetablissement(String activiteprincipaleetablissement) {
//		this.activiteprincipaleetablissement = activiteprincipaleetablissement;
//	}
//
//	public String getAdresseetablissement() {
//		return adresseetablissement;
//	}
//
//	public void setAdresseetablissement(String adresseetablissement) {
//		this.adresseetablissement = adresseetablissement;
//	}
//
//	public String getComplementadresseetablissement() {
//		return complementadresseetablissement;
//	}
//
//	public void setComplementadresseetablissement(String complementadresseetablissement) {
//		this.complementadresseetablissement = complementadresseetablissement;
//	}
//
//	public String getComplementadresse2etablissement() {
//		return complementadresse2etablissement;
//	}
//
//	public void setComplementadresse2etablissement(String complementadresse2etablissement) {
//		this.complementadresse2etablissement = complementadresse2etablissement;
//	}
//
//	public String getCodepostaletablissement() {
//		return codepostaletablissement;
//	}
//
//	public void setCodepostaletablissement(String codepostaletablissement) {
//		this.codepostaletablissement = codepostaletablissement;
//	}
//
//	public String getLibellecommuneetablissement() {
//		return libellecommuneetablissement;
//	}
//
//	public void setLibellecommuneetablissement(String libellecommuneetablissement) {
//		this.libellecommuneetablissement = libellecommuneetablissement;
//	}
//
//	public String getNomunitelegale() {
//		return nomunitelegale;
//	}
//
//	public void setNomunitelegale(String nomunitelegale) {
//		this.nomunitelegale = nomunitelegale;
//	}
//
//	public String getDivisionetablissement() {
//		return divisionetablissement;
//	}
//
//	public void setDivisionetablissement(String divisionetablissement) {
//		this.divisionetablissement = divisionetablissement;
//	}
//	
//	@Override
//	public String toString() {
//		return ToStringBuilder.reflectionToString(this);
//	}
//
//}

package com.huntimmo.data.dto.sirene;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Fields {

	private String siren;
	private String siret;
	private String nomen_long;
	private String nj;
	private String libnj;
	private String nic;
	private String apet700;
	private String l1_normalisee;
	private String l4_normalisee;
	private String l6_normalisee;
	private String l7_normalisee;
	private String codpos;
	private String code_postal;
	private String libcom;
	private String libelle_commune;
	private String departement;
	private String dcren;
	private String natetab;
	private String nom;
	private String libapet;
	private String activite_principale;
	private String libelle_activite_principale;
	private String tranche_effectif_salarie;
	private String nom_raison_sociale;
	private String prenom;
	private String civilite;
	private String categorie_entreprise;
	private String date_creation_entreprise;
	private String longitude;
	private String latitude;
	private String geo_score;
	private String geo_adresse;
	private String geo_id;
	

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSiren() {
		return siren;
	}

	public void setSiren(String siren) {
		this.siren = siren;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public String getNj() {
		return nj;
	}

	public void setNj(String nj) {
		this.nj = nj;
	}

	public String getLibnj() {
		return libnj;
	}

	public void setLibnj(String libnj) {
		this.libnj = libnj;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public String getApet700() {
		return apet700;
	}

	public void setApet700(String apet700) {
		this.apet700 = apet700;
	}

	public String getL1_normalisee() {
		return l1_normalisee;
	}

	public void setL1_normalisee(String l1_normalisee) {
		this.l1_normalisee = l1_normalisee;
	}

	public String getL4_normalisee() {
		return l4_normalisee;
	}

	public void setL4_normalisee(String l4_normalisee) {
		this.l4_normalisee = l4_normalisee;
	}

	public String getL6_normalisee() {
		return l6_normalisee;
	}

	public void setL6_normalisee(String l6_normalisee) {
		this.l6_normalisee = l6_normalisee;
	}

	public String getCodpos() {
		return codpos;
	}

	public void setCodpos(String codpos) {
		this.codpos = codpos;
	}

	public String getLibcom() {
		return libcom;
	}

	public void setLibcom(String libcom) {
		this.libcom = libcom;
	}

	public String getDcren() {
		return dcren;
	}

	public void setDcren(String dcren) {
		this.dcren = dcren;
	}

	public String getNomen_long() {
		return nomen_long;
	}

	public void setNomen_long(String nomen_long) {
		this.nomen_long = nomen_long;
	}

	/**
	 * @return the l7_normalisee
	 */
	public String getL7_normalisee() {
		return l7_normalisee;
	}

	/**
	 * @param l7_normalisee the l7_normalisee to set
	 */
	public void setL7_normalisee(String l7_normalisee) {
		this.l7_normalisee = l7_normalisee;
	}

	public String getNatetab() {
		return natetab;
	}

	public void setNatetab(String natetab) {
		this.natetab = natetab;
	}

	/**
	 * @return the libapet
	 */
	public String getLibapet() {
		return libapet;
	}

	/**
	 * @param libapet the libapet to set
	 */
	public void setLibapet(String libapet) {
		this.libapet = libapet;
	}

	public String getCode_postal() {
		return code_postal;
	}

	public void setCode_postal(String code_postal) {
		this.code_postal = code_postal;
	}

	public String getLibelle_commune() {
		return libelle_commune;
	}

	public void setLibelle_commune(String libelle_commune) {
		this.libelle_commune = libelle_commune;
	}

	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public String getLibelle_activite_principale() {
		return libelle_activite_principale;
	}

	public void setLibelle_activite_principale(String libelle_activite_principale) {
		this.libelle_activite_principale = libelle_activite_principale;
	}

	public String getTranche_effectif_salarie() {
		return tranche_effectif_salarie;
	}

	public void setTranche_effectif_salarie(String tranche_effectif_salarie) {
		this.tranche_effectif_salarie = tranche_effectif_salarie;
	}

	public String getNom_raison_sociale() {
		return nom_raison_sociale;
	}

	public void setNom_raison_sociale(String nom_raison_sociale) {
		this.nom_raison_sociale = nom_raison_sociale;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getCategorie_entreprise() {
		return categorie_entreprise;
	}

	public void setCategorie_entreprise(String categorie_entreprise) {
		this.categorie_entreprise = categorie_entreprise;
	}

	public String getDate_creation_entreprise() {
		return date_creation_entreprise;
	}

	public void setDate_creation_entreprise(String date_creation_entreprise) {
		this.date_creation_entreprise = date_creation_entreprise;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getGeo_score() {
		return geo_score;
	}

	public void setGeo_score(String geo_score) {
		this.geo_score = geo_score;
	}

	public String getGeo_adresse() {
		return geo_adresse;
	}

	public void setGeo_adresse(String geo_adresse) {
		this.geo_adresse = geo_adresse;
	}

	public String getGeo_id() {
		return geo_id;
	}

	public void setGeo_id(String geo_id) {
		this.geo_id = geo_id;
	}


	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getActivite_principale() {
		return activite_principale;
	}

	public void setActivite_principale(String activite_principale) {
		this.activite_principale = activite_principale;
	}


}
