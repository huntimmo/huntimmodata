package com.huntimmo.data.dto.sirene;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class APISireneDto {

	private Integer nhits;

	private List<Records> records = new ArrayList<Records>();
	
	private List<Etablissements> etablissements = new ArrayList<Etablissements>();

	public List<Records> getRecords() {
		return records;
	}

	public void setRecords(List<Records> records) {
		this.records = records;
	}

	public Integer getNhits() {
		return nhits;
	}

	public void setNhits(Integer nhits) {
		this.nhits = nhits;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public List<Etablissements> getEtablissements() {
		return etablissements;
	}

	public void setEtablissements(List<Etablissements> etablissements) {
		this.etablissements = etablissements;
	}
}
