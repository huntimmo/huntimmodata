//  package com.natixis.fd3.dto.sirene;
//
//import org.apache.commons.lang3.builder.ToStringBuilder;
//
//public class Fields {
//
//	private String siren;
//	private String siret;
//	private String denominationunitelegale;
//	private String categoriejuridiqueunitelegale;
//	private String libnj;
//	private String nic;
//	private String activiteprincipaleetablissement;
//	private String adresseetablissement;
//	private String complementadresseetablissement; //pour remplacer l4_normalisee
//	private String complementadresse2etablissement;//pour remplacer l6_normalisee 
//	private String l6_normalisee;
//	private String l7_normalisee;
//	private String codepostaletablissement;
//	private String libellecommuneetablissement;
//	private String classeetablissement;
//	public String getClasseetablissement() {
//		return classeetablissement;
//	}
//
//	public void setclasseetablissement(String classeetablissement) {
//		this.classeetablissement = classeetablissement;
//	}
//	
//	public String getDatecreationetablissement() {
//		return datecreationetablissement;
//	}
//
//	public void setDatecreationetablissement(String datecreationetablissement) {
//		this.datecreationetablissement = datecreationetablissement;
//	}
//
//	private String datecreationetablissement;
//	private String natetab;
//	private String nomunitelegale;
//	private String divisionetablissement;
//
//	public String getSiren() {
//		return siren;
//	}
//
//	public void setSiren(String siren) {
//		this.siren = siren;
//	}
//
//	public String getSiret() {
//		return siret;
//	}
//
//	public void setSiret(String siret) {
//		this.siret = siret;
//	}
//
//	public String getLibnj() {
//		return libnj;
//	}
//
//	public void setLibnj(String libnj) {
//		this.libnj = libnj;
//	}
//
//	public String getNic() {
//		return nic;
//	}
//
//	public void setNic(String nic) {
//		this.nic = nic;
//	}
//
//	public String getL6_normalisee() {
//		return l6_normalisee;
//	}
//
//	public void setL6_normalisee(String l6_normalisee) {
//		this.l6_normalisee = l6_normalisee;
//	}
//
//	/**
//	 * @return the l7_normalisee
//	 */
//	public String getL7_normalisee() {
//		return l7_normalisee;
//	}
//
//	/**
//	 * @param l7_normalisee the l7_normalisee to set
//	 */
//	public void setL7_normalisee(String l7_normalisee) {
//		this.l7_normalisee = l7_normalisee;
//	}
//
//	public String getNatetab() {
//		return natetab;
//	}
//
//	public void setNatetab(String natetab) {
//		this.natetab = natetab;
//	}
//
//
//	public String getDenominationunitelegale() {
//		return denominationunitelegale;
//	}
//
//	public void setDenominationunitelegale(String denominationunitelegale) {
//		this.denominationunitelegale = denominationunitelegale;
//	}
//
//	public String getCategoriejuridiqueunitelegale() {
//		return categoriejuridiqueunitelegale;
//	}
//
//	public void setCategoriejuridiqueunitelegale(String categoriejuridiqueunitelegale) {
//		this.categoriejuridiqueunitelegale = categoriejuridiqueunitelegale;
//	}
//
//	public String getActiviteprincipaleetablissement() {
//		return activiteprincipaleetablissement;
//	}
//
//	public void setActiviteprincipaleetablissement(String activiteprincipaleetablissement) {
//		this.activiteprincipaleetablissement = activiteprincipaleetablissement;
//	}
//
//	public String getAdresseetablissement() {
//		return adresseetablissement;
//	}
//
//	public void setAdresseetablissement(String adresseetablissement) {
//		this.adresseetablissement = adresseetablissement;
//	}
//
//	public String getComplementadresseetablissement() {
//		return complementadresseetablissement;
//	}
//
//	public void setComplementadresseetablissement(String complementadresseetablissement) {
//		this.complementadresseetablissement = complementadresseetablissement;
//	}
//
//	public String getComplementadresse2etablissement() {
//		return complementadresse2etablissement;
//	}
//
//	public void setComplementadresse2etablissement(String complementadresse2etablissement) {
//		this.complementadresse2etablissement = complementadresse2etablissement;
//	}
//
//	public String getCodepostaletablissement() {
//		return codepostaletablissement;
//	}
//
//	public void setCodepostaletablissement(String codepostaletablissement) {
//		this.codepostaletablissement = codepostaletablissement;
//	}
//
//	public String getLibellecommuneetablissement() {
//		return libellecommuneetablissement;
//	}
//
//	public void setLibellecommuneetablissement(String libellecommuneetablissement) {
//		this.libellecommuneetablissement = libellecommuneetablissement;
//	}
//
//	public String getNomunitelegale() {
//		return nomunitelegale;
//	}
//
//	public void setNomunitelegale(String nomunitelegale) {
//		this.nomunitelegale = nomunitelegale;
//	}
//
//	public String getDivisionetablissement() {
//		return divisionetablissement;
//	}
//
//	public void setDivisionetablissement(String divisionetablissement) {
//		this.divisionetablissement = divisionetablissement;
//	}
//	
//	@Override
//	public String toString() {
//		return ToStringBuilder.reflectionToString(this);
//	}
//
//}

package com.huntimmo.data.dto.dvf;

import java.util.List;

public class Fields {

	private String valeur_fonciere;	//229000
	private String dist;	///"1530.27379973"
	private String nom_reg;//	"ILE-DE-FRANCE"
	private String nombre_pieces_principales;//	3
	private String no_voie;//	6
	private String nature_mutation;//	"Vente"
	private String section;//	"AB"
	private String type_local;//	"Appartement"
	private String commune;//	"VILLIERS-SUR-MARNE"
	private String voie;//	"PIERRE MENDES FRANCE"
	private String code_parcelle;//	"94079000AB0167"
	private String date_mutation;//	"2019-12-06"
	private String adresse;//	"6 ALL PIERRE MENDES FRANCE"
	private String nom_dep;//	"VAL-DE-MARNE"
	private String code_postal;//	"94350"
	private String prix_m2_bati;//	3693.548387096774
	private String surface_reelle_bati;//	62
	private String code_departement;//	"94"
	private String nombre_de_lots;
	
	private List<String> geo = null;
	
	public String getValeur_fonciere() {
		return valeur_fonciere;
	}
	public void setValeur_fonciere(String valeur_fonciere) {
		this.valeur_fonciere = valeur_fonciere;
	}
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	public String getNom_reg() {
		return nom_reg;
	}
	public void setNom_reg(String nom_reg) {
		this.nom_reg = nom_reg;
	}
	public String getNombre_pieces_principales() {
		return nombre_pieces_principales;
	}
	public void setNombre_pieces_principales(String nombre_pieces_principales) {
		this.nombre_pieces_principales = nombre_pieces_principales;
	}
	public String getNo_voie() {
		return no_voie;
	}
	public void setNo_voie(String no_voie) {
		this.no_voie = no_voie;
	}
	public String getNature_mutation() {
		return nature_mutation;
	}
	public void setNature_mutation(String nature_mutation) {
		this.nature_mutation = nature_mutation;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getType_local() {
		return type_local;
	}
	public void setType_local(String type_local) {
		this.type_local = type_local;
	}
	public String getCommune() {
		return commune;
	}
	public void setCommune(String commune) {
		this.commune = commune;
	}
	public String getVoie() {
		return voie;
	}
	public void setVoie(String voie) {
		this.voie = voie;
	}
	public String getCode_parcelle() {
		return code_parcelle;
	}
	public void setCode_parcelle(String code_parcelle) {
		this.code_parcelle = code_parcelle;
	}
	public String getDate_mutation() {
		return date_mutation;
	}
	public void setDate_mutation(String date_mutation) {
		this.date_mutation = date_mutation;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getNom_dep() {
		return nom_dep;
	}
	public void setNom_dep(String nom_dep) {
		this.nom_dep = nom_dep;
	}
	public String getCode_postal() {
		return code_postal;
	}
	public void setCode_postal(String code_postal) {
		this.code_postal = code_postal;
	}
	public String getPrix_m2_bati() {
		return prix_m2_bati;
	}
	public void setPrix_m2_bati(String prix_m2_bati) {
		this.prix_m2_bati = prix_m2_bati;
	}
	public String getSurface_reelle_bati() {
		return surface_reelle_bati;
	}
	public void setSurface_reelle_bati(String surface_reelle_bati) {
		this.surface_reelle_bati = surface_reelle_bati;
	}
	public String getCode_departement() {
		return code_departement;
	}
	public void setCode_departement(String code_departement) {
		this.code_departement = code_departement;
	}
	public List<String> getGeo() {
		return geo;
	}
	public void setGeo(List<String> geo) {
		this.geo = geo;
	}
	public String getNombre_de_lots() {
		return nombre_de_lots;
	}
	public void setNombre_de_lots(String nombre_de_lots) {
		this.nombre_de_lots = nombre_de_lots;
	}
	
}
