package com.huntimmo.data.dto.dvf;
/**
 * @author EKW
 * 
 * Summary of the DVF webservice call.
 * KO, OK.
 *
 */
public class DvfSearchResultsSummary {

	public static String CODE_OK = "OK"; // OK : everything is OK.
	public static String CODE_KO_TECHNICAL = "KO_TECHNICAL"; // KO Tech : something is going wrong BDE aside.
	public static String CODE_KO_TECHNICAL_OPENDATA = "KO_TECHNICAL_OPENDATA"; // KO Tech : something is going wrong Opendata aside.
	public static String CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND = "KO_FUNCTIONNAL_NO_RESULT_FOUND"; // KO Func : no result found.
	public static String CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS = "KO_FUNCTIONNAL_TOO_MANY_RESULTS"; // KO Func : too many results.

	private String code;
	private String message;
	
	public DvfSearchResultsSummary() {
		super();
	}
	
	public DvfSearchResultsSummary(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
