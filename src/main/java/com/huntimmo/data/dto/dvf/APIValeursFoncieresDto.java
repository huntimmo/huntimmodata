package com.huntimmo.data.dto.dvf;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class APIValeursFoncieresDto {

	private Integer nhits;

	private List<Records> records = new ArrayList<Records>();

	public List<Records> getRecords() {
		return records;
	}

	public void setRecords(List<Records> records) {
		this.records = records;
	}

	public Integer getNhits() {
		return nhits;
	}

	public void setNhits(Integer nhits) {
		this.nhits = nhits;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
