package com.huntimmo.data.dto.dvf;

import java.util.function.Function;

import org.springframework.beans.BeanUtils;

public class ValeursFoncieres {
	
	private String valeur_fonciere;	//229000
	private String dist;	///"1530.27379973"
	private String nom_reg;//	"ILE-DE-FRANCE"
	private String nombre_pieces_principales;//	3
	private String no_voie;//	6
	private String nature_mutation;//	"Vente"
	private String section;//	"AB"
	private String type_local;//	"Appartement"
	private String commune;//	"VILLIERS-SUR-MARNE"
	private String voie;//	"PIERRE MENDES FRANCE"
	private String code_parcelle;//	"94079000AB0167"
	private String date_mutation;//	"2019-12-06"
	private String adresse;//	"6 ALL PIERRE MENDES FRANCE"
	private String nom_dep;//	"VAL-DE-MARNE"
	private String code_postal;//	"94350"
	private String prix_m2_bati;//	3693.548387096774
	private String surface_reelle_bati;//	62
	private String code_departement;//	"94"
	private String longitude;
	private String latitude;
	private String nombre_de_lots;
	
	/**
     * Converting Records to EnterpriseDto
     */
    public static Function<Records, ValeursFoncieres> fromRecordsDvf = new Function<Records, ValeursFoncieres>() {
		@Override
		public ValeursFoncieres apply(Records input) {
			ValeursFoncieres valeursFoncieres = new ValeursFoncieres();
			BeanUtils.copyProperties(input.getFields(), valeursFoncieres);
			
			if(input.getFields().getGeo() != null && !input.getFields().getGeo().isEmpty()){
				valeursFoncieres.setLatitude(input.getFields().getGeo().get(0));
				valeursFoncieres.setLongitude(input.getFields().getGeo().get(1));
				
			}
			return valeursFoncieres;
		}
	};
	
	public String getValeur_fonciere() {
		return valeur_fonciere;
	}
	public void setValeur_fonciere(String valeur_fonciere) {
		this.valeur_fonciere = valeur_fonciere;
	}
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	public String getNom_reg() {
		return nom_reg;
	}
	public void setNom_reg(String nom_reg) {
		this.nom_reg = nom_reg;
	}
	public String getNombre_pieces_principales() {
		return nombre_pieces_principales;
	}
	public void setNombre_pieces_principales(String nombre_pieces_principales) {
		this.nombre_pieces_principales = nombre_pieces_principales;
	}
	public String getNo_voie() {
		return no_voie;
	}
	public void setNo_voie(String no_voie) {
		this.no_voie = no_voie;
	}
	public String getNature_mutation() {
		return nature_mutation;
	}
	public void setNature_mutation(String nature_mutation) {
		this.nature_mutation = nature_mutation;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getType_local() {
		return type_local;
	}
	public void setType_local(String type_local) {
		this.type_local = type_local;
	}
	public String getCommune() {
		return commune;
	}
	public void setCommune(String commune) {
		this.commune = commune;
	}
	public String getVoie() {
		return voie;
	}
	public void setVoie(String voie) {
		this.voie = voie;
	}
	public String getCode_parcelle() {
		return code_parcelle;
	}
	public void setCode_parcelle(String code_parcelle) {
		this.code_parcelle = code_parcelle;
	}
	public String getDate_mutation() {
		return date_mutation;
	}
	public void setDate_mutation(String date_mutation) {
		this.date_mutation = date_mutation;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getNom_dep() {
		return nom_dep;
	}
	public void setNom_dep(String nom_dep) {
		this.nom_dep = nom_dep;
	}
	public String getCode_postal() {
		return code_postal;
	}
	public void setCode_postal(String code_postal) {
		this.code_postal = code_postal;
	}
	public String getPrix_m2_bati() {
		return prix_m2_bati;
	}
	public void setPrix_m2_bati(String prix_m2_bati) {
		this.prix_m2_bati = prix_m2_bati;
	}
	public String getSurface_reelle_bati() {
		return surface_reelle_bati;
	}
	public void setSurface_reelle_bati(String surface_reelle_bati) {
		this.surface_reelle_bati = surface_reelle_bati;
	}
	public String getCode_departement() {
		return code_departement;
	}
	public void setCode_departement(String code_departement) {
		this.code_departement = code_departement;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getNombre_de_lots() {
		return nombre_de_lots;
	}
	public void setNombre_de_lots(String nombre_de_lots) {
		this.nombre_de_lots = nombre_de_lots;
	}

}
