package com.huntimmo.data.dto.dvf;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 
 * 
 *         Search results for DVF webservices.
 *
 */
public class DvfSearchResultsDto {

	/**
	 * The summary results. 
	 */
	private DvfSearchResultsSummary dvfSearchResultsSummary;
	
	/**
	 * List of ValeursFoncieres found. Default is empty.
	 */
	private List<ValeursFoncieres> listValeursFoncieres = new ArrayList<ValeursFoncieres>();
	
	public DvfSearchResultsDto() {
		super();
	}

	public DvfSearchResultsSummary getDvfSearchResultsSummary() {
		return dvfSearchResultsSummary;
	}

	public void setDvfSearchResultsSummary(DvfSearchResultsSummary dvfSearchResultsSummary) {
		this.dvfSearchResultsSummary = dvfSearchResultsSummary;
	}

	public List<ValeursFoncieres> getListValeursFoncieres() {
		return listValeursFoncieres;
	}

	public void setListValeursFoncieres(List<ValeursFoncieres> listValeursFoncieres) {
		this.listValeursFoncieres = listValeursFoncieres;
	}
}
