package com.huntimmo.data.dto.commune;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 
 * 
 *         Search results for Commune webservices.
 *
 */
public class CommuneSearchResultsDto {

	/**
	 * The summary results. 
	 */
	private CommuneSearchResultsSummary communeSearchResultsSummary;
	
	/**
	 * List of Commune found. Default is empty.
	 */
	private List<Commune> listCommunes = new ArrayList<Commune>();
	
	public CommuneSearchResultsDto() {
		super();
	}

	public List<Commune> getListCommunes() {
		return listCommunes;
	}

	public void setListCommunes(List<Commune> listCommunes) {
		this.listCommunes = listCommunes;
	}

	public CommuneSearchResultsSummary getCommuneSearchResultsSummary() {
		return communeSearchResultsSummary;
	}

	public void setCommuneSearchResultsSummary(CommuneSearchResultsSummary communeSearchResultsSummary) {
		this.communeSearchResultsSummary = communeSearchResultsSummary;
	}
}
