
package com.huntimmo.data.dto.commune;

import java.util.List;
import java.util.function.Function;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.huntimmo.data.dto.commune.Record;

public class Commune {

    @JsonProperty("code_comm")
    private String codeComm;
    @JsonProperty("nom_dept")
    private String nomDept;
    @JsonProperty("statut")
    private String statut;
    @JsonProperty("z_moyen")
    private Integer zMoyen;
    @JsonProperty("nom_region")
    private String nomRegion;
    @JsonProperty("code_reg")
    private String codeReg;
    @JsonProperty("insee_com")
    private String inseeCom;
    @JsonProperty("code_dept")
    private String codeDept;
    @JsonProperty("geo_point_2d")
    private List<Double> geoPoint2d = null;
    @JsonProperty("postal_code")
    private String postalCode;
    @JsonProperty("id_geofla")
    private String idGeofla;
    @JsonProperty("code_cant")
    private String codeCant;
    @JsonProperty("geo_shape")
    private GeoShape geoShape;
    @JsonProperty("superficie")
    private Integer superficie;
    @JsonProperty("nom_comm")
    private String nomComm;
    @JsonProperty("code_arr")
    private String codeArr;
    @JsonProperty("population")
    private Double population;
    
    /**
     * Converting Records to Commune
     */
    public static Function<Record, Commune> fromRecordsCommune = new Function<Record, Commune>() {
		@Override
		public Commune apply(Record input) {
			Commune commune = new Commune();
			BeanUtils.copyProperties(input.getFields(), commune);
			
			return commune;
		}
	};
  
    @JsonProperty("code_comm")
    public String getCodeComm() {
        return codeComm;
    }

    @JsonProperty("code_comm")
    public void setCodeComm(String codeComm) {
        this.codeComm = codeComm;
    }

    @JsonProperty("nom_dept")
    public String getNomDept() {
        return nomDept;
    }

    @JsonProperty("nom_dept")
    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }

    @JsonProperty("statut")
    public String getStatut() {
        return statut;
    }

    @JsonProperty("statut")
    public void setStatut(String statut) {
        this.statut = statut;
    }

    @JsonProperty("z_moyen")
    public Integer getZMoyen() {
        return zMoyen;
    }

    @JsonProperty("z_moyen")
    public void setZMoyen(Integer zMoyen) {
        this.zMoyen = zMoyen;
    }

    @JsonProperty("nom_region")
    public String getNomRegion() {
        return nomRegion;
    }

    @JsonProperty("nom_region")
    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }

    @JsonProperty("code_reg")
    public String getCodeReg() {
        return codeReg;
    }

    @JsonProperty("code_reg")
    public void setCodeReg(String codeReg) {
        this.codeReg = codeReg;
    }

    @JsonProperty("insee_com")
    public String getInseeCom() {
        return inseeCom;
    }

    @JsonProperty("insee_com")
    public void setInseeCom(String inseeCom) {
        this.inseeCom = inseeCom;
    }

    @JsonProperty("code_dept")
    public String getCodeDept() {
        return codeDept;
    }

    @JsonProperty("code_dept")
    public void setCodeDept(String codeDept) {
        this.codeDept = codeDept;
    }

    @JsonProperty("geo_point_2d")
    public List<Double> getGeoPoint2d() {
        return geoPoint2d;
    }

    @JsonProperty("geo_point_2d")
    public void setGeoPoint2d(List<Double> geoPoint2d) {
        this.geoPoint2d = geoPoint2d;
    }

    @JsonProperty("postal_code")
    public String getPostalCode() {
        return postalCode;
    }

    @JsonProperty("postal_code")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @JsonProperty("id_geofla")
    public String getIdGeofla() {
        return idGeofla;
    }

    @JsonProperty("id_geofla")
    public void setIdGeofla(String idGeofla) {
        this.idGeofla = idGeofla;
    }

    @JsonProperty("code_cant")
    public String getCodeCant() {
        return codeCant;
    }

    @JsonProperty("code_cant")
    public void setCodeCant(String codeCant) {
        this.codeCant = codeCant;
    }

    @JsonProperty("geo_shape")
    public GeoShape getGeoShape() {
        return geoShape;
    }

    @JsonProperty("geo_shape")
    public void setGeoShape(GeoShape geoShape) {
        this.geoShape = geoShape;
    }

    @JsonProperty("superficie")
    public Integer getSuperficie() {
        return superficie;
    }

    @JsonProperty("superficie")
    public void setSuperficie(Integer superficie) {
        this.superficie = superficie;
    }

    @JsonProperty("nom_comm")
    public String getNomComm() {
        return nomComm;
    }

    @JsonProperty("nom_comm")
    public void setNomComm(String nomComm) {
        this.nomComm = nomComm;
    }

    @JsonProperty("code_arr")
    public String getCodeArr() {
        return codeArr;
    }

    @JsonProperty("code_arr")
    public void setCodeArr(String codeArr) {
        this.codeArr = codeArr;
    }

    @JsonProperty("population")
    public Double getPopulation() {
        return population;
    }

    @JsonProperty("population")
    public void setPopulation(Double population) {
        this.population = population;
    }

}
