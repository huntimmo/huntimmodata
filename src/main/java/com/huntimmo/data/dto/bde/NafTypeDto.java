package com.huntimmo.data.dto.bde;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class NafTypeDto {

	private String codeNaf;
    private String libelleCourtNaf;

	public String getCodeNaf() {
		return codeNaf;
	}
	public void setCodeNaf(String codeNaf) {
		this.codeNaf = codeNaf;
	}
	public String getLibelleCourtNaf() {
		return libelleCourtNaf;
	}
	public void setLibelleCourtNaf(String libelleCourtNaf) {
		this.libelleCourtNaf = libelleCourtNaf;
	}
	
//	/**
//	 * Fonction permettant de créer un objet NafTypeDto depuis un objet NafType
//	 */
//	public static Function<NafType, NafTypeDto> fromNafType = new Function<NafType, NafTypeDto>() {
//		@Override
//		public NafTypeDto apply(NafType input) {
//			NafTypeDto nafTypeDto = new NafTypeDto();
//
//			nafTypeDto.setCodeNaf(input.getCodeNaf());
//			nafTypeDto.setLibelleCourtNaf(input.getLibelleCourtNaf());
//
//			return nafTypeDto;
//		}
//	};
//	
//	/**
//	 * Fonction permettant de créer un objet NafTypeDto depuis un objet com.natixis.ekw.service.bde.etablissement.wsdl.NafType
//	 */
//	public static Function<com.natixis.fd3.service.bde.etablissement.wsdl.NafType, NafTypeDto> fromNafTypeEtablissement = new Function<com.natixis.fd3.service.bde.etablissement.wsdl.NafType, NafTypeDto>() {
//		@Override
//		public NafTypeDto apply(com.natixis.fd3.service.bde.etablissement.wsdl.NafType input) {
//			NafTypeDto nafTypeDto = new NafTypeDto();
//
//			nafTypeDto.setCodeNaf(input.getCodeNaf());
//			nafTypeDto.setLibelleCourtNaf(input.getLibelleCourtNaf());
//
//			return nafTypeDto;
//		}
//	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codeNaf == null) ? 0 : codeNaf.hashCode());
		result = prime * result
				+ ((libelleCourtNaf == null) ? 0 : libelleCourtNaf.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NafTypeDto other = (NafTypeDto) obj;
		if (codeNaf == null) {
			if (other.codeNaf != null)
				return false;
		} else if (!codeNaf.equals(other.codeNaf))
			return false;
		if (libelleCourtNaf == null) {
			if (other.libelleCourtNaf != null)
				return false;
		} else if (!libelleCourtNaf.equals(other.libelleCourtNaf))
			return false;
		return true;
	}
	@Override
    public String toString() {
		return ToStringBuilder.reflectionToString(this);
    }
}
