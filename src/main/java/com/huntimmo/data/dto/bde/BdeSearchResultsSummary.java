package com.huntimmo.data.dto.bde;
/**
 * @author EKW
 * 
 * Summary of the BDE webservice call.
 * KO, OK.
 *
 */
public class BdeSearchResultsSummary {

	public static String CODE_OK = "OK"; // OK : everything is OK.
	public static String CODE_KO_TECHNICAL = "KO_TECHNICAL"; // KO Tech : something is going wrong BDE aside.
	public static String CODE_KO_TECHNICAL_OPENDATA = "KO_TECHNICAL_OPENDATA"; // KO Tech : something is going wrong Opendata aside.
	public static String CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND = "KO_FUNCTIONNAL_NO_RESULT_FOUND"; // KO Func : no result found.
	public static String CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS = "KO_FUNCTIONNAL_TOO_MANY_RESULTS"; // KO Func : too many results.
	public static String CODE_KO_FUNCTIONNAL_INEXISTANT_ESTABLISHMENT = "KO_FUNCTIONNAL_INEXISTANT_ESTABLISHMENT"; // KO Func : Etablissement siège de cette entreprise inexistant
	public static String CODE_KO_FUNCTIONNAL_INVALID_ESTABLISHMENT = "KO_FUNCTIONNAL_INVALID_ESTABLISHMENT"; // KO Func : IDENTIFIANT ETABLISSEMENT INVALIDE
	public static String CODE_KO_FUNCTIONNAL_NO_COTATION = "KO_FUNCTIONNAL_NO_COTATION"; // KO Func : Absence de cotation pour cette entreprise
	public static String CODE_KO_FUNCTIONNAL_NO_IMPAYES = "KO_FUNCTIONNAL_NO_IMPAYES"; // KO Func : Aucun impayé trouvé
	public static String CODE_KO_FUNCTIONNAL_NO_DIRIGEANTS = "KO_FUNCTIONNAL_NO_DIRIGEANTS"; // KO Func : Aucun dirigeant pour cette entreprise
	public static String CODE_KO_FUNCTIONNAL_NO_ACTIONNAIRES = "KO_FUNCTIONNAL_NO_ACTIONNAIRES"; // KO Func : Aucun actionnaire connu pour cette entreprise
	public static String CODE_KO_FUNCTIONNAL_NO_PARTICIPATIONS = "KO_FUNCTIONNAL_NO_PARTICIPATIONS"; // KO Func : Aucune participation détenue par cette entreprise
	public static String CODE_KO_FUNCTIONNAL_NO_KEY_FIGURES = "KO_FUNCTIONNAL_NO_KEY_FIGURES"; // KO Func : Pas de chiffres-clés pour cette entreprise
	public static String CODE_KO_FUNCTIONNAL_NO_RISK = "KO_FUNCTIONNAL_NO_RISK"; // KO Func : Absence de risques bancaires pour cette entreprise et cettte catégorie
	public static String CODE_KO_FUNCTIONNAL_NO_COTATION_STANDBY_REFRESH = "CODE_KO_FUNCTIONNAL_NO_COTATION_STANDBY_REFRESH"; // KO Func : Absence de cotation mais rafraichissement en cours
	public static String CODE_KO_FUNCTIONNAL_NO_DIRIGEANT_STANDBY_REFRESH = "CODE_KO_FUNCTIONNAL_NO_DIRIGEANT_STANDBY_REFRESH"; // KO Func : Absence de dirigeant mais rafraichissement en cours
	public static String CODE_KO_FUNCTIONNAL_INVALID_SIRET = "CODE_KO_FUNCTIONNAL_INVALID_SIRET"; // KO Func : siret invalid

	private String code;
	private String message;
	
	public BdeSearchResultsSummary() {
		super();
	}
	
	public BdeSearchResultsSummary(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
