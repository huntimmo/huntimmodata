package com.huntimmo.data.dto.bde;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author EKM
 *         Search criteria for BDE webservices.
 */
public class BdeSearchCriteriaDto {

	String identifiant;
	String codePostal;
	String commune;

	public BdeSearchCriteriaDto() {
		super();
	}

	public BdeSearchCriteriaDto(String identifiant,
			String codePostal, String commune) {
		super();
		this.identifiant = identifiant;
		this.codePostal = codePostal;
		this.commune = commune;
	}

	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getCommune() {
		return commune;
	}
	public void setCommune(String commune) {
		this.commune = commune;
	}

	@JsonIgnore
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
