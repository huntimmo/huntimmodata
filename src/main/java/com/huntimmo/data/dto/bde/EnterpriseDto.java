package com.huntimmo.data.dto.bde;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import com.huntimmo.data.dto.common.MontantDto;
import com.huntimmo.data.dto.sirene.Records;
import com.huntimmo.data.util.EnterpriseUtils;


/**
 * @author EKM
 * 
 *         EnterpriseDto corresponding to Entreprise BDE Object.
 *
 */
public class EnterpriseDto {

	private static final String ESPACE = " ";
	private static final String SLASH = "/";
	private static final String ETOILE = "*";

	public EnterpriseDto() {
		super();
	}

	private BdeSearchResultsSummary bdeSearchResultsSummary;
	private String siren;
	private String nic;
	private String siret;
	private String raisonSociale;
	private String enseigne;
	private String adresseLigne1;
	private String adresseLigne2;
	private String adresseLigne3;
	private String adresseLigne4;
	private String codeCommune;
	private String commune;
	private String pays;
	private String numeroTelephone;
	private String creationDate;
	private String natureJuridiqueCode;
	private String natureJuridiqueLibelle;
	private MontantDto capital;
	private NafTypeDto nafType;
	private MontantDto chiffreAffaires;
	private String nomNaissance;
	private String libelleNaf;

  
	
	/**
     * Converting Records to EnterpriseDto
     */
    public static Function<Records, EnterpriseDto> fromEtablissementSireneType = new Function<Records, EnterpriseDto>() {
		@Override
		public EnterpriseDto apply(Records input) {
			EnterpriseDto enterpriseDto = new EnterpriseDto();
			enterpriseDto.setAdresseLigne1(input.getFields().getL4_normalisee());
			enterpriseDto.setAdresseLigne4(input.getFields().getL6_normalisee());
			enterpriseDto.setCodeCommune(input.getFields().getCodpos());
			enterpriseDto.setCommune(input.getFields().getLibcom());
			enterpriseDto.setPays(input.getFields().getL7_normalisee());
			enterpriseDto.setCreationDate(input.getFields().getDcren());
			// si la nature juridique est renseignée alors il s'agit d'une entreprise du type Prof.Lib.
			String codeNatureJuridique = "";
			if(StringUtils.isNotEmpty(input.getFields().getNatetab())){
				codeNatureJuridique = input.getFields().getNatetab();
			}
			else{
				codeNatureJuridique = input.getFields().getNj();
			}
			enterpriseDto.setNatureJuridiqueCode(codeNatureJuridique);
			enterpriseDto.setNatureJuridiqueLibelle(EnterpriseUtils.getLibelleFormeJuridiqueByCode(codeNatureJuridique));
			
			enterpriseDto.setNic(input.getFields().getNic());
			enterpriseDto.setRaisonSociale(StringUtils.replaceEach(
					input.getFields().getNomen_long(), new String[]{ETOILE, SLASH}, new String[]{ESPACE, ESPACE}));
			enterpriseDto.setSiren(input.getFields().getSiren());
			enterpriseDto.setSiret(input.getFields().getSiret());
			// nom de naissance dans le cas d'une personne physique
			enterpriseDto.setNomNaissance(input.getFields().getNom());
			
			// récupération du code NAF et libellé NAF
			NafTypeDto nafTypeDto = new NafTypeDto();
			nafTypeDto.setCodeNaf(input.getFields().getApet700());
			nafTypeDto.setLibelleCourtNaf(input.getFields().getLibapet());
			enterpriseDto.setNafType(nafTypeDto);
			
			return enterpriseDto;
		}
	};
	
	
//	/**
//	 * Permet de supprimer les espaces possibles dans la valeur du SIRET.
//	 * 
//	 * @param input
//	 * @param enterpriseDto
//	 */
//	private static void renseignerSiret(EtablissementTrouveType input,
//			EnterpriseDto enterpriseDto) {
//		if(input.getSiret() != null && StringUtils.isNotEmpty(input.getSiret())){
//			enterpriseDto.setSiret(input.getSiret().replaceAll(ESPACE, ""));
//		}
//	}

	public BdeSearchResultsSummary getBdeSearchResultsSummary() {
		return bdeSearchResultsSummary;
	}
	public void setBdeSearchResultsSummary(
			BdeSearchResultsSummary bdeSearchResultsSummary) {
		this.bdeSearchResultsSummary = bdeSearchResultsSummary;
	}
	/**
	 * @return the siren
	 */
	public String getSiren() {
		return siren;
	}
	/**
	 * @param siren the siren to set
	 */
	public void setSiren(String siren) {
		this.siren = siren;
	}
	/**
	 * @return the nic
	 */
	public String getNic() {
		return nic;
	}
	/**
	 * @param nic the nic to set
	 */
	public void setNic(String nic) {
		this.nic = nic;
	}
	/**
	 * @return the siret
	 */
	public String getSiret() {
		return siret;
	}
	/**
	 * @param siret the siret to set
	 */
	public void setSiret(String siret) {
		this.siret = siret;
	}
	/**
	 * @return the raisonSociale
	 */
	public String getRaisonSociale() {
		return raisonSociale;
	}
	/**
	 * @param raisonSociale the raisonSociale to set
	 */
	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}
	/**
	 * @return the enseigne
	 */
	public String getEnseigne() {
		return enseigne;
	}
	/**
	 * @param enseigne the enseigne to set
	 */
	public void setEnseigne(String enseigne) {
		this.enseigne = enseigne;
	}
	/**
	 * @return the adresseLigne1
	 */
	public String getAdresseLigne1() {
		return adresseLigne1;
	}
	/**
	 * @param adresseLigne1 the adresseLigne1 to set
	 */
	public void setAdresseLigne1(String adresseLigne1) {
		this.adresseLigne1 = adresseLigne1;
	}
	/**
	 * @return the adresseLigne2
	 */
	public String getAdresseLigne2() {
		return adresseLigne2;
	}
	/**
	 * @param adresseLigne2 the adresseLigne2 to set
	 */
	public void setAdresseLigne2(String adresseLigne2) {
		this.adresseLigne2 = adresseLigne2;
	}
	/**
	 * @return the adresseLigne3
	 */
	public String getAdresseLigne3() {
		return adresseLigne3;
	}
	/**
	 * @param adresseLigne3 the adresseLigne3 to set
	 */
	public void setAdresseLigne3(String adresseLigne3) {
		this.adresseLigne3 = adresseLigne3;
	}
	/**
	 * @return the adresseLigne4
	 */
	public String getAdresseLigne4() {
		return adresseLigne4;
	}
	/**
	 * @param adresseLigne4 the adresseLigne4 to set
	 */
	public void setAdresseLigne4(String adresseLigne4) {
		this.adresseLigne4 = adresseLigne4;
	}
	/**
	 * @return the codeCommune
	 */
	public String getCodeCommune() {
		return codeCommune;
	}
	/**
	 * @param codeCommune the codeCommune to set
	 */
	public void setCodeCommune(String codeCommune) {
		this.codeCommune = codeCommune;
	}
	/**
	 * @return the commune
	 */
	public String getCommune() {
		return commune;
	}
	/**
	 * @param commune the commune to set
	 */
	public void setCommune(String commune) {
		this.commune = commune;
	}
	/**
	 * @return the pays
	 */
	public String getPays() {
		return pays;
	}
	/**
	 * @param pays the pays to set
	 */
	public void setPays(String pays) {
		this.pays = pays;
	}
	/**
	 * @return the numeroTelephone
	 */
	public String getNumeroTelephone() {
		return numeroTelephone;
	}
	/**
	 * @param numeroTelephone the numeroTelephone to set
	 */
	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}	
	public MontantDto getCapital() {
		return capital;
	}
	public void setCapital(MontantDto capital) {
		this.capital = capital;
	}
	public String getNatureJuridiqueCode() {
		return natureJuridiqueCode;
	}
	public void setNatureJuridiqueCode(String natureJuridiqueCode) {
		this.natureJuridiqueCode = natureJuridiqueCode;
	}
	public String getNatureJuridiqueLibelle() {
		return natureJuridiqueLibelle;
	}
	public void setNatureJuridiqueLibelle(String natureJuridiqueLibelle) {
		this.natureJuridiqueLibelle = natureJuridiqueLibelle;
	}
	public NafTypeDto getNafType() {
		return nafType;
	}
	public void setNafType(NafTypeDto nafType) {
		this.nafType = nafType;
	}
	public MontantDto getChiffreAffaires() {
		return chiffreAffaires;
	}
	public void setChiffreAffaires(MontantDto chiffreAffaires) {
		this.chiffreAffaires = chiffreAffaires;
	}

	public String getNomNaissance() {
		return nomNaissance;
	}

	public void setNomNaissance(String nomNaissance) {
		this.nomNaissance = nomNaissance;
	}

	/**
	 * @return the libelleNaf
	 */
	public String getLibelleNaf() {
		return libelleNaf;
	}

	/**
	 * @param libelleNaf the libelleNaf to set
	 */
	public void setLibelleNaf(String libelleNaf) {
		this.libelleNaf = libelleNaf;
	}
}