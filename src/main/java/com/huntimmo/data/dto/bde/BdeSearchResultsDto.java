package com.huntimmo.data.dto.bde;

import java.util.ArrayList;
import java.util.List;

import com.huntimmo.data.dto.sirene.Etablissements;

/**
 * @author EKM
 * 
 *         Search results for BDE webservices.
 *
 */
public class BdeSearchResultsDto {

	/**
	 * The summary results. 
	 */
	private BdeSearchResultsSummary bdeSearchResultsSummary;
	
	/**
	 * List of EnterpriseDto found. Default is empty.
	 */
	private List<EnterpriseDto> listEnterpriseDtos = new ArrayList<EnterpriseDto>(); 
	
	/**
	 * List of Etablissments found. Default is empty.
	 */
	private List<Etablissements> listEtablissements = new ArrayList<Etablissements>();
	
	public BdeSearchResultsDto() {
		super();
	}

	/**
	 * @return the bdeSearchResultsSummary
	 */
	public BdeSearchResultsSummary getBdeSearchResultsSummary() {
		return bdeSearchResultsSummary;
	}

	/**
	 * @param bdeSearchResultsSummary the bdeSearchResultsSummary to set
	 */
	public void setBdeSearchResultsSummary(BdeSearchResultsSummary bdeSearchResultsSummary) {
		this.bdeSearchResultsSummary = bdeSearchResultsSummary;
	}

	/**
	 * @return the listEnterpriseDtos
	 */
	public List<EnterpriseDto> getListEnterpriseDtos() {
		return listEnterpriseDtos;
	}

	/**
	 * @param listEnterpriseDtos the listEnterpriseDtos to set
	 */
	public void setListEnterpriseDtos(List<EnterpriseDto> listEnterpriseDtos) {
		this.listEnterpriseDtos = listEnterpriseDtos;
	}

	public List<Etablissements> getListEtablissements() {
		return listEtablissements;
	}

	public void setListEtablissements(List<Etablissements> listEtablissements) {
		this.listEtablissements = listEtablissements;
	}
}
