package com.huntimmo.data.service.sirene;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.huntimmo.data.dto.bde.BdeSearchCriteriaDto;
import com.huntimmo.data.dto.bde.BdeSearchResultsDto;
import com.huntimmo.data.dto.bde.BdeSearchResultsSummary;
import com.huntimmo.data.dto.bde.EnterpriseDto;
import com.huntimmo.data.dto.common.ResponseDto;
import com.huntimmo.data.dto.sirene.APISireneDto;
import com.huntimmo.data.util.EnterpriseUtils;

@Service
public class APISireneService {

	private static final String QUERY = "&q=";

	private static final String ESPACE_VIDE = "";

	private static final String ESPACE = " ";

	/** Logger */

	private final Logger log = LoggerFactory.getLogger(APISireneService.class);

	@Value("${sirene.webservices.url}")

	private String sireneWebserviceUrl;

	private String sireneNearPointWebserviceUrl = "https://entreprise.data.gouv.fr/api/sirene/v1/near_point/?radius=2&per_page=50&page=1"; //&lat=48.84&long=2.55&activite_principale=8531Z&
		
	private final RestTemplate restTemplate;

	public APISireneService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	/**
	 * 
	 * Search enterprise data by siret
	 * 
	 * @param siret
	 * 
	 * @return APISireneDto
	 * 
	 */
	public ResponseDto<EnterpriseDto> searchBySiret(String siret) {
		ResponseDto<EnterpriseDto> responseDto = null;
		APISireneDto result = null;
		initProxy();
		log.debug("SIRENE - methode : Sirene_searchBySiret - identifiant : {} - request : {}", siret, "/opendata/");
		if (StringUtils.isEmpty(siret)) {
			responseDto = new ResponseDto<EnterpriseDto>(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
					BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND);
			return responseDto;
		}
		String urlSirene = sireneWebserviceUrl + QUERY;
		URI url = null;
		try {
			url = new URI(urlSirene + "siret=" + siret);
		} catch (URISyntaxException e) {
			log.debug("SIRENE - methode : Sirene_searchBySiret - identifiant : {} - error : {}", siret, e);
		}
		ResponseEntity<APISireneDto> response = null;

		try {
			response = restTemplate.getForEntity(url, APISireneDto.class);
			// gestion du cas où aucune donnée trouvée
			if (response != null) {
				result = response.getBody();
			}
			else {
				// gestion du cas où OPENDATA non disponible
				responseDto = new ResponseDto<EnterpriseDto>(BdeSearchResultsSummary.CODE_KO_TECHNICAL,
						BdeSearchResultsSummary.CODE_KO_TECHNICAL_OPENDATA);
				return responseDto;
			}
		} catch (RestClientException e) {
			// gestion du cas où OPENDATA non disponible
			log.error("SIRENE - methode : Sirene_searchByCriteria - identifiant : {} - error : {}", siret.toString(),e);

			// gestion du cas où OPENDATA non disponible
			responseDto = new ResponseDto<EnterpriseDto>(BdeSearchResultsSummary.CODE_KO_TECHNICAL,
					BdeSearchResultsSummary.CODE_KO_TECHNICAL_OPENDATA);
			return responseDto;
		}

		if (result.getRecords().isEmpty()) {
			responseDto = new ResponseDto<EnterpriseDto>(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
					BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND);
		}
		else {
			responseDto = new ResponseDto<EnterpriseDto>(
					result
							.getRecords()
							.stream()
							.map(record -> {
								EnterpriseDto enterpriseDto = EnterpriseDto.fromEtablissementSireneType.apply(record);
								return enterpriseDto;
							}).findFirst().get(),
					BdeSearchResultsSummary.CODE_OK, BdeSearchResultsSummary.CODE_OK
			);
		}
		log.debug("SIRENE - methode : Sirene_searchBySiret - identifiant : {} - response : {}", result, "/opendata/");
		return responseDto;

	}

	/**
	 * Search by SIREN or Raison Sociale and Code postal
	 * @param criteria
	 * @return APISireneDto
	 */

	public BdeSearchResultsDto searchByCriteria(BdeSearchCriteriaDto searchCriteriaDto) {
		initProxy();
		BdeSearchResultsDto bdeSearchResultsDto = new BdeSearchResultsDto();
		String urlSirene = sireneWebserviceUrl + QUERY;
		// cas aucun critère, on bloque la recherche
		if (StringUtils.isEmpty(searchCriteriaDto.getIdentifiant())
				&& StringUtils.isEmpty(searchCriteriaDto.getCodePostal())
				&& StringUtils.isEmpty(searchCriteriaDto.getCommune())) {
			bdeSearchResultsDto.setBdeSearchResultsSummary(
					new BdeSearchResultsSummary(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS,
							BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS));
			return bdeSearchResultsDto;
		}

		APISireneDto result = null;
		StringBuilder criteria = new StringBuilder();
		// recherche par raison sociale + code postal
		if (StringUtils.isNotEmpty(searchCriteriaDto.getIdentifiant())
				&& StringUtils.isNotEmpty(searchCriteriaDto.getCodePostal())) {
			criteria.append(searchCriteriaDto.getIdentifiant() + ESPACE);
			criteria.append(searchCriteriaDto.getCodePostal());
		}
		// recherche par SIREN
		else {
			if (StringUtils.isNotEmpty(searchCriteriaDto.getIdentifiant())
					&& EnterpriseUtils.isSirenValid(searchCriteriaDto.getIdentifiant())) {
				criteria.append(StringUtils.isNotEmpty(searchCriteriaDto.getIdentifiant())
						? "siren=" + searchCriteriaDto.getIdentifiant() : ESPACE_VIDE);
			}
			else {
				criteria.append(StringUtils.isNotEmpty(searchCriteriaDto.getIdentifiant())
						? searchCriteriaDto.getIdentifiant() : ESPACE_VIDE);
			}
		}

		log.debug("SIRENE - methode : Sirene_searchByCriteria - identifiant : {} - request : {}", criteria.toString(),
				urlSirene + criteria.toString());

		URI url = null;
		try {
			String querySiren = urlSirene + URLEncoder.encode(criteria.toString(), StandardCharsets.UTF_8.name());
			url = new URI(querySiren);
		} catch (URISyntaxException e) {
			log.error("SIRENE - methode : Sirene_searchByCriteria - identifiant : {} - error : {}", criteria.toString(),
					e);
		} catch (UnsupportedEncodingException e) {
			log.error("SIRENE - methode : Sirene_searchByCriteria - identifiant : {} - error : {}", criteria.toString(),
					e);
		}

		ResponseEntity<APISireneDto> response = null;
		try {
			response = restTemplate.getForEntity(url, APISireneDto.class);
			// gestion du cas où aucune donnée trouvée
			if (response != null) {
				result = response.getBody();
			}
			else {
				// gestion du cas où OPENDATA non disponible
				bdeSearchResultsDto.setBdeSearchResultsSummary(
						new BdeSearchResultsSummary(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
								BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
				return bdeSearchResultsDto;
			}
		} catch (RestClientException e) {
			// gestion du cas où OPENDATA non disponible

			log.error("SIRENE - methode : Sirene_searchByCriteria - identifiant : {} - error : {}", criteria.toString(), e);
			bdeSearchResultsDto.setBdeSearchResultsSummary(new BdeSearchResultsSummary(
					BdeSearchResultsSummary.CODE_KO_TECHNICAL, BdeSearchResultsSummary.CODE_KO_TECHNICAL));
			return bdeSearchResultsDto;
		}
		if (result.getRecords().isEmpty()) {
			bdeSearchResultsDto.setBdeSearchResultsSummary(
					new BdeSearchResultsSummary(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
							BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
		}
		else {

			bdeSearchResultsDto.setListEnterpriseDtos(
					result
							.getRecords()
							.stream()
							.map(record -> {
								EnterpriseDto enterpriseDto = EnterpriseDto.fromEtablissementSireneType.apply(record);
								// recherche des données nature juridique dans
								// la base SIRENE
								return enterpriseDto;
							}).collect(Collectors.toList())
			);

			bdeSearchResultsDto.setBdeSearchResultsSummary(
					new BdeSearchResultsSummary(BdeSearchResultsSummary.CODE_OK, BdeSearchResultsSummary.CODE_OK));
		}

		log.debug("SIRENE - methode : Sirene_searchByCriteria - identifiant : {} - response : {}", criteria.toString(),
				result.getRecords().size());

		return bdeSearchResultsDto;

	}

	/**
	 * @param latitude
	 * @param longitude
	 * @param naf
	 * @return
	 */
	public BdeSearchResultsDto searchByLatitudeLongitudeAndCodeNaf(String latitude, String longitude, String naf) {
		initProxy();
		BdeSearchResultsDto bdeSearchResultsDto = new BdeSearchResultsDto();
		String urlSirene = sireneNearPointWebserviceUrl;
		// cas aucun critère, on bloque la recherche
		if (StringUtils.isEmpty(latitude)
				&& StringUtils.isEmpty(longitude)
				&& StringUtils.isEmpty(naf)) {
			bdeSearchResultsDto.setBdeSearchResultsSummary(
					new BdeSearchResultsSummary(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
							BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
			return bdeSearchResultsDto;
		}

		APISireneDto result = null;
		StringBuilder criteria = new StringBuilder();
		// recherche par latitude + longitude + code NAF
		// //&lat=48.84&long=2.55&activite_principale=8531Z&
		criteria.append("&lat=").append(latitude);
		criteria.append("&long=").append(longitude);
		criteria.append("&activite_principale=").append(naf);
		
		log.debug("SIRENE - methode : Sirene_searchByLatitudeLongitudeAndCodeNaf - identifiant : {} - request : {}", criteria.toString(),
				urlSirene + criteria.toString());

		URI url = null;
		try {
			String querySiren = urlSirene + criteria.toString();
			url = new URI(querySiren);
		} catch (URISyntaxException e) {
			log.error("SIRENE - methode : Sirene_searchByLatitudeLongitudeAndCodeNaf - identifiant : {} - error : {}", criteria.toString(), e);
		}
		
		ResponseEntity<APISireneDto> response = null;
		try {
			response = restTemplate.getForEntity(url, APISireneDto.class);
			// gestion du cas où aucune donnée trouvée
			if (response != null) {
				result = response.getBody();
			}
			else {
				// gestion du cas où OPENDATA non disponible
				bdeSearchResultsDto.setBdeSearchResultsSummary(
						new BdeSearchResultsSummary(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
								BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
				return bdeSearchResultsDto;
			}
		} catch (RestClientException e) {
			// gestion du cas où OPENDATA non disponible

			log.error("SIRENE - methode : Sirene_searchByLatitudeLongitudeAndCodeNaf - identifiant : {} - error : {}", criteria.toString(), e);
			bdeSearchResultsDto.setBdeSearchResultsSummary(new BdeSearchResultsSummary(
					BdeSearchResultsSummary.CODE_KO_TECHNICAL, BdeSearchResultsSummary.CODE_KO_TECHNICAL));
			return bdeSearchResultsDto;
		}
		if (result.getEtablissements().isEmpty()) {
			bdeSearchResultsDto.setBdeSearchResultsSummary(
					new BdeSearchResultsSummary(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
							BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
		}
		else {

			bdeSearchResultsDto.setListEtablissements(result.getEtablissements());

			bdeSearchResultsDto.setBdeSearchResultsSummary(
					new BdeSearchResultsSummary(BdeSearchResultsSummary.CODE_OK, BdeSearchResultsSummary.CODE_OK));
		}

		log.debug("SIRENE - methode : Sirene_searchByLatitudeLongitudeAndCodeNaf - identifiant : {} - response : {}", criteria.toString(),
				result.getRecords().size());

		return bdeSearchResultsDto;

	}

	
	/**
	 * 
	 * Sets the proxy if one is defined
	 * 
	 */

	private void initProxy() {
		MySimpleClientHttpRequestFactory requestFactory = new MySimpleClientHttpRequestFactory(new NullHostnameVerifier());
		restTemplate.setRequestFactory(requestFactory);

//		 if (StringUtils.isNotEmpty(sireneHttpProxyHost) &&
//			 StringUtils.isNotEmpty(sireneHttpProxyPort)) {
//			
//			 initProxyHostPort(sireneHttpProxyHost, sireneHttpProxyPort);
//		
//		 }
//		
//		 if (StringUtils.isNotEmpty(sireneHttpsProxyHost) &&
//		 StringUtils.isNotEmpty(sireneHttpsProxyPort)) {
//		
//		 initProxyHostPort(sireneHttpsProxyHost, sireneHttpsProxyPort);
//		
//		 }

	}

	/**
	 * 
	 * Sets the proxy config
	 * @param host
	 * @param port
	 * 
	 */

	private void initProxyHostPort(String host, String port) {
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress(host, Integer.parseInt(port)));
		requestFactory.setProxy(proxy);
		restTemplate.setRequestFactory(requestFactory);
	}
	
	private class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
	
	public class MySimpleClientHttpRequestFactory extends SimpleClientHttpRequestFactory {

        private final HostnameVerifier verifier;

        public MySimpleClientHttpRequestFactory(HostnameVerifier verifier) {
            this.verifier = verifier;
        }

        @Override
        protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
            if (connection instanceof HttpsURLConnection) {
                ((HttpsURLConnection) connection).setHostnameVerifier(verifier);
                ((HttpsURLConnection) connection).setSSLSocketFactory(trustSelfSignedSSL().getSocketFactory());
                ((HttpsURLConnection) connection).setAllowUserInteraction(true);
            }
            super.prepareConnection(connection, httpMethod);
        }

        public SSLContext trustSelfSignedSSL() {
            try {
                SSLContext ctx = SSLContext.getInstance("TLS");
                X509TrustManager tm = new X509TrustManager() {

                    public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    }

                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                };
                ctx.init(null, new TrustManager[] { tm }, null);
                SSLContext.setDefault(ctx);
                return ctx;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

    }


    public class NullHostnameVerifier implements HostnameVerifier {
       public boolean verify(String hostname, SSLSession session) {
          return true;
       }
    }
   

}