package com.huntimmo.data.service.insee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huntimmo.data.domain.insee.Indicateurs;
import com.huntimmo.data.repository.insee.IndicateursRepository;

@Service
public class IndicateursService {

	@Autowired
	private IndicateursRepository repository;

	public Indicateurs findIndicateur(String codeInsee){
		return repository.findByCODGEO(codeInsee);
	}
}
