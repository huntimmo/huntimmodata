package com.huntimmo.data.service.dvf;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.huntimmo.data.dto.dvf.APIValeursFoncieresDto;
import com.huntimmo.data.dto.dvf.DvfSearchResultsDto;
import com.huntimmo.data.dto.dvf.DvfSearchResultsSummary;
import com.huntimmo.data.dto.dvf.ValeursFoncieres;

@Service

public class APIValeursFoncieresService {

	private static final String RAYON_EN_METRES_1000 = "2000";
	
	private static final String ESPACE_VIDE = "";
	private static final String VIRGULE = ",";

	private static final String ESPACE = " ";

	/** Logger */

	private final Logger log = LoggerFactory.getLogger(APIValeursFoncieresService.class);

	private String dvfAppartementsNearPointWebserviceUrl = "https://data.opendatasoft.com/api/records/1.0/search/?dataset=demande-de-valeurs-foncieres%40public&rows=5000&sort=date_mutation&facet=date_mutation&facet=nature_mutation&facet=commune&facet=section&facet=nombre_de_lots&facet=type_local&facet=nature_culture&facet=nom_reg&facet=nom_dep&facet=nom_epci&facet=libelle_nature_culture_speciale&refine.nature_mutation=Vente&refine.type_local=Appartement&geofilter.distance="; 
	//48.843835%2C+2.540861%2C+2000
		
	private String dvfVEFANearPointWebserviceUrl = "https://data.opendatasoft.com/api/records/1.0/search/?dataset=demande-de-valeurs-foncieres%40public&rows=5000&sort=date_mutation&facet=date_mutation&facet=nature_mutation&facet=commune&facet=section&facet=nombre_de_lots&facet=type_local&facet=nature_culture&facet=nom_reg&facet=nom_dep&facet=nom_epci&facet=libelle_nature_culture_speciale&refine.nombre_de_lots=2&refine.nature_mutation=Vente+en+l%27%C3%A9tat+futur+d%27ach%C3%A8vement&geofilter.distance=";	
	// 48.843835%2C+2.540861%2C+5000
	
	private final RestTemplate restTemplate;

	public APIValeursFoncieresService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	/**
	 * @param latitude
	 * @param longitude
	 * @return liste DVF Appartements
	 */
	public DvfSearchResultsDto searchAppartementByLatitudeLongitude(String latitude, String longitude, String rayonMetres) {
		return searchByLatitudeLongitude(latitude, longitude, rayonMetres, dvfAppartementsNearPointWebserviceUrl);
	}
	
	/**
	 * @param latitude
	 * @param longitude
	 * @return liste DVF Appartements
	 */
	public DvfSearchResultsDto searchVefaByLatitudeLongitude(String latitude, String longitude, String rayonMetres) {
		return searchByLatitudeLongitude(latitude, longitude, rayonMetres, dvfVEFANearPointWebserviceUrl);
	}

	/**
	 * @param latitude
	 * @param longitude
	 * @param naf
	 * @return
	 */
	private DvfSearchResultsDto searchByLatitudeLongitude(String latitude, String longitude, String rayonMetres, String urlDVF) {
		initProxy();
		DvfSearchResultsDto dvfSearchResultsDto = new DvfSearchResultsDto();
		// cas aucun critère, on bloque la recherche
		if (StringUtils.isEmpty(latitude)
				&& StringUtils.isEmpty(longitude)) {
			dvfSearchResultsDto.setDvfSearchResultsSummary(
					new DvfSearchResultsSummary(DvfSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
							DvfSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
			return dvfSearchResultsDto;
		}

		APIValeursFoncieresDto result = null;
		StringBuilder criteria = new StringBuilder();
		// recherche par latitude + longitude + code NAF
		//48.843835%2C+2.540861%2C+2000
		criteria.append(latitude);
		criteria.append(VIRGULE);
		criteria.append(longitude);
		criteria.append(VIRGULE);
		if(StringUtils.isNotEmpty(rayonMetres)){
			criteria.append(rayonMetres);
		}
		else{
			criteria.append(RAYON_EN_METRES_1000);
		}
		
		log.debug("DVF - methode : searchByLatitudeLongitude - identifiant : {} - request : {}", criteria.toString(),
				urlDVF + criteria.toString());

		URI url = null;
		try {
			String query = urlDVF + URLEncoder.encode(criteria.toString(), StandardCharsets.UTF_8.name());
			url = new URI(query);
		} catch (URISyntaxException e) {
			log.error("DVF - methode : searchByLatitudeLongitude - identifiant : {} - error : {}", criteria.toString(),
					e);
		} catch (UnsupportedEncodingException e) {
			log.error("DVF - methode : searchByLatitudeLongitude - identifiant : {} - error : {}", criteria.toString(),
					e);
		}
		
		ResponseEntity<APIValeursFoncieresDto> response = null;
		try {
			response = restTemplate.getForEntity(url, APIValeursFoncieresDto.class);
			// gestion du cas où aucune donnée trouvée
			if (response != null) {
				result = response.getBody();
			}
			else {
				// gestion du cas où OPENDATA non disponible
				dvfSearchResultsDto.setDvfSearchResultsSummary(
						new DvfSearchResultsSummary(DvfSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
								DvfSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
				return dvfSearchResultsDto;
			}
		} catch (RestClientException e) {
			// gestion du cas où OPENDATA non disponible

			log.error("DVF - methode : searchByLatitudeLongitude - identifiant : {} - error : {}", criteria.toString(), e);
			dvfSearchResultsDto.setDvfSearchResultsSummary(new DvfSearchResultsSummary(
					DvfSearchResultsSummary.CODE_KO_TECHNICAL, DvfSearchResultsSummary.CODE_KO_TECHNICAL));
			return dvfSearchResultsDto;
		}
		if (result.getRecords().isEmpty()) {
			dvfSearchResultsDto.setDvfSearchResultsSummary(
					new DvfSearchResultsSummary(DvfSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
							DvfSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
		}
		else {
			dvfSearchResultsDto.setListValeursFoncieres(
					result
							.getRecords()
							.stream()
							.map(record -> {
								ValeursFoncieres valeursFoncieres = ValeursFoncieres.fromRecordsDvf.apply(record);
								return valeursFoncieres;
							}).collect(Collectors.toList())
			);
			

			dvfSearchResultsDto.setDvfSearchResultsSummary(
					new DvfSearchResultsSummary(DvfSearchResultsSummary.CODE_OK, DvfSearchResultsSummary.CODE_OK));
		}

		log.debug("DVF - methode : searchByLatitudeLongitude - identifiant : {} - response : {}", criteria.toString(),
				result.getRecords().size());

		return dvfSearchResultsDto;

	}
	
	/**
	 * 
	 * Sets the proxy if one is defined
	 * 
	 */

	private void initProxy() {
		MySimpleClientHttpRequestFactory requestFactory = new MySimpleClientHttpRequestFactory(new NullHostnameVerifier());
		restTemplate.setRequestFactory(requestFactory);

//		 if (StringUtils.isNotEmpty(sireneHttpProxyHost) &&
//			 StringUtils.isNotEmpty(sireneHttpProxyPort)) {
//			
//			 initProxyHostPort(sireneHttpProxyHost, sireneHttpProxyPort);
//		
//		 }
//		
//		 if (StringUtils.isNotEmpty(sireneHttpsProxyHost) &&
//		 StringUtils.isNotEmpty(sireneHttpsProxyPort)) {
//		
//		 initProxyHostPort(sireneHttpsProxyHost, sireneHttpsProxyPort);
//		
//		 }

	}

	/**
	 * 
	 * Sets the proxy config
	 * @param host
	 * @param port
	 * 
	 */

	private void initProxyHostPort(String host, String port) {
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress(host, Integer.parseInt(port)));
		requestFactory.setProxy(proxy);
		restTemplate.setRequestFactory(requestFactory);
	}
	
	private class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
	
	public class MySimpleClientHttpRequestFactory extends SimpleClientHttpRequestFactory {

        private final HostnameVerifier verifier;

        public MySimpleClientHttpRequestFactory(HostnameVerifier verifier) {
            this.verifier = verifier;
        }

        @Override
        protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
            if (connection instanceof HttpsURLConnection) {
                ((HttpsURLConnection) connection).setHostnameVerifier(verifier);
                ((HttpsURLConnection) connection).setSSLSocketFactory(trustSelfSignedSSL().getSocketFactory());
                ((HttpsURLConnection) connection).setAllowUserInteraction(true);
            }
            super.prepareConnection(connection, httpMethod);
        }

        public SSLContext trustSelfSignedSSL() {
            try {
                SSLContext ctx = SSLContext.getInstance("TLS");
                X509TrustManager tm = new X509TrustManager() {

                    public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    }

                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                };
                ctx.init(null, new TrustManager[] { tm }, null);
                SSLContext.setDefault(ctx);
                return ctx;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

    }


    public class NullHostnameVerifier implements HostnameVerifier {
       public boolean verify(String hostname, SSLSession session) {
          return true;
       }
    }
   

}