package com.huntimmo.data.service.commune;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.huntimmo.data.dto.commune.APICommuneDto;
import com.huntimmo.data.dto.commune.Commune;
import com.huntimmo.data.dto.commune.CommuneSearchResultsDto;
import com.huntimmo.data.dto.commune.CommuneSearchResultsSummary;

@Service

public class APICommuneService {

	/** Logger */

	private final Logger log = LoggerFactory.getLogger(APICommuneService.class);

	private String communeCodeInseeWebserviceUrl = "https://public.opendatasoft.com/api/records/1.0/search//?dataset=correspondance-code-insee-code-postal&refine.insee_com="; 
		
	private String communeCodePostalWebserviceUrl = "https://public.opendatasoft.com/api/records/1.0/search//?dataset=correspondance-code-insee-code-postal&refine.postal_code=";	
	
	private String communeGeoShapeWebserviceUrl = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=contours-geographiques-des-communes-2019&facet=nom_com&facet=insee_com&facet=statut&facet=insee_arr&facet=nom_dep&facet=nom_reg&refine.insee_com=";
		
	private final RestTemplate restTemplate;

	public APICommuneService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	/**
	 * @param code
	 * @return Commune
	 */
	public CommuneSearchResultsDto searchCommuneByCodeInsee(String code) {
		CommuneSearchResultsDto searchResultsDto = searchByCode(code, communeCodeInseeWebserviceUrl);
		if(!searchResultsDto.getListCommunes().isEmpty()){
			CommuneSearchResultsDto searchGeoShapePrecisCommuneByCodeInsee = 
					searchGeoShapePrecisCommuneByCodeInsee(code);
			if(!searchGeoShapePrecisCommuneByCodeInsee.getListCommunes().isEmpty()){
				searchResultsDto.getListCommunes().get(0).
					setGeoShape(searchGeoShapePrecisCommuneByCodeInsee.getListCommunes().get(0).getGeoShape());
			}
		}
		return searchResultsDto;
	}
	
	/**
	 * @param code
	 * @return Commune
	 */
	public CommuneSearchResultsDto searchCommuneByCodePostal(String code) {
		CommuneSearchResultsDto searchResultsDto = searchByCode(code, communeCodePostalWebserviceUrl);
		if(!searchResultsDto.getListCommunes().isEmpty()){
			CommuneSearchResultsDto searchGeoShapePrecisCommuneByCodeInsee = 
					searchGeoShapePrecisCommuneByCodeInsee(searchResultsDto.getListCommunes().get(0).getInseeCom());
			if(!searchGeoShapePrecisCommuneByCodeInsee.getListCommunes().isEmpty()){
				searchResultsDto.getListCommunes().get(0).
					setGeoShape(searchGeoShapePrecisCommuneByCodeInsee.getListCommunes().get(0).getGeoShape());
			}
		}
		return searchResultsDto;
	}
	
	private CommuneSearchResultsDto searchGeoShapePrecisCommuneByCodeInsee(String code) {
		return searchByCode(code, communeGeoShapeWebserviceUrl);
	}

	/**
	 * @param latitude
	 * @param longitude
	 * @param naf
	 * @return
	 */
	private CommuneSearchResultsDto searchByCode(String code, String urlCommune) {
		initProxy();
		CommuneSearchResultsDto communeSearchResultsDto = new CommuneSearchResultsDto();
		// cas aucun critère, on bloque la recherche
		if (StringUtils.isEmpty(code)) {
			communeSearchResultsDto.setCommuneSearchResultsSummary(
					new CommuneSearchResultsSummary(CommuneSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
							CommuneSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
			return communeSearchResultsDto;
		}

		APICommuneDto result = null;
		StringBuilder criteria = new StringBuilder();
		// recherche par code
		criteria.append(code);
		
		log.debug("Commune - methode : searchByCode - identifiant : {} - request : {}", criteria.toString(),
				urlCommune + criteria.toString());

		URI url = null;
		try {
			String query = urlCommune + URLEncoder.encode(criteria.toString(), StandardCharsets.UTF_8.name());
			url = new URI(query);
		} catch (URISyntaxException e) {
			log.error("Commune - methode : searchByCode - identifiant : {} - error : {}", criteria.toString(),
					e);
		} catch (UnsupportedEncodingException e) {
			log.error("Commune - methode : searchByCode - identifiant : {} - error : {}", criteria.toString(),
					e);
		}
		
		ResponseEntity<APICommuneDto> response = null;
		try {
			response = restTemplate.getForEntity(url, APICommuneDto.class);
			// gestion du cas où aucune donnée trouvée
			if (response != null) {
				result = response.getBody();
			}
			else {
				// gestion du cas où OPENDATA non disponible
				communeSearchResultsDto.setCommuneSearchResultsSummary(
						new CommuneSearchResultsSummary(CommuneSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
								CommuneSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
				return communeSearchResultsDto;
			}
		} catch (RestClientException e) {
			// gestion du cas où OPENDATA non disponible

			log.error("Commune - methode : searchByCode - identifiant : {} - error : {}", criteria.toString(), e);
			communeSearchResultsDto.setCommuneSearchResultsSummary(new CommuneSearchResultsSummary(
					CommuneSearchResultsSummary.CODE_KO_TECHNICAL, CommuneSearchResultsSummary.CODE_KO_TECHNICAL));
			return communeSearchResultsDto;
		}
		if (result.getRecords().isEmpty()) {
			communeSearchResultsDto.setCommuneSearchResultsSummary(
					new CommuneSearchResultsSummary(CommuneSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND,
							CommuneSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND));
		}
		else {
			communeSearchResultsDto.setListCommunes(
					result
							.getRecords()
							.stream()
							.map(record -> {
								Commune commune = Commune.fromRecordsCommune.apply(record);
								return commune;
							}).collect(Collectors.toList())
			);
			

			communeSearchResultsDto.setCommuneSearchResultsSummary(
					new CommuneSearchResultsSummary(CommuneSearchResultsSummary.CODE_OK, CommuneSearchResultsSummary.CODE_OK));
		}

		log.debug("Commune - methode : searchByCode - identifiant : {} - response : {}", criteria.toString(),
				result.getRecords().size());

		return communeSearchResultsDto;

	}
	
	/**
	 * 
	 * Sets the proxy if one is defined
	 * 
	 */

	private void initProxy() {
		MySimpleClientHttpRequestFactory requestFactory = new MySimpleClientHttpRequestFactory(new NullHostnameVerifier());
		restTemplate.setRequestFactory(requestFactory);

//		 if (StringUtils.isNotEmpty(sireneHttpProxyHost) &&
//			 StringUtils.isNotEmpty(sireneHttpProxyPort)) {
//			
//			 initProxyHostPort(sireneHttpProxyHost, sireneHttpProxyPort);
//		
//		 }
//		
//		 if (StringUtils.isNotEmpty(sireneHttpsProxyHost) &&
//		 StringUtils.isNotEmpty(sireneHttpsProxyPort)) {
//		
//		 initProxyHostPort(sireneHttpsProxyHost, sireneHttpsProxyPort);
//		
//		 }

	}

	/**
	 * 
	 * Sets the proxy config
	 * @param host
	 * @param port
	 * 
	 */

	private void initProxyHostPort(String host, String port) {
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress(host, Integer.parseInt(port)));
		requestFactory.setProxy(proxy);
		restTemplate.setRequestFactory(requestFactory);
	}
	
	private class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
	
	public class MySimpleClientHttpRequestFactory extends SimpleClientHttpRequestFactory {

        private final HostnameVerifier verifier;

        public MySimpleClientHttpRequestFactory(HostnameVerifier verifier) {
            this.verifier = verifier;
        }

        @Override
        protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
            if (connection instanceof HttpsURLConnection) {
                ((HttpsURLConnection) connection).setHostnameVerifier(verifier);
                ((HttpsURLConnection) connection).setSSLSocketFactory(trustSelfSignedSSL().getSocketFactory());
                ((HttpsURLConnection) connection).setAllowUserInteraction(true);
            }
            super.prepareConnection(connection, httpMethod);
        }

        public SSLContext trustSelfSignedSSL() {
            try {
                SSLContext ctx = SSLContext.getInstance("TLS");
                X509TrustManager tm = new X509TrustManager() {

                    public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    }

                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                };
                ctx.init(null, new TrustManager[] { tm }, null);
                SSLContext.setDefault(ctx);
                return ctx;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

    }


    public class NullHostnameVerifier implements HostnameVerifier {
       public boolean verify(String hostname, SSLSession session) {
          return true;
       }
    }
   

}