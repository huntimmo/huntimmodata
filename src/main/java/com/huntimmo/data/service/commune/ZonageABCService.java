package com.huntimmo.data.service.commune;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huntimmo.data.domain.commune.ZonageABC;
import com.huntimmo.data.repository.commune.ZonageABCRepository;

@Service
public class ZonageABCService {

	@Autowired
	private ZonageABCRepository repository;

	public ZonageABC findByCodeInsee(String codeInsee){
		return repository.findByCodeinsee(codeInsee);
	}
}
