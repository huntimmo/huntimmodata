package com.huntimmo.data.controller.dvf;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.huntimmo.data.dto.common.ResponseDto;
import com.huntimmo.data.dto.dvf.DvfSearchResultsDto;
import com.huntimmo.data.dto.dvf.DvfSearchResultsSummary;
import com.huntimmo.data.dto.dvf.ValeursFoncieres;
import com.huntimmo.data.service.dvf.APIValeursFoncieresService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/v1/dvf")
@Api("DVF")
public class ValeursFoncieresController {
	
	/** Logger */
	private final Logger log = LoggerFactory.getLogger(ValeursFoncieresController.class);

	
	@Autowired
	APIValeursFoncieresService apiValeursFoncieresService;  
	
	@RequestMapping(value = "/appartements/{longitude}/{latitude}/{rayonMetres}", method = RequestMethod.GET)
	@ApiOperation(value = "findDevfByLongitudeLatitude", notes = "Renvoie les informations de valeurs foncières en Opendata (DVF) selon le rayon indiqué (en mètres) ", response = ValeursFoncieres.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findDvfAppartementsByLongitudeLatitudeRayon(@PathVariable("longitude") String longitude,
			@PathVariable("latitude") String latitude, @PathVariable("rayonMetres") String rayonMetres) {

		log.debug("findDvfAppartementsByLongitudeLatitude - params : {} ", longitude, latitude, rayonMetres);
		DvfSearchResultsDto dvfSearchResultsDto = searchAppartements(longitude, latitude, rayonMetres);

		return executeDvfSearch(longitude, latitude, dvfSearchResultsDto);
	}

	@RequestMapping(value = "/appartements/{longitude}/{latitude}", method = RequestMethod.GET)
	@ApiOperation(value = "findDevfByLongitudeLatitude", notes = "Renvoie les informations de valeurs foncières en Opendata (DVF) ", response = ValeursFoncieres.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findDvfAppartementsByLongitudeLatitude(@PathVariable("longitude") String longitude,
			@PathVariable("latitude") String latitude) {

		log.debug("findDvfAppartementsByLongitudeLatitude - params : {} ", longitude, latitude);
		
		DvfSearchResultsDto dvfSearchResultsDto = searchAppartements(longitude, latitude, null);

		return executeDvfSearch(longitude, latitude, dvfSearchResultsDto);
	}

	@RequestMapping(value = "/vefa/{longitude}/{latitude}/{rayonMetres}", method = RequestMethod.GET)
	@ApiOperation(value = "findDevfByLongitudeLatitude", notes = "Renvoie les informations de valeurs foncières en Opendata (DVF) selon le rayon indiqué (en mètres) ", response = ValeursFoncieres.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findDvfVefaByLongitudeLatitudeRayon(@PathVariable("longitude") String longitude,
			@PathVariable("latitude") String latitude, @PathVariable("rayonMetres") String rayonMetres) {

		log.debug("findDvfVefaByLongitudeLatitude - params : {} ", longitude, latitude, rayonMetres);
		
		DvfSearchResultsDto dvfSearchResultsDto = searchVefa(longitude, latitude, rayonMetres);

		return executeDvfSearch(longitude, latitude, dvfSearchResultsDto);
	}

	@RequestMapping(value = "/vefa/{longitude}/{latitude}", method = RequestMethod.GET)
	@ApiOperation(value = "findDevfByLongitudeLatitude", notes = "Renvoie les informations de valeurs foncières en Opendata (DVF)", response = ValeursFoncieres.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findDvfVefaByLongitudeLatitude(@PathVariable("longitude") String longitude,
			@PathVariable("latitude") String latitude) {

		log.debug("findDvfVefaByLongitudeLatitude - params : {} ", longitude, latitude);
		
		DvfSearchResultsDto dvfSearchResultsDto = searchVefa(longitude, latitude, null);

		return executeDvfSearch(longitude, latitude, dvfSearchResultsDto);
	}
	
	/**
	 * @param longitude
	 * @param latitude
	 * @param rayonMetres
	 * @return
	 */
	private DvfSearchResultsDto searchAppartements(String longitude, String latitude, String rayonMetres) {
		rayonMetres = checkRayon(rayonMetres);
		
		DvfSearchResultsDto dvfSearchResultsDto = apiValeursFoncieresService.searchAppartementByLatitudeLongitude(latitude, longitude, rayonMetres);
		return dvfSearchResultsDto;
	}

	/**
	 * @param longitude
	 * @param latitude
	 * @param rayonMetres
	 * @return
	 */
	private DvfSearchResultsDto searchVefa(String longitude, String latitude, String rayonMetres) {
		rayonMetres = checkRayon(rayonMetres);
		
		DvfSearchResultsDto dvfSearchResultsDto = apiValeursFoncieresService.searchVefaByLatitudeLongitude(latitude, longitude, rayonMetres);
		return dvfSearchResultsDto;
	}

	/**
	 * @param rayonMetres
	 * @return
	 */
	private String checkRayon(String rayonMetres) {
		if(StringUtils.isNotEmpty(rayonMetres)){
			if(!StringUtils.isNumeric(rayonMetres)){
				rayonMetres = null;
			}
			else{
				if(Integer.valueOf(rayonMetres) > 5000){
					// maximum 5000 mètres
					rayonMetres = "5000";
				}
			}
		}
		return rayonMetres;
	}
	

	/**
	 * @param longitude
	 * @param latitude
	 * @param dvfSearchResultsDto
	 * @return
	 */
	private ResponseEntity<ResponseDto<?>> executeDvfSearch(String longitude, String latitude,
			DvfSearchResultsDto dvfSearchResultsDto) {
		ResponseDto<DvfSearchResultsDto> responseDto = new ResponseDto<DvfSearchResultsDto>(dvfSearchResultsDto, 
				dvfSearchResultsDto.getDvfSearchResultsSummary().getCode(), dvfSearchResultsDto.getDvfSearchResultsSummary().getMessage());

		log.debug("findDevfByLongitudeLatitude - params : {} - results : {} ", longitude+","+latitude, dvfSearchResultsDto);
		
		if(DvfSearchResultsSummary.CODE_OK.equals(dvfSearchResultsDto.getDvfSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else if(DvfSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND.equals(dvfSearchResultsDto.getDvfSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);	
		} else if(DvfSearchResultsSummary.CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS.equals(dvfSearchResultsDto.getDvfSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else{
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
	}

}