package com.huntimmo.data.controller.insee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.huntimmo.data.domain.insee.Indicateurs;
import com.huntimmo.data.dto.common.ResponseDto;
import com.huntimmo.data.dto.commune.CommuneSearchResultsDto;
import com.huntimmo.data.dto.dvf.DvfSearchResultsDto;
import com.huntimmo.data.dto.dvf.DvfSearchResultsSummary;
import com.huntimmo.data.dto.dvf.ValeursFoncieres;
import com.huntimmo.data.repository.insee.IndicateursRepository;
import com.huntimmo.data.service.commune.APICommuneService;
import com.huntimmo.data.service.dvf.APIValeursFoncieresService;
import com.huntimmo.data.service.insee.IndicateursService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/v1/indicateursInsee")
@Api("DVF")
public class IndicateursInseeController {
	
	/** Logger */
	private final Logger log = LoggerFactory.getLogger(IndicateursInseeController.class);

	
	@Autowired
	IndicateursService indicateursService;  
	
	@Autowired
	APICommuneService apiCommuneService; 
	
	@RequestMapping(value = "/{codePostal}", method = RequestMethod.GET)
	@ApiOperation(value = "findIndicateurs", notes = "Renvoie les indicateurs Insee par codePostal", response = Indicateurs.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findIndicateurs(@PathVariable("codePostal") String codePostal) {

		log.debug("findIndicateurs - codePostal : {} ", codePostal);
		
		Indicateurs indicateurs = null; 
		ResponseDto<Indicateurs> responseDto = null;
				
		CommuneSearchResultsDto commune = apiCommuneService.searchCommuneByCodePostal(codePostal);
		
		if(commune.getListCommunes().isEmpty()){
			responseDto = new ResponseDto<Indicateurs>(indicateurs);
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
		
		indicateurs = indicateursService.findIndicateur(commune.getListCommunes().get(0).getInseeCom()); 
		responseDto = new ResponseDto<Indicateurs>(indicateurs);
		
		if(indicateurs != null){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else{
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
	}

}