package com.huntimmo.data.controller.sirene;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.huntimmo.data.dto.bde.BdeSearchCriteriaDto;
import com.huntimmo.data.dto.bde.BdeSearchResultsDto;
import com.huntimmo.data.dto.bde.BdeSearchResultsSummary;
import com.huntimmo.data.dto.bde.EnterpriseDto;
import com.huntimmo.data.dto.common.ResponseDto;
import com.huntimmo.data.dto.sirene.Etablissements;
import com.huntimmo.data.service.sirene.APISireneService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/v1/sirene")
@Api("SIRENE")
public class SireneController {
	
	/** Logger */
	private final Logger log = LoggerFactory.getLogger(SireneController.class);

	
	@Autowired
	APISireneService apiSireneService;  
	
	/**
	 * @param searchCriteriaDto
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "Technical error") 
			})
    public ResponseEntity<ResponseDto<?>> findEtablissementsByCriteria(@RequestBody BdeSearchCriteriaDto searchCriteriaDto) {
		log.debug("Find Etablissements by criteria {} ",searchCriteriaDto);
		
		BdeSearchResultsDto findEtablissementsByCriteria = apiSireneService.searchByCriteria(searchCriteriaDto);
		
		ResponseDto<BdeSearchResultsDto> responseDto = new ResponseDto<BdeSearchResultsDto>(findEtablissementsByCriteria, 
				findEtablissementsByCriteria.getBdeSearchResultsSummary().getCode(), findEtablissementsByCriteria.getBdeSearchResultsSummary().getMessage());

		log.debug("Find Etablissements by criteria results : {} ",responseDto);
		
		if(BdeSearchResultsSummary.CODE_OK.equals(findEtablissementsByCriteria.getBdeSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else if(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND.equals(findEtablissementsByCriteria.getBdeSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);	
		} else if(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS.equals(findEtablissementsByCriteria.getBdeSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else{
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
		
	}

	@RequestMapping(value = "/{siret}", method = RequestMethod.GET)
	@ApiOperation(value = "getLegalEntity", notes = "Renvoie les informations recherchées d'un établissement en Opendata (SIRENE) ou BDE", response = EnterpriseDto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> getLegalEntity(@PathVariable("siret") String siret) {

		log.debug("getEtablissement - siret : {} ", siret);
		
		ResponseDto<EnterpriseDto> responseDto = apiSireneService.searchBySiret(siret);
		
		log.debug("getEtablissement - siret : {} - results : {} ", siret, responseDto);
		
		if(BdeSearchResultsSummary.CODE_OK.equals(responseDto.getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else if(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND.equals(responseDto.getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);	
		} else if(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS.equals(responseDto.getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else{
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/{longitude}/{latitude}/{naf}", method = RequestMethod.GET)
	@ApiOperation(value = "getLegalEntity", notes = "Renvoie les informations recherchées d'un établissement en Opendata (SIRENE)", response = Etablissements.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findEtablissementsByLongitudeLatitudeNafCode(@PathVariable("longitude") String longitude,
			@PathVariable("latitude") String latitude, @PathVariable("naf") String naf) {

		log.debug("findEtablissementsByLongitudeLatitudeNafCode - params : {} ", longitude, latitude, naf);
		
		BdeSearchResultsDto bdeSearchResultsDto = apiSireneService.searchByLatitudeLongitudeAndCodeNaf(latitude, longitude, naf);
		
		log.debug("getEtablissement - naf : {} - results : {} ", naf, bdeSearchResultsDto);
		
		ResponseDto<BdeSearchResultsDto> responseDto = new ResponseDto<BdeSearchResultsDto>(bdeSearchResultsDto, 
				bdeSearchResultsDto.getBdeSearchResultsSummary().getCode(), bdeSearchResultsDto.getBdeSearchResultsSummary().getMessage());

		log.debug("Find Etablissements by criteria results : {} ",responseDto);
		
		if(BdeSearchResultsSummary.CODE_OK.equals(bdeSearchResultsDto.getBdeSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else if(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND.equals(bdeSearchResultsDto.getBdeSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);	
		} else if(BdeSearchResultsSummary.CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS.equals(bdeSearchResultsDto.getBdeSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else{
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
	}


}