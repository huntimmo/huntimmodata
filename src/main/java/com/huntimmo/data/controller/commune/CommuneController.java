package com.huntimmo.data.controller.commune;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.huntimmo.data.domain.commune.ZonageABC;
import com.huntimmo.data.domain.insee.Indicateurs;
import com.huntimmo.data.dto.common.ResponseDto;
import com.huntimmo.data.dto.commune.Commune;
import com.huntimmo.data.dto.commune.CommuneSearchResultsDto;
import com.huntimmo.data.dto.commune.CommuneSearchResultsSummary;
import com.huntimmo.data.service.commune.APICommuneService;
import com.huntimmo.data.service.commune.ZonageABCService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/v1/commune")
@Api("COMMUNE")
public class CommuneController {
	
	/** Logger */
	private final Logger log = LoggerFactory.getLogger(CommuneController.class);

	
	@Autowired
	APICommuneService apiCommuneService;
	
	@Autowired
	ZonageABCService zonageABCService;
	
	@RequestMapping(value = "/zonageABC/{codePostal}", method = RequestMethod.GET)
	@ApiOperation(value = "findIndicateurs", notes = "Renvoie le zonage ABC par codePostal", response = ZonageABC.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findZonageABC(@PathVariable("codePostal") String codePostal) {

		log.debug("findZonageABC - codePostal : {} ", codePostal);
		
		ZonageABC zonageABC = null; 
		ResponseDto<ZonageABC> responseDto = null;
				
		CommuneSearchResultsDto commune = apiCommuneService.searchCommuneByCodePostal(codePostal);
		
		if(commune.getListCommunes().isEmpty()){
			responseDto = new ResponseDto<ZonageABC>(zonageABC);
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
		
		zonageABC = zonageABCService.findByCodeInsee(commune.getListCommunes().get(0).getInseeCom()); 
		responseDto = new ResponseDto<ZonageABC>(zonageABC);
		
		if(zonageABC != null){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else{
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
	}
  
	
	@RequestMapping(value = "/codeInsee/{codeInsee}", method = RequestMethod.GET)
	@ApiOperation(value = "findByCodeInsee", notes = "Renvoie les informations de commune en Opendata", response = Commune.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findByCodeInsee(@PathVariable("codeInsee") String codeInsee) {

		log.debug("findByCodeInsee - codeInsee : {} ", codeInsee);
		
		CommuneSearchResultsDto communeSearchResultsDto = apiCommuneService.searchCommuneByCodeInsee(codeInsee);

		return getCommuneSearch(codeInsee, communeSearchResultsDto);
	}

	@RequestMapping(value = "/codePostal/{codePostal}", method = RequestMethod.GET)
	@ApiOperation(value = "findByCodePostal", notes = "Renvoie les informations de commune en Opendata", response = Commune.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK, KO_FUNCTIONNAL_NO_RESULT_FOUND, KO_FUNCTIONNAL_TOO_MANY_RESULTS"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "Technical error") 
			})
	public ResponseEntity<ResponseDto<?>> findByCodePostal(@PathVariable("codePostal") String codePostal) {

		log.debug("findByCodePostal - codePostal : {} ", codePostal);
		
		CommuneSearchResultsDto communeSearchResultsDto = apiCommuneService.searchCommuneByCodePostal(codePostal);

		return getCommuneSearch(codePostal, communeSearchResultsDto);
	}

	/**
	 * @param code
	 * @param communeSearchResultsDto
	 * @return
	 */
	private ResponseEntity<ResponseDto<?>> getCommuneSearch(String code, CommuneSearchResultsDto communeSearchResultsDto) {
		ResponseDto<CommuneSearchResultsDto> responseDto = new ResponseDto<CommuneSearchResultsDto>(communeSearchResultsDto, 
				communeSearchResultsDto.getCommuneSearchResultsSummary().getCode(), communeSearchResultsDto.getCommuneSearchResultsSummary().getMessage());

		log.debug("findDevfByLongitudeLatitude - params : {} - results : {} ", code, communeSearchResultsDto);
		
		if(CommuneSearchResultsSummary.CODE_OK.equals(communeSearchResultsDto.getCommuneSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else if(CommuneSearchResultsSummary.CODE_KO_FUNCTIONNAL_NO_RESULT_FOUND.equals(communeSearchResultsDto.getCommuneSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);	
		} else if(CommuneSearchResultsSummary.CODE_KO_FUNCTIONNAL_TOO_MANY_RESULTS.equals(communeSearchResultsDto.getCommuneSearchResultsSummary().getCode())){
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.OK);	
		} else{
			return new ResponseEntity<ResponseDto<?>>(
					responseDto,
					HttpStatus.NOT_FOUND);
		}
	}

}