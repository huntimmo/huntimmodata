package com.huntimmo.data.repository.insee;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.huntimmo.data.domain.insee.Indicateurs;

public interface IndicateursRepository extends MongoRepository<Indicateurs, String> {

	public Indicateurs findByCODGEO(String codgeo);

}
