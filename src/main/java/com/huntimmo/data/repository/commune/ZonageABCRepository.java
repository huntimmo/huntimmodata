package com.huntimmo.data.repository.commune;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.huntimmo.data.domain.commune.ZonageABC;

public interface ZonageABCRepository extends MongoRepository<ZonageABC, String> {

	public ZonageABC findByCodeinsee(String codeInsee);

}
