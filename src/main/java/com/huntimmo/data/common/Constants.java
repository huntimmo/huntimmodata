package com.huntimmo.data.common;


//TODO to remplace by Constantes.java
@Deprecated
public interface Constants {

	// Status
	String STATUS_ACTIVE = "ACTIVE";
	String STATUS_INACTIVE = "INACTIVE";

	String BAREME_ACTIF = "A";
	String BAREME_INACTIF = "I";
	
	String ADMIN = "ROLE_ADMIN";
    String ANALYSTE = "ROLE_ANALYSTE";
    String DIRECTOR = "ROLE_DIRECTOR";     
    String COMMERCIAL = "ROLE_COMMERCIAL"; 
    String ANONYMOUS = "ROLE_ANONYMOUS";
    String USER = "ROLE_USER";

	// Fournisseur
	String CONVENTION_TYPE_MANDAT = "MANDAT";

	// Moteur de scoring
	int ONE_YEAR = 12; // en mois
	int THREE_YEAR = 36; // en mois
	int MONTH_22 = 22;

	String[] FORME_JURIDIQUE = {"1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", 
		"1800", "1900", "5202", "5203", "6561", "6562", "6563", "6564", "6565", "6566", "6567", "6568", "6569", "6571", 
		"6572", "6573", "6574", "6575", "6576", "6577", "6578", "6585"};

	String MATERIAL_CAT_MEDICAL = "MEDICAL";
	String MATERIAL_COTE_A = "A";
	String MATERIAL_COTE_B = "B";
	String MATERIAL_QUALITY_PREMIUM = "PREMIUM";
	String MATERIAL_QUALITY_PREMIUM_PLUS = "PREMIUM+";
	int MATERIAL_VR_MIN = 1;
	int MATERIAL_DUREE_PRECONIZED = 48;
	int MATERIAL_DUREE_MIN = 36;

	String[] COTATION_FIBEN = {"6", "7", "8", "9", "P"};
	String[] COTATION_BDF_DEGRADEE = {"5", "6", "7", "8", "9", "P"};
	String COTATION_3PLUSPLUS = "3++";
	String COTATION_3PLUS = "3+";
	String COTATION_3 = "3";
	String COTATION_4PLUS = "4+";
	String COTATION_4 = "4";
	String COTATION_5PLUS = "5+";
	String COTATION_ZERO = "000";

	String[] NOTATION_COFACE_DEGRADEE = {"1", "2", "3"};
	String COFACE_1 = "1";
	String COFACE_2 = "2";
	String COFACE_3 = "3";
	String COFACE_4 = "4";
	String COFACE_5 = "5";
	String COFACE_6 = "6";
	String COFACE_7 = "7";
	String COFACE_8 = "8";
	String COFACE_9 = "9";
	String COFACE_10 = "10";

	String[] NAF_CODE_MEDECIN = {"8621Z", "8622A", "8622B", "8622C", "8623Z"};
	String NAF_CODE_GENERALISTE = "8621Z";
	String NAF_CODE_RADIOLOGUE = "8622A";
	String NAF_CODE_CHIRURGIEN = "8622B";
	String NAF_CODE_SPECIALISTE = "8622C";
	String NAF_CODE_DENTISTE = "8623Z";
	String NAF_CODE_VETERINAIRE = "7500Z";
	String[] NAF_CODE_SENSIBLE = {"2013A", "2051Z", "2441Z", "2446Z", "2540Z", "3040Z", "3811Z", "3812Z", "3821Z", "3822Z", "3831Z", "3832Z", "4677Z", 
			"8010Z", "8899B", "9200Z", "9491Z", "9492Z", "9499Z", "9900Z"};
	
	String MOTIF_IMPAYES_NT = "NT";

	String IDENTIFIANT_TYPE_PP_BDE = "BF";
	String IDENTIFIANT_TYPE_ENTREPRISE_BDE = "SI";
	String TYPE_PP = "PART";
	String TYPE_PM = "PM";
	
	String[] PAS_DE_PC = {"NON", "?", "NORM", "N"}; // Pas de procédure collective
	String[] EASY_RISK_CODE_REFUS = {"17", "20", "21", "22"}; // Liste des codes Refus EasyRisk


	String[] MOTEURS_NOTATION = {"NIE", "TRR", "NIO"};
	String MOTEUR_NOTATION_TRR = "TRR";
	String MOTEUR_NOTATION_NIO = "NIO";
	String MOTEUR_NOTATION_NIE = "NIE";
	String NOTATION_GROUPE_R_1_3_C_1_8 = "NOTATION_GROUPE_R_1_3_C_1_8";
	String NOTATION_GROUPE_R_4_5_C_9_12 = "NOTATION_GROUPE_R_4_5_C_9_12";
	String NOTATION_GROUPE_R_6PLUS_C_13PLUS = "NOTATION_GROUPE_R_6PLUS_C_13PLUS";

	String[] NOTATION_ACCEPTED = {"NIE11", "NIE12", "NIO7"};

	String COURT_TERME = "CT";
	String MOYEN_LONG_TERME = "ML";
	String CREDIT_BAIL = "CB";

	String VENDOR_QUALIF_A = "A";
	String VENDOR_QUALIF_B = "B";
	String VENDOR_QUALIF_C = "C";

	String EASY_RISK_STATUS_REFUS = "REFUS";

	String ALERT_CLIENT_DOUTEUX = "Client douteux";
	String ALERT_IMPAYES = "Impayés NL";
	String ALERT_PROC_COLLECTIVE = "Procédure collective";
	String ALERT_CESSATION_ACTIVITE = "Cessation d'activité";
	String ALERT_DATE_CREA_INF_UN_AN = "Date de création inférieure à 12 mois";
	String ALERT_LAT = "Alerte LAT";
	String ALERT_WATCH_LIST = "Alerte Watch list";
	String ALERT_BDF_DEGRADEE = "Cotation BDF dégradée";
	String ALERT_IMPAYES_BDF = "Impayés BDF";
	String ALERT_CAF_NEGATIVE = "CAF négative";
	String ALERT_DATE_CLOTURE_SUP_22_MOIS_OU_VIDE = "Date de clôture des comptes supérieure à 22 mois ou vide";
	String ALERT_EASY_RISK = "Alerte EasyRisk";
	String ALERT_RESID_HORS_FR = "Pays résidence hors france";
	String ALERT_ACT_SENSIBLE = "Activité sensible";
	String ALERT_FONDS_PROPRES_NEGATIFS = "Fonds propres négatifs";
	String ALERT_NOTATION_DEGRADEE = "Notation dégradée";

	String PAYS_FRANCE = "FRANCE";

	// Utils
	String SIREN_NULL = "000000000";
	String MONTANT_UNITE_MILLION = "M";
	String CHAINE_VIDE = " ";
	String SIREN_NATIXIS_LEASE = "379155369";

	String RELATION_TYPE_DIRIGEANT = "DIRIGEANT";
	String RELATION_TYPE_ACTIONNAIRE = "ACTIONNAIRE";
	String RELATION_TYPE_PARTICIPATION = "PARTICIPATION";

	String TTC = "TTC";
	// Contrats
	String CONTRACT_WAITING_SIGNATURE = "CONTRACT_WAITING_SIGNATURE";

	// Simulateur
	String SIMULATOR_BY_AMOUNT = "vpm";

	/**
	 * CONSTANTE UTILISEE POUR IDENTIFIER LES REQUETES HTTP
	 */
	String FILTER_REQUESTHTTP_ID = "REQUESTHTTPID";

	String DEFAULT_MATERIAL_CATEGORY = "default";
	
	String NUMBER_PATTERN_FOR_LETTRE = "#.00";
}
