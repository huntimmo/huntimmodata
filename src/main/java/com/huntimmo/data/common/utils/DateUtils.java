package com.huntimmo.data.common.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Classe de fonctions pour les dates.
 * 
 * @author delobelleju
 * 
 */
public class DateUtils {
	/**
	 * Logger
	 */
	private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);
	static final long ONE_MINUTE_IN_MILLIS=60000;//millisecs

	/**
	 * Retourne une date dont les heures, minutes et secondes sont � 0.
	 * La date ne contient plus d'informations sur les heures.
	 * Cette date est utilisee pour les requetes sur les dates de debut.
	 * @param aDateDebut la date a convertir
	 * @return la date sans les heures
	 */
	public final static Date convertirDateDebut(Date aDateDebut){
		// Si la date est null, retourne null
		if(aDateDebut == null){
			return null;
		}
		
		// Instancie un gregorianClaendar
		GregorianCalendar calendier = new GregorianCalendar();
		// Met le gregorianCalendar � 0 pour les Heures, Minutes, Secondes et Millisecondes
		calendier.setTime(aDateDebut);
		calendier.set(GregorianCalendar.HOUR_OF_DAY, 0);
		calendier.set(GregorianCalendar.MINUTE, 0);
		calendier.set(GregorianCalendar.SECOND, 0);
		calendier.set(GregorianCalendar.MILLISECOND, 0);
		// Retourne la date
		return calendier.getTime();
	}
	
	
	
	/**
	 * Retourne une date dont les minutes et secondes sont � 0.
	 * La date ne contient plus d'informations sur les minutes.
	 * Cette date est utilisee pour les requetes sur les heures de debut.
	 * @param aDateDebut la date a convertir
	 * @return la date sans les minutes
	 */
	public final static Date convertirHeureDebut(Date aDateDebut){
		// Si la date est null, retourne null
		if(aDateDebut == null){
			return null;
		}
		
		// Instancie un gregorianClaendar
		GregorianCalendar calendier = new GregorianCalendar();
		// Met le gregorianCalendar � 0 pour les Minutes, Secondes et Millisecondes
		calendier.setTime(aDateDebut);
		calendier.set(GregorianCalendar.MINUTE, 0);
		calendier.set(GregorianCalendar.SECOND, 0);
		calendier.set(GregorianCalendar.MILLISECOND, 0);
		// Retourne la date
		return calendier.getTime();
	}
	
	/**
	 * Retourne une date dont les secondes sont � 0.
	 * La date ne contient plus d'informations sur les secondes.
	 * Cette date est utilisee pour les requetes sur les minutes de debut.
	 * @param aDateDebut la date a convertir
	 * @return la date sans les heures
	 */
	public final static Date convertirMinuteDebut(Date aDateDebut){
		// Si la date est null, retourne null
		if(aDateDebut == null){
			return null;
		}
		
		// Instancie un gregorianClaendar
		GregorianCalendar calendier = new GregorianCalendar();
		// Met le gregorianCalendar � 0 pour les Secondes et Millisecondes
		calendier.setTime(aDateDebut);
		calendier.set(GregorianCalendar.SECOND, 0);
		calendier.set(GregorianCalendar.MILLISECOND, 0);
		// Retourne la date
		return calendier.getTime();
	}

	/**
	 * Retourne une date dont l'heure est la derniere de la journee.
	 * Cette date est utilisee pour les requetes sur les dates de fin.
	 * @param aDateFin la date a convertir
	 * @return la date avec la derni�re heure de la journee
	 */
	public final static Date convertirDateFin(Date aDateFin){
		// Si la date est null, retourne null
		if(aDateFin == null){
			return null;
		}
		
		// Instancie un gregorianClaendar
		GregorianCalendar calendier = new GregorianCalendar();
		// Met le gregorianCalendar � 0 pour les Heures, Minutes, Secondes et Millisecondes
		calendier.setTime(aDateFin);
		calendier.set(GregorianCalendar.HOUR_OF_DAY, 23);
		calendier.set(GregorianCalendar.MINUTE, 59);
		calendier.set(GregorianCalendar.SECOND, 59);
		calendier.set(GregorianCalendar.MILLISECOND, 999);
		// Retourne la date
		return calendier.getTime();
	}
	
	/**
	 * Retourne une date dont la minute est la derni�re de l'heure.
	 * Cette date est utilisee pour les requetes sur les dates de fin.
	 * @param aDateFin la date a convertir
	 * @return la date avec la derni�re heure de la journee
	 */
	public final static Date convertirHeureFin(Date aDateFin){
		// Si la date est null, retourne null
		if(aDateFin == null){
			return null;
		}
		
		// Instancie un gregorianClaendar
		GregorianCalendar calendier = new GregorianCalendar();
		// Met le gregorianCalendar � 0 pour les Minutes, Secondes et Millisecondes
		calendier.setTime(aDateFin);
		calendier.set(GregorianCalendar.MINUTE, 59);
		calendier.set(GregorianCalendar.SECOND, 59);
		calendier.set(GregorianCalendar.MILLISECOND, 999);
		// Retourne la date
		return calendier.getTime();
	}
	
	/**
	 * Retourne une date dont la seconde est la derniere de la minutes.
	 * Cette date est utilisee pour les requetes sur la derni�re seconde de la minute.
	 * @param aDateFin la date a convertir
	 * @return la date avec la derni�re seconde de la minute
	 */
	public final static Date convertirMinuteFin(Date aDateFin){
		// Si la date est null, retourne null
		if(aDateFin == null){
			return null;
		}
		
		// Instancie un gregorianClaendar
		GregorianCalendar calendier = new GregorianCalendar();
		// Met le gregorianCalendar � 0 pour les Secondes et Millisecondes
		calendier.setTime(aDateFin);
		calendier.set(GregorianCalendar.SECOND, 59);
		calendier.set(GregorianCalendar.MILLISECOND, 999);
		// Retourne la date
		return calendier.getTime();
	}
	
	/**
	 * Retourne vrai si la date1 est sup�rieure ou egale � la date2
	 * 
	 * Si une des dates est null, retourne false
	 * 
	 * @param aDate1
	 * @param aDate2
	 * @return boolean
	 */
	public final static boolean dateSuperieureOuEgale(Date aDate1, Date aDate2){
		// Si une des dates est null, retourne false
		if(aDate1 == null || aDate2 == null){
			return false;
		}
		
		return !aDate1.before(aDate2);
	}
	
	
	/**
	 * Retourne vrai si la date1 est inf�rieure ou egale � la date2
	 * 
	 * Si une des dates est null, retourne false
	 * 
	 * @param aDate1
	 * @param aDate2
	 * @return boolean
	 */
	public final static boolean dateInferieureOuEgale(Date aDate1, Date aDate2){
		// Si une des dates est null, retourne false
		if(aDate1 == null || aDate2 == null){
			return false;
		}
		
		return !aDate1.after(aDate2);
	}

	/**
	 * Retourne vrai si a date est inclus dans aDateBorneInf <-> aDateBorneSup
	 * 
	 * Si une des dates est null, retourne false
	 * 
	 * @param aDate
	 * @param aDateBorneInf
	 * @param aDateBorneSup
	 * @return boolean
	 */
	public final static boolean dateIncluse(Date aDate, Date aDateBorneInf, Date aDateBorneSup){
		// Si une des dates est null, retourne false
		if(aDate == null || aDateBorneInf == null || aDateBorneSup == null){
			return false;
		}
		
		return dateSuperieureOuEgale(aDate, aDateBorneInf) && DateUtils.dateInferieureOuEgale(aDate, aDateBorneSup);
	}
	
	
	/**
	 * Formatte la date en param�tre suivant le pattern pass� lui aussi en param�tre
	 * 
	 * @param aPattern Pattern
	 * @param aDate Date
	 * @return String
	 */
	public final static String formatDate(String aPattern, Date aDate) {
		String lStringDate = null;
		if (StringUtils.isNotEmpty(aPattern) && aDate != null) {
			// Date Format
			SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat(aPattern, Locale.FRENCH);
			// Retourne le format
			lStringDate = lSimpleDateFormat.format(aDate);
		}
		return lStringDate;
	}
	
	public final static String formatDateString(String aPattern, String aDate) {
		String lStringDate = null;
		try {
			Date date = parseDate("yyyy-MM-dd", aDate);
			if (StringUtils.isNotEmpty(aPattern) && date != null) {
				// Date Format
				SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat(aPattern, Locale.FRENCH);
				// Retourne le format
				lStringDate = lSimpleDateFormat.format(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return lStringDate;
	}
	
	/**
	 * Formatte la date en param�tre suivant le pattern pass� lui aussi en param�tre
	 * 
	 * @param aPattern Pattern
	 * @param aDate Date
	 * @return String
	 * @throws ParseException 
	 */
	public final static Date parseDate(String aPattern, String aDate) throws ParseException {
		Date d = null;
		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat(aPattern,Locale.FRENCH);
		d = lSimpleDateFormat.parse(aDate);
		return d;
	}
		
	
	/**
	 * Verifie le format de la date en fonction du pattern pass� en param 
	 * 
	 * @param aStrDate String � transform� 
	 * @param aDatePattern 
	 * @return Date si ok null si Ko
	 */
	public final static Date verifierformatDateEtHeure(String aStrDate, String aDatePattern){
		// Instancie un SimpleDateFormat
		SimpleDateFormat lSdf = new SimpleDateFormat(aDatePattern, Locale.FRENCH);
		lSdf.setLenient(false);
		try {
			return lSdf.parse(aStrDate);
		} catch (java.text.ParseException e) {
			// Le parse c'est pas bien pass�, on retourne null
			logger.error("La date '"+aStrDate+"' ne peut �tre parser avec le pattern '"+aDatePattern+"'.");
			return null;
		}
	}
	
    /**
     * <p>Checks if two dates are on the same day ignoring time.</p>
     * @param date1  the first date, not altered, not null
     * @param date2  the second date, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either date is <code>null</code>
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }
	
    /**
     * <p>Checks if two calendars represent the same day ignoring time.</p>
     * @param cal1  the first calendar, not altered, not null
     * @param cal2  the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is <code>null</code>
     */
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
    
    /**
     * <p>Checks if a date is today.</p>
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    /**
     * Ajouter des mois � une date
     * @param date
     * @param monthNumber
     * @return
     */
	public static Date addMonthToDate(Date date, int monthNumber) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, monthNumber);
		return cal.getTime();
	}
	
	public static Date addDaysToDate(Date date, int dayNumber) {
	    return org.apache.commons.lang3.time.DateUtils.addDays(date, dayNumber);
	}

	public static Date addMinuteToDate(Date date, int minuteNumber) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		long t= cal.getTimeInMillis();
		Date afterAddingMinutes=new Date(t + (minuteNumber * ONE_MINUTE_IN_MILLIS));
		
		return afterAddingMinutes;
	}
	
	/**
	 * Méthode qui effectue la conversion d'un Timestamp en XMLGregorianCalendar
	 * 
	 * @param aSource: source en Timestamp à convertir en XMLGregorianCalendar
	 * @return la source convertie au format XMLGregorianCalendar
	 */
	public static XMLGregorianCalendar convertToXMLGregorianCalendar(Timestamp aSource){
		if(aSource != null) {
			// Si la source passée en paramètre n'est pas nulle
			//DateFormat lDf = new SimpleDateFormat(PATTERN_KSIOP);
			GregorianCalendar lGCalendar = new GregorianCalendar();
			XMLGregorianCalendar lXMLCal = null;
			lGCalendar.setTime(aSource);
			try {
				//Conversion de la source de type Timestamp en XMLGregorianCalendar
				//lXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(lDf.format(aSource));
				lXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(lGCalendar);
			} catch (DatatypeConfigurationException e) {
				logger.warn(e.getMessage());
			}
			return lXMLCal;
		}
		return null;
	}
}