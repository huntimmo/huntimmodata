package com.huntimmo.data.common.utils;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Binding;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;

/**
 * Logger SOAP
 * 
 */
public class SOAPLogger implements SOAPHandler<SOAPMessageContext> {

	/** The logger. */
	private static Logger logger = LoggerFactory.getLogger(SOAPLogger.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.
	 * MessageContext)
	 */
	@Override
	public boolean handleMessage(final SOAPMessageContext context) {
		logToSystemOut(context);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext
	 * )
	 */
	@Override
	public boolean handleFault(final SOAPMessageContext context) {
		logToSystemOut(context);
		return true;
	}

	/**
	 * Log to system out.
	 * 
	 * @param smc
	 *            the smc
	 */
	private void logToSystemOut(final SOAPMessageContext smc) {
		final Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (outboundProperty.booleanValue()) {
			// Ajout du Header
			addHeader(smc);

			// Ajoute le namespace
			addNamespace(smc);

			logger.info("FLUX SORTANT : ");
		} else {
			logger.info("FLUX ENTRANT : ");
		}

		final SOAPMessage message = smc.getMessage();
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			message.writeTo(baos);
			logger.info(baos.toString());
		} catch (final Exception e) {
			logger.error("Exception in handler: " + e);
		}
	}

	/**
	 * Ajoute un header vide
	 * 
	 * @param smc
	 */
	private void addHeader(final SOAPMessageContext smc) {
		try {
			if (smc.getMessage().getSOAPHeader() == null) {
				smc.getMessage().getSOAPPart().getEnvelope().addHeader();
			}
		} catch (SOAPException e) {
			logger.error("Impossible d'ajouter le header vide : " + e);
		}
	}

	/**
	 * Ajoute un header vide
	 * 
	 * @param smc
	 */
	private void addNamespace(final SOAPMessageContext smc) {
		try {
			if (smc.getMessage().getSOAPPart() != null && smc.getMessage().getSOAPPart().getEnvelope() != null
					&& smc.getMessage().getSOAPPart().getEnvelope().getAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:soap") == null) {
				smc.getMessage().getSOAPPart().getEnvelope().setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
			}
		} catch (DOMException e) {
			logger.error("Impossible de modifier le namespace : " + e);
		} catch (SOAPException e) {
			logger.error("Impossible de modifier le namespace : " + e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext)
	 */
	@Override
	public void close(final MessageContext context) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders()
	 */
	@Override
	public Set<QName> getHeaders() {
		return Collections.emptySet();
	}

	/**
	 * Adds the to port.
	 * 
	 * @param binding
	 *            the binding
	 */
	@SuppressWarnings("rawtypes")
	/** 
	 * This static method adds the handler to the provided port's binding object.  
	 *  
	 * @param binding - The binding object can be fetched by <code>((BindingProvider) port).getBinding()</code> 
	 */
	public static void addToPort(final Binding binding) {
		final List<Handler> handlerChain = binding.getHandlerChain();
		handlerChain.add(new SOAPLogger());

		/*
		 * Check List<Handler> javax.xml.ws.Binding.getHandlerChain() javadocs.
		 * It states: Gets a copy of the handler chain for a protocol binding
		 * instance. If the returned chain is modified a call to setHandlerChain
		 * is required to configure the binding instance with the new chain.
		 */
		binding.setHandlerChain(handlerChain);
	}
}