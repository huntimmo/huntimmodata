/*
 * Cr��/modifi� le %PRT% par %PO%
 * Sp�cification Dimensions : %PID%
 * Statut Dimensions : %PS%
 *
 */
package com.huntimmo.data.common.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;


/**
 *  Classe utilitaire contenant des m�thodes sur les montants.
 * 
 * @author delobelleju
 * 
 */
public class MontantUtils {
		
	/**
	 * Retourne vrai si le montant est strictement sup�rieur � 0.
	 * 
	 * @param aMontant
	 * @return Boolean
	 */
	public static Boolean isMontantSuperieurA0(BigDecimal aMontant){
		Boolean lIsMontantSuperieurA0 = null;
		if(aMontant != null){
			lIsMontantSuperieurA0 = (aMontant.compareTo(BigDecimal.ZERO) == 1);
		}else{
			lIsMontantSuperieurA0 = false;
		}
		return lIsMontantSuperieurA0;
	}
	
	/**
	 * Retourne vrai si le montant est �gal � 0.
	 * 
	 * @param aMontant
	 * @return Boolean
	 */
	public static Boolean isMontantEgalA0(BigDecimal aMontant){
		Boolean lIsMontantEgalA0 = null;
		if(aMontant != null){
			lIsMontantEgalA0 = (aMontant.compareTo(BigDecimal.ZERO)) == 0;
		}else{
			lIsMontantEgalA0 = false;
		}
		return lIsMontantEgalA0;
	}
	
	/**
	 * Format le BigDecimal en entr�e en selon le pattern en param�tre
	 * 
	 * @param aMontant
	 * @param aPattern
	 * @return String
	 */
	public static String format(BigDecimal aMontant, String aPattern){
		return formatInterne(aMontant, aPattern);
	}
	
	/**
	 * Format le Integer en entr�e en selon le pattern en param�tre
	 * 
	 * @param aMontant
	 * @param aPattern
	 * @return String
	 */
	public static String format(Integer aMontant, String aPattern){
		return formatInterne(aMontant, aPattern);
	}
	
	/**
	 * Format le Long en entr�e en selon le pattern en param�tre
	 * 
	 * @param aMontant
	 * @param aPattern
	 * @return String
	 */
	public static String format(Long aMontant, String aPattern){
		return formatInterne(aMontant, aPattern);
	}
	/**
	 * Format l'objet en entr�e en selon le pattern en param�tre
	 * 
	 * @param aMontant
	 * @param aPattern
	 * @return String
	 */
	private static String formatInterne(Object aMontant, String aPattern){
		// Decimal Format
		DecimalFormat lDecimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.FRENCH);
		// Pattern
		lDecimalFormat.applyPattern(aPattern);;
		// Format
		return lDecimalFormat.format(aMontant);
	}
	
	
	/**
	 * Convertit un montant de entier en BigDecimal � l'aide du nombre de decimal en montant
	 * 
	 * @param aMontant
	 * @param aNbDecimal Nombre de decimal
	 * 
	 * @return montant converti (BigDecimal)
	 */
	public final static BigDecimal montantIntegerToBigDecimal(BigInteger aMontant, Integer aNbDecimal) {
		// Si le montant est null, retourne null;
		if(aMontant == null){
			return null;
		}

		// Si le nombre decimal est null, on retourne le m�me montant qu'on a en entr�e
		BigDecimal lMontant = new BigDecimal(aMontant);

		// Si on a un nombre de decimal
		if (aNbDecimal != null) {
			// Nb Decimal
			BigDecimal lMultiplicateur = new BigDecimal(10);
			lMultiplicateur = lMultiplicateur.pow(aNbDecimal);

			// Montant � traiter
			BigDecimal lMontantM = new BigDecimal(aMontant);
			lMontant = lMontantM.divide(lMultiplicateur);

		}
		
		return lMontant;
	}

	/**
	 * Convertit un montant de BigDecimal en Entier � l'aide du nombre de decimal en montant
	 * 
	 * @param aMontant
	 * @param aNbDecimal Nombre de decimal
	 * 
	 * @return montant converti (BigInteger)
	 */
	public final static BigInteger montantBigDecimalToInteger(BigDecimal aMontant, Integer aNbDecimal) {
		// Si le montant est null, retourne null;
		if(aMontant == null){
			return null;
		}

		// Si le nombre decimal est null, on retourne le m�me montant qu'on a en entr�e
		BigInteger lMontant = aMontant.toBigInteger();

		// Si on a un nombre de decimal
		if (aNbDecimal != null) {
			// Nb Decimal
			BigDecimal lDiviseur = new BigDecimal(10);
			lDiviseur = lDiviseur.pow(aNbDecimal);

			// Montant � traiter
			BigDecimal lMontantM = aMontant;
			lMontantM = lMontantM.multiply(lDiviseur);

			lMontant = lMontantM.toBigInteger();

		}
		
		return lMontant;
	}
	
	
	/**
	 * Permet la division de 2 valeurs.
	 * 
	 * @param aValeur1
	 * @param aValeur2
	 * @return Resultat arrondi � 2 chiffres apr�s la virgule.
	 */
	public final static Long diviser(Long aValeur1, Long aValeur2){
		Long lResultat = null;
		
		if(aValeur1 != null && aValeur2 != null){
			BigDecimal lValeur1 = BigDecimal.valueOf(aValeur1);
			BigDecimal lValeur2 = BigDecimal.valueOf(aValeur2);
			BigDecimal lResultatBd = lValeur1.divide(lValeur2,2,RoundingMode.HALF_UP);
			lResultat = lResultatBd.longValue();
		}
		
		return lResultat;
	}



	/**
	 * Addition BigDecimal
	 * 
	 * @param aNombre
	 * @param aAjout
	 * @return BigDecmal
	 */
	public static BigDecimal ajouter(BigDecimal aNombre, BigDecimal aAjout){
		if(aNombre == null && aAjout == null){
			return null;
		} else if(aNombre == null && aAjout != null){
			return aAjout;
		} else if(aNombre != null && aAjout == null){
			return aNombre;
		} else {
			return aNombre.add(aAjout);
		}
	}
	
	
	public static Double convertStringToDouble(String numberStr){
		Double number=null;
		numberStr = numberStr.replace(" ", "").replace(",", ".");
		if(numberStr!=null && !numberStr.equals("")){
			try{
				number = Double.valueOf(numberStr);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return number;
	}
}
