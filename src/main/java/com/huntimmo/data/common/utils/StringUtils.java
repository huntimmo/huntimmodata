package com.huntimmo.data.common.utils;

import java.nio.charset.StandardCharsets;

import javax.xml.bind.DatatypeConverter;

public class StringUtils {
	
	public static boolean isNotEmpty(String str){
		
		if(str!=null && !str.trim().equals("")){
			return true;
		}
		return false;
	}
	
public static boolean isEmpty(String str){
		
		if(str==null || str.trim().equals("")){
			return true;
		}
		return false;
	}
	
	public static String encodeBase64(byte[] bytes) throws Exception {
		String str = new String(bytes, "UTF-8");
	      return  DatatypeConverter.printBase64Binary
	          (str.getBytes(StandardCharsets.UTF_8)); // use "utf-8" if java 6
	   }

}