package com.huntimmo.data.common;




public class Constantes {

 // TODO mettre en place les constantes suivantes pour les adresses : 
//	0	Correspondance
//	1	Légale
//	2	Fiscale
//	3	Localisation géographique (ex. : pour établissements d’une entreprise)
//	X	Indéterminée
//	Z	Autre

	/**
	 * Parametre Cassiopae MTDISPO
	 */
	public final static String CODE_PARAMETRE_MONTANT_DISPO="MTDISPO";
	/**
	 * Parametre Cassiopae DOSID
	 */
	public final static String CODE_PARAMETRE_DOSSIER_ID="DOSID";
	/**
	 * Parametre Cassiopae DATEVALIDITE
	 */
	public final static String CODE_PARAMETRE_DATE_VALIDITE="DATEVALIDITE";
	/**
	 * Parametre Cassiopae MTINIT
	 */
	public final static String CODE_PARAMETRE_MONTANT_INIT="MTINIT";
	/**
	 * Parametre Cassiopae DPRNUMERO
	 */
	public final static String CODE_PARAMETRE_DPRNUMERO="DPRNUMERO";
	
	public static final String CLIENT = "CLIENT";
	
	/**
	 * Clé date : dd/MM/yyyy
	 */
	public static final String KEY_DATE_FR = "dd/MM/yyyy";

	/**
	 * Clé date : yyyy-MM-dd
	 */
	public static final String KEY_DATE_ISO = "yyyy-MM-dd";
	
	/**
	 * Clé date : YYYY-MM-dd hh:mm:ss
	 */
	public static final String KEY_PATTERN_REST_SERVICE_DATE = "YYYY-MM-dd hh:mm:ss";
	
	public static final String KEY_PATTERN_SALESFORCE_DATE = "yyyy-MM-dd HH:mm:ss";
	/**
	 * Clé date : YYMMddhhmmss
	 */
	public static final String KEY_PATTERN_DATE_YYMMddhhmmss = "YYMMddhhmmss";
	
	/*########################################################
	  #####################  Cassiopae Constantes ############       
	  ########################################################*/

	
	/**
	 * Constante représentant le Tiers de type Client
	 */
	public static final String TIERS_CLIENT = "CLIENT";
	
	/**
	 * Constante représentant le Tiers de type Garant
	 */
	public static final String TIERS_GARANT = "GARANT";
	
	/**
	 * Constante représentant le Tiers de type Fournisseur
	 */
	public static final String TIERS_FOURNISSEUR = "FOURN";
	
	/**
	 * Constante représentant le Tiers de type Prescripteur
	 */
	public static final String TIERS_PRESCRIPTEUR = "PRESCRI";
	
	/**
	 * COnstante represensant le role CRISQUE pour les Actor
	 */
	public static final String TIERS_ROLE_RISQUE = "CRISQUE";
	
	/**
	 * Constante représentant le Tiers de type Dirigeant
	 */
	public static final String TIERS_DIDRIGEANT = "DIDRIGEANT";
	
	/**
	 * Constante représentant le Tiers de type Conjoint dirigeant
	 */
	public static final String TIERS_CONJOINT_DIRIGEANT = "CONJOINT";
	
	/**
	 * Constante représentant le Tiers de type Caution
	 */
	public static final String TIERS_CAUTION = "CAUTION";
	
	/**
	 * Constante représentant les Tiers de type dirigeant et conjoint dirigeant
	 */
	public static final String TIERS_INTERVENANT = "INTERV";
	/**
	 * String d'espacement entre le code et le label d'une erreur KSIOP
	 */
	public static final String ESPACEMENT_LIBELLE_EXCEPTION = " : ";
	/**
	 * Type functionnel d'exception ksiop
	 */
	public static final String TYPE_EXCEPTION_KSIOP_FUNCTIONNEL = "FUNC";
	/**
	 * Constante représentant le Code France
	 */
	public static final String CODE_FRANCE = "FR";
	
	/**
	 * Constante représentant la divise euros
	 */
	public static final String DEVISE_EUR = "EUR";
	
	/**
	 * Constante représentant le Régime TVA 'D'
	 */
	public static final String REGIME_TVA_D = "D";
	
	/**
	 * La string qui est avant la note dans le dprnom de l'enveloppe
	 */
	public static final String NOTE_DPRNOM_ENVELOPPE = " NOTE=";
	/**
	 * Constante représentant le Code UGE
	 */
	public static final String CODE_UGE = "UG001";
	
	/**
	 * Constante représentant le Code Rib principal
	 */
	public static final String RIB_PRINCIPAL = "RIB_PRINCIPAL_1";
	
	/**
	 * Constante représentant le Type de Registre Commercial RC
	 */
	public static final String TYPE_REGISTRE_COMMERCIAL_RC = "RC";
	
	/**
	 * Constante représentant le type Particulier/Personne physique
	 */
	public static final String TIERS_TYPE_PARTICULIER = "0";
	
	/**
	 * Constante représentant le type Entreprise Individuelle
	 */
	public static final String TIERS_TYPE_ENTREPRISE_INDIVIDUELLE = "1";
	
	/**
	 * Constante représentant le type Personne Morale
	 */
	public static final String TIERS_TYPE_PERSONNE_MORALE = "2";
	
	/**
	 * Constante représentant le type Particulier/Personne physique KSIOP
	 */
	public static final String TIERS_TYPE_PARTICULIER_KSIOP = "PART";
	
	/**
	 * Constante représentant le type Entreprise Individuelle KSIOP
	 */
	public static final String TIERS_TYPE_ENTREPRISE_INDIVIDUELLE_KSIOP = "EI";
	
	/**
	 * Constante représentant le type Personne Morale KSIOP
	 */
	public static final String TIERS_TYPE_PERSONNE_MORALE_KSIOP = "PM";
	
	/**
	 * Constante représentant le code de la phase d'initialisation
	 */
	public static final String PHASE_CODE_INI = "INI";
	
	/**
	 * Constante représentant le code de la phase de destination Acteur
	 */
	public static final String PHASE_CODE_ACTEUR = "ACTEUR";
	
	/**
	 * Constante représentant le code de la phase FL
	 */
//	public static final String PHASE_CODE_FL = "CREERFL";
	
	/**
	 * Constante représentant le code Corporate
	 */
	public static final String CODE_SEGMENT_CORPORATE = "CORPORATE";
	
	/**
	 * Constante représentant le code Retail
	 */
	public static final String CODE_SEGMENT_RETAIL = "RETAIL";
	
	/**
	 * Constante représentant le type de notation Segmentation
	 */
	public static final String TYPE_RATING_SEGMENT = "SEGOCT";
	
	public static final String CODE_RATING_SEGMENT_CORPORATE = "CORPOCT";
	public static final String CODE_RATING_SEGMENT_RETAIL = "RETAOCT";
	
	/**
	 * Constante représentant le type de notation Corporate
	 */
	public static final String TYPE_RATING_CORPORATE = "CMCDOOC";
	
	/**
	 * Constante représentant le type de notation Retail
	 */
	public static final String TYPE_RATING_RETAIL = "RMCDOOC";
	
	public static final String FOURNISSEUR_CAT_JURIDIQUE = "9999";
	
	public static final String FOURNISSEUR_CODE_NAF = "9999Z";
	
	/**
	 * Constante représentant le code catégorie juridique à renseigner pour un particulier
	 */
	public static final String PARTICULIER_CAT_JURIDIQUE = "1900";
	
	/**
	 * Constante définissant que le deal est en délégation
	 */
	public static final String EN_DELEGATION = "DELEG";
	/**
	 * Constante définissant que le deal est hors délégation
	 */
	public static final String HORS_DELEGATION = "HORSDEL";
	
	/**
	 * Constante représentant le flux financier LOYCBM
	 */
	public static final String FLUX_FINANCIER_LOYCBM = "LOYCBM";
	
	/**
	 * Constante représentant l'acteur Dirigeant pour le deal
	 */
	public static final String DEAL_ACTOR_DIRIGEANT = "DIRIG";
	/**
	 * Constante représentant l'acteur conjoint du dirigeant pour le deal
	 */
	public static final String DEAL_ACTOR_CONJOINT_DIRIGEANT = "CONJ";
	
	/**
	 * Constante permettant de determiner le type de changement de step dans le cas d'un appel à DOSSIER_WORKFLOW
	 */
	public static final String TYPE_CHANGEMENT_STEP_DEROGATION = "DEROGATION";
	/**
	 * Constante permettant de determiner le type de changement de step dans le cas d'un appel à DOSSIER_WORKFLOW
	 */
	public static final String TYPE_CHANGEMENT_STEP_ETUDE = "ETUDE";
	/**
	 * Constante permettant de determiner le type de changement de step dans le cas d'un appel à DOSSIER_WORKFLOW
	 */
	public static final String TYPE_CHANGEMENT_STEP_SIMULATION = "SIMULATION";
	/**
	 * Constante permettant de determiner le type de changement de step dans le cas d'un appel à DOSSIER_WORKFLOW
	 */
	public static final String TYPE_CHANGEMENT_STEP_SCHEMA_DELEGATAIRE = "SCHEMA_DELEGATAIRE";
	/**
	 * Constante permettant de determiner le type de changement de step dans le cas d'un appel à DOSSIER_WORKFLOW
	 */
	public static final String TYPE_CHANGEMENT_STEP_ENVOI_ACTEURS = "ACTEURS";
	/**
	 * Constante permettant de determiner le type de changement de step dans le cas d'un appel à DOSSIER_WORKFLOW
	 */
	public static final String TYPE_CHANGEMENT_STEP_EDITER_CONTRAT = "EDITER_CONTRAT";
	/**
	 * Constante permettant de determiner le type de changement de step dans le cas d'un appel à DOSSIER_WORKFLOW
	 */
	public static final String TYPE_CHANGEMENT_STEP_FINALISER_CONTRAT = "FINALISER_CONTRAT";
	/**
	 * Constante permettant de determiner le type de changement de step dans le cas d'un appel à DOSSIER_WORKFLOW
	 */
	public static final String TYPE_CHANGEMENT_STEP_DECISION = "AJOUT_DECISION";
	
	public static final String JALON_INTLITE = "INTLITE";
	/**
	 * Jalon de creation de simulation
	 */
//	public static final String JALON_CREATION = "CREERFL";
	/**
	 * Jalon de demande de derogation
	 */
	public static final String JALON_CREATION_DEROG_DEMANDE = "D_DEROG";
	/**
	 * Jalon de derogation traitée
	 */
	public static final String JALON_CREATION_DEROG_TRAITEE = "T_DEROG";
	/**
	 * Jalon etude
	 */
	public static final String JALON_ETUDE = "D_ETUD";
	/**
	 * Jalon du début de schema delegataire
	 */
	public static final String JALON_SCHEMA_DELEGATIRE_DEBUT = "DEBSDG";
	/**
	 * Jalon de fin de shema delegataire
	 */
	public static final String JALON_SCHEMA_DELEGATAIRE_FIN = "DEL_OK";
	/**
	 * Jalon de fin de shema delegataire
	 */
	public static final String JALON_SCHEMA_DELEGATAIRE_KO = "DEL_KO";
	/**
	 * Jalon etude par le DR
	 */
	public static final String JALON_ETUDE_DR = "AETUDDR";
	/**
	 * Jalon etude par Natixis Lease
	 */
	public static final String JALON_ETUDE_NL = "AETUDNL";
	/**
	 * Jalon figer
	 */
	public static final String JALON_FIGER = "D_FIGER";
	/**
	 * Jalon décision
	 */
	public static final String JALON_DECISION = "D_DECID";
	/**
	 * Jalon edition de contrat
	 */
	public static final String JALON_EDITION_CONTRAT = "AEDITER";
	/**
	 * Jalon contrat édité
	 */
	public static final String JALON_CONTRAT_EDITE = "EDITER";
	
	//Decisions
	/**
	 * Decision delegation
	 */
	public static final String DECISION_DELEGATION = "DELEG";
	/**
	 * Decision hors delegation
	 */
	public static final String DECISION_HORS_DELEGATION = "HORSDEL";
	/**
	 * Decision refus banque
	 */
	public static final String DECISION_REFUS_BANQUE = "REFUSBQE";
	/**
	 * Decision DECISION_ACCORD_RESEAU ACCCAI
	 */
	public static final String DECISION_ACCORD_RESEAU = "ACCCAI";
	
	/**
	 * Decision DECISION_WILIZ DECIDED
	 */
	public static final String DECISION_WILIZ = "DECISION_WILIZ";
	
	/**
	 * Decision DECISION_DEN DECISION_DEN
	 */
	public static final String DECISION_DEN = "DECISION_DEN";
	
	/**
	 * Decision DECISION_EXCLUSION_FRONTLEASE ENVDECEXCL
	 */
	public static final String DECISION_EXCLUSION_FRONTLEASE = "ENVDECEXCL";
	
	/**
	 * Decision valide
	 */
	public static final String DECISION_VALIDE = "VALID";
	/**
	 * Devision validée par GAR
	 */
	public static final String DECISION_VALIDE_GAR = "VALIDGAR";
	/**
	 * Decision validée par GAR
	 */
	public static final String DECISION_VALIDE_PAR = "VALIDGAR";
	/**
	 * Décision validér par les deux parties
	 */
	public static final String DECISION_VALIDE_PARGAR = "VALIDPARGAR";
	/**
	 * Décision Derogation Materiel d'occasion
	 */
	public static final String DECISION_DEROC = "DEROC";
	
	/**
	 * Constante définissant le commentaire pour le jalon analyse.
	 */
	public static final String COMMENTAIRE_ANALYSE = "COM_ANALYSE";

	/**
	 * Constante représentant un mode création
	 */
	public static final String MODE_CREATION = "CREATION";
	
	/**
	 * Constante représentant la phase du dossier Négociation
	 */
	public static final String PHASE_DOSSIER_NEGO = "NEGO";
	
	/**
	 * Constante représentant la phase du dossier Prod
	 */
	public static final String PHASE_DOSSIER_PROD = "PROD";
	
	/**
	 * Constante représentant la phase du dossier Gest
	 */
	public static final String PHASE_DOSSIER_GEST = "GEST";
	
	/**
	 * Constante représentant la phase du dossier Fin
	 */
	public static final String PHASE_DOSSIER_FIN = "FIN";
	
	/**
	 * Constante représentant la phase de destination AVDOSS
	 */
	public static final String PHASE_DESTINATION_AVDOSS = "AVDOSS";
	
	/**
	 * Constante représentant la phase étape CREEFL
	 */
//	public static final String PHASE_ETAPE_CREERFL = "CREERFL";
	
	/**
	 * Constante définissant le code de type de decision lors d'une creation ou modification de dossier.
	 */
	public static final String CODE_DECISION_DOSSIER = "MOTIFI";
	
	
	/**
	 * Constante définissant le code de contrat lors d'une creation ou modification de dossier.
	 */
	public static final String NEGO = "NEGO";
	
	/**
	 * Constante représentant l'acteur Partenaire pour le deal
	 */
	public static final String DEAL_ACTOR_PARTENAIRE = "PARTEN";

	public static final String DEAL_ACTOR_APPORTEUR = "APPORT";
		
	/**
	 * Constante représentant l'acteur Sous-apporteur pour le deal
	 */
	public static final String DEAL_ACTOR_SOUS_APPORTEUR = "AGENCE";
	
	/**
	 * Constante représentant l'acteur Garant pour le deal
	 */
	public static final String DEAL_ACTOR_GARANT = "GARANT";
	
	/**
	 * Constante représentant Natixis Lease
	 */
	public static final String ID_NATIXIS_LEASE = "137";
	
	/**
	 * Code metier du DR.
	 */
	public static final String METIER_DR = "RC";
	
	/**
	 * Constante représentant le code de prélèvement par chèque
	 */
	public static final String PRELEVEMENT_CHEQUE = "CHQ";
	
	/**
	 * Constante représentant le code de virement automatique
	 */
	public static final String VIREMENT_AUTO = "VIRAUTO";
	
	/**
	 * Constante représentant l'acteur Client pour le deal
	 */
	public static final String DEAL_ACTOR_CLIENT = "CLIENT";
	
	/**
	 * Constante représentant le code de prélèvement automatique
	 */
	public static final String PRELEVEMENT_AUTOMATIQUE = "PRLAUTO";
	
	/**
	 * Constante représentant un début de nom de dossier
	 */
	public static final String NOM_DOSSIER_DL = "DL-";
	public static final String NOM_DOSSIER_WL = "WL-";
	
	/**
	 * Constante définissant le code de l'ID dossier distributeur
	 */
	public static final String CUSTOMCHAR_VALUE_CODE_DEAL = "CCHFLDEAL";
	
	public static final String INDEMNITE_RESILIATION_IRCLASS = "IRCLASS";
	
	/**
	 * Constante représentant le flux financier
	 */
	public static final String FLUX_FINANCIER_LOYFIX = "LOYFIX";
	public static final String FLUX_FINANCIER_PALMT = "PALMT";

	
	public static final Double BASE_REPARTITION_100 = 100.0;
	public static final Double BASE_REPARTITION_50 = 50.0;
	public static final String CBM = "CBM";
	public static final String LOCFIN = "LOCFIN";
	public static final String LLD = "LLD";
	public static final String CODE_TAXE_TVAM = "TVAM";
	public static final String DTMEP = "DTMEP";
	public static final String DTACC = "DTACC";
	public static final String P1M = "P1M";
	public static final String P2M = "P2M";
	public static final String P5CBM2 = "P5CBM2";
	
	public static final Double VALEUR_RESIDUELLE_DEFAULT = 0.06;
	public static final Integer PERIODICITE_DEFAULT = 60;
	public static final Double DEFAULT_DOUBLE_ZERO = 0.0;

	/**
	 * Nombre de caractères max 32
	 */
	public static final Integer NB_CARACTERE_MAX_32 = 32;
	
	public static final String USER_CASSIOPAE = "ORFI";

	/**
	 * ReadPrincing 
	 */
	public static final String CODE_FILTER_FTRENT = "FTRENT";
	public static final String CODE_FILTER_FTCOM = "FTCOM";

	/**
	 * Attribut CHAINE
	 */
	public static final String NOMINAL = "NOMINAL";
	public static final String FM = "FM";
	public static final String ITE = "ITE";
	public static final String REFINANCING_TYPE_VARIABLE = "VAR";
	public static final String STANDARD = "STANDARD";
	public static final String DEROG = "DEROG";
	public static final String TXLIB = "TXLIB";
	public static final String TCI = "TCI";
	

	/**
	 * Attribute CODE
	 */
	public static final String  PFIBAREMETYPE = "PFIBAREMETYPE";
	public static final String  PFIFTVCODE = "PFIFTVCODE";
	public static final String  PAPPROCHEFLUX = "PAPPROCHEFLUX";
	public static final String  APPROCHEFLUX = "APPROCHEFLUX";
	public static final String  TYPE_REMUNERATION_REMSAI = "REMSAI";
	
	public static final String  COMCCBP = "COMCCBP";
	public static final String  PFIGROCODE = "PFIGROCODE";
	public static final String  VALIDE = "VALIDE";
	public static final String  PFISTATUT = "PFISTATUT";
	
	public static final String  BASE_JOURS_360 = "360";
	
	public static final String  REGIME_GENERAL = "GEN";
	
	public static final String  TERME_COTATION_A = "A";
	
	public static final String  SECTEUR_GESTION_EX = "EX";
	
	/**
	 * CODE_MATERIEL_ENVELOPPE_DEFAULT
	 */
	public static final String  NATURE_MATERIEL_ENVELOPPE_DEFAULT = "VEHICULES";
	public static final String  CODE_MATERIEL_ENVELOPPE_DEFAULT = "34102";
	public static final Double  TAUX_TVA_MATERIEL_ENVELOPPE_DEFAULT = 20.0;
	
	/**
	 * CONSTANTE UTILISEE POUR IDENTIFIER LES REQUETES HTTP
	 */
	public static final String FILTER_REQUESTHTTP_ID = "REQUESTHTTPID";
	
	/**
	 * LABELS Statuts Salesforce :
	 */
	public static final String LABEL_STATUS_DEMANDE_EN_COURS = "Demande en cours";
	public static final String LABEL_STATUS_RETOUR_DOCUMENT = "Retour document";
	public static final String LABEL_STATUS_RETOUR_FOURNISSEUR = "Retour fournisseur";
	public static final String LABEL_STATUS_MISE_EN_LOYER  = "Mise en loyer";
	public static final String LABEL_STATUS_DEMANDE_ANNULEE = "Demande annulée";
	/**
	 * CODES Statuts Salesforce :
	 */
	public static final String CODE_STATUS_DEMANDE_EN_COURS = "01";
	public static final String CODE_STATUS_RETOUR_DOCUMENT_OK = "02";
	public static final String CODE_STATUS_RETOUR_DOCUMENT_KO = "03";
	public static final String CODE_STATUS_RETOUR_FOURNISSEUR = "04";
	public static final String CODE_STATUS_MISE_EN_LOYER  = "05";
	public static final String CODE_STATUS_DEMANDE_ANNULEE = "06";
	
	
	public static final String MONTANT_UNITE_MILLION = "M";
	
	public static final String ASSFIN = "ASSFIN";

	/**
	 * Prestation
	 */
	public static final String CODE_FRAIS_DOSSIER_BP = "FRAISDBP";
	public static final String CODE_FRAIS_DOSSIER = "FRAISDOS";
	public static final String CODE_FRAIS_GREFFE = "GREFREF";

	/**
	 * RUBCODE
	 */
	public static final String CODE_RUBCODE_FRAIS_DOSSIER_DIRECT = "FRSDOS";
	public static final String CODE_RUBCODE_FRAIS_DOSSIER_SC = "SCFDOS";
	public static final String CODE_RUBCODE_MATERIEL_ANDROMEDE = "99";

	/**
	 * CODES RESEAU
	 */
	public static final String RESEAU_CE = "CE";
	public static final String RESEAU_BP = "BP";
	public static final String RESEAU_BRED = "BRED";
	public static final String RESEAU_BANQUE = "BANQUE";
	public static final String DIRECT = "DIRECT";

	/**
	 * CODES GARANTS
	 */
	public static final String CODE_GARANT_BP = "GARANBP";
	public static final String CODE_GARANT_CE = "GARANCE";

	/**
	 * CODE JALON
	 */
	public static final String CODE_JALON_DISPOLEASE = "CREERDL";
	public static final String CODE_JALON_WILIZ = "CREERWL";

	/**
	 * CUSTOMCHAR ACTOR
	 */
	public static final String CODE_CUSTOMCHAR_ACTOR_DISPOLEASE = "CCHFLIDDISP";
	public static final String CODE_CUSTOMCHAR_ACTOR_WILIZ = "CCHFLIDWILI";

	/**
	 * CUSTOMCHAR ASSET
	 */
	public static final String CODE_CUSTOMCHAR_ASSET_DATELIVRAISON = "CCHASSDTLIV";

	/**
	 * CUSTOMCHAR_DEAL - CODE
	 */
	public static final String CODE_CUSTOMCHAR_DEAL_DISPOLEASE = "CCHDISPDEAL";
	public static final String CODE_CUSTOMCHAR_DEAL_WILIZ = "CCHWILIDEAL";
	public static final String CODE_CUSTOMCHAR_DEAL_RECUPTVA = "RECUPTVA";

	/**
	 * CUSTOMCHAR_DEAL - VALUE
	 */
	public static final String VALUE_CUSTOMCHAR_DEAL_RECUPTVA_Y = "Y";
	public static final String VALUE_CUSTOMCHAR_DEAL_RECUPTVA_N = "N";

	/**
	 * PARAM CASSIOAPE
	 */
	public static final String PARAM_DELEGATION = "DOSRESEAUCIAL";
	public static final String PARAM_BANK = "FLETABRESEAU";
	public static final String PARAM_PRODUCT = "FLRESEAUPRDT";
	public static final String PARAM_RUBRIQUE = "RUBRIQUE";
	public static final String PARAM_MATERIAL = "NATMATNAP";
	public static final String PARAM_VAT_MATERIAL = "FLTAXTAUX";
	public static final String PARAM_RESPONSABLE = "FLETABRESP";
	public static final String PARAM_INSURANCE_MATERIAL = "AVTPRESTATIONTAXE_MAT";
	public static final String PARAM_INSURANCE_PERSON = "AVTPRESTATIONTAXE_ACT";
	public static final String CODE_PARAM_RESEAU = "RESEAU";
	public static final String CODE_PARAM_LIBELLE = "LIBELLE";
	public static final String CODE_PARAM_DELEG = "DELEG";	
	public static final String CODE_PARAM_ETABLISSEMENT = "ETABLISSEMENT";
	public static final String CODE_PARAM_ACTOR_ID = "ACTID";
	public static final String CODE_PARAM_DR_USER = "RESP";
	public static final String CODE_PARAM_CODE_ROLE = "ROLCODE";
	public static final String CODE_PARAM_APPORTEUR = "APP";
	public static final String CODE_PARAM_GARANT = "GARA";
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final String CODE_PARAM_RUBID = "RUBID";
	public static final String CODE_PARAM_RUBCODE = "RUBCODE";
	public static final String CODE_APPLICATION_EQUIP_PRO = "EAX";
	
	/**
	 * BAPI
	 */
	public static final String LABEL_RISK_SEGMENTATION_EI = "Entrepreneur individuel";
	public static final String CODE_RISK_SEGMENTATION_EI = "3110";
	public static final String CODE_LEGAL_SITUATION_EI = "1";
	public static final String CODE_ROLE_LEGAL_REPRESENTATIVE = "09007";
}