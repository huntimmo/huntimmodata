package com.huntimmo.data.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.WebUtils;

/**
* A filter which logs web requests that lead to an error in the system.
*
*/
@Component
public class LogRequestFilter extends OncePerRequestFilter implements Ordered {

    private static final String UNKNOWN = "[unknown]";

	private static final String TABULATION = "\t";

	private static final String ESPACE = "\r";

	private static final String RETOUR_CHARIOT = "\n";

	private static final String REPLACEMENT = "";

	private static final String REQUEST_BODY = "requestBody";

	private static final String STATUS_CODE = "statusCode";

	private static final String QUERY = "query";

	private static final String PATH = "path";

	private static final String METHOD = "method";

	private final Log logger = LogFactory.getLog(getClass());

    // put filter at the end of all other filters to make sure we are processing after all others
    private int order = Ordered.LOWEST_PRECEDENCE - 8;
//    private ErrorAttributes errorAttributes;

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        throws ServletException, IOException {
        ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);

        int status = HttpStatus.INTERNAL_SERVER_ERROR.value();

        // pass through filter chain to do the actual request handling
        filterChain.doFilter(wrappedRequest, response);
        status = response.getStatus();

        Map<String, Object> trace = getTrace(wrappedRequest, status);

        // body can only be read after the actual request handling was done!
        getBody(wrappedRequest, trace);
        logTrace(wrappedRequest, trace);
    }

    /**
     * Wrapping the request body for logging
     * 
     * @param request
     * @param trace
     */
    private void getBody(ContentCachingRequestWrapper request, Map<String, Object> trace) {
        // wrap request to make sure we can read the body of the request (otherwise it will be consumed by the actual
        // request handler)
        ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        if (wrapper != null) {
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                String payload;
                try {
                    payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
                }
                catch (UnsupportedEncodingException ex) {
                    payload = UNKNOWN;
                }
                
                trace.put(REQUEST_BODY, payload.replace(RETOUR_CHARIOT, REPLACEMENT).replace(ESPACE,REPLACEMENT).replace(TABULATION,REPLACEMENT));
            }
        }
    }

    /**
     * Logging the data 
     * 
     * @param request
     * @param trace
     */
    private void logTrace(HttpServletRequest request, Map<String, Object> trace) {
        Object method = trace.get(METHOD);
        Object path = trace.get(PATH);
        Object statusCode = trace.get(STATUS_CODE);
        Object query = trace.get(QUERY);
        Object requestBody = trace.get(REQUEST_BODY);
		
        // log : method path statusCode query requestBody 
        logger.info(String.format("httpmethod : %s - path : %s - statuscode : %s - querystring : %s - requestbody : %s", method, path, statusCode, query, requestBody));
    }
    
    /**
     * @param request
     * @param status
     * @return Map containing the data to log
     */
    protected Map<String, Object> getTrace(HttpServletRequest request, int status) {
        Map<String, Object> trace = new LinkedHashMap<String, Object>();
        trace.put(METHOD, request.getMethod());
        trace.put(PATH, request.getRequestURI());
        trace.put(QUERY, request.getQueryString());
        trace.put(STATUS_CODE, status);

        return trace;
    }

}