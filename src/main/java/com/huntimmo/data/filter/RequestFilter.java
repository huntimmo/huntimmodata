//package com.huntimmo.data.filter;
//
//import java.io.IOException; 
//import java.util.UUID;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//
//import org.jboss.logging.MDC;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Component;
//
//import com.natixis.fd3.common.Constantes;
//
//@Component
//public class RequestFilter implements Filter {
//
//	@Override
//	public void doFilter(ServletRequest request, ServletResponse response,
//			FilterChain chain) throws IOException, ServletException {
////		try {
//			// nettoyage du MDC avant logging
//			MDC.clear();
//			
//			String userId = "NOTCONNECTED";
//			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//			if(authentication != null){
//				userId = authentication.getName();
//			}
//			String requestId = UUID.randomUUID().toString();
//			String mdcData = String.format("- userid:%s - requesthttpid:%s -", userId, requestId);
//			MDC.put(Constantes.FILTER_REQUESTHTTP_ID, requestId);
//			MDC.put("mdcData", mdcData);
//			chain.doFilter(request, response);
////		} finally {
////			MDC.clear();
////		}
//	}
//
//	@Override
//	public void destroy() {
//	}
//
//	@Override
//	public void init(FilterConfig arg0) throws ServletException {
//	}
//}