//package com.huntimmo.data.filter;
//
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.Jws;
//import io.jsonwebtoken.JwtException;
//
//import java.io.IOException;
//import java.security.GeneralSecurityException;
//import java.util.List;
//
//import javax.annotation.PostConstruct;
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.authentication.AnonymousAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import com.natixis.fd3.security.TokenVerifier;
//
//@Component
//public class AuthenticationFilter extends OncePerRequestFilter {
//	
//	@Value("${gateway.environment}")
//    private String gatewayEnv;
//	
//	@Value("${swagger.activate}")
//	private Boolean swaggerActivate;
//	
//	private static TokenVerifier tokenVerifier;
//	
//	@Override
//	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
//			throws ServletException, IOException {
//		String token = request.getHeader("Authorization");
//	    if (token == null) token = request.getParameter("access_token");
//	    if(token == null){
//	    	token = request.getHeader("X-Authorization");
//	    }
//	    
//	    try {
//	    	if (token == null) throw new GeneralSecurityException("No token found in headers");
//	    	// Cut the bearer part
//	    	token = (token.startsWith("Bearer ")) ? token.replaceFirst("Bearer ", "") : token;
//            logger.debug("Trying to authenticate user with token: { " + ((token.length()) > 6 ? token.substring(0, 5) : token) + "... }");
//            
//            // Decoding the token with the secret key
//            Jws<Claims> claims = tokenVerifier.getClaims(token);
//            
//	    	logger.debug(claims);
//            setRoles(claims.getBody());
//	        chain.doFilter(request, response);
//	    } catch (JwtException | IllegalArgumentException | GeneralSecurityException badJwt) {
//	    	SecurityContextHolder.clearContext();
//	        logger.error("Token error : " + badJwt.getLocalizedMessage());
//	        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, badJwt.getLocalizedMessage());
//	    }
//		
//	}
//
//	private void setRoles(Claims aClaims) throws IllegalArgumentException {
////		List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(aClaims.getSubject());
////		List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(aClaims.get("appname").toString());
//		List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList("DHM_OSCAR");
//		
//		try {
//			UserDetails p = new User(aClaims.getIssuer(), "none", authorities);
//			Authentication authentication = new AnonymousAuthenticationToken("key", p, authorities);
//			SecurityContextHolder.getContext().setAuthentication(authentication);
//		} catch (IllegalArgumentException e) {
//			throw new IllegalArgumentException("Error while parsing token.", e);
//		}
//	}
//	
//	@Bean
//	public FilterRegistrationBean loggingFilter(){
//	    FilterRegistrationBean registrationBean = new FilterRegistrationBean();
//	         
//	    registrationBean.setFilter(new AuthenticationFilter());
//
//	    // si swagger activé
////    	if(swaggerActivate != null && swaggerActivate.booleanValue()){
////    		registrationBean.addUrlPatterns("/none/");
////	    }
////    	else {
//    		registrationBean.addUrlPatterns("/api/v1/deal/eligibilityRenting/*");
////	    }
//
//	    return registrationBean;    
//	}
//	
//	
//	@PostConstruct
//    public void afterInit() {
//		if(tokenVerifier == null){
//			tokenVerifier = new TokenVerifier(gatewayEnv);
//		}
//		
//    }
//	
//}
