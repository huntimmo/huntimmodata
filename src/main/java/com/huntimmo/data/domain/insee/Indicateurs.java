package com.huntimmo.data.domain.insee;

import org.springframework.data.annotation.Id;

public class Indicateurs {

	@Id
	public String id;

	private String CODGEO;
	private String Nb_Pharmacies_et_parfumerie;
	private String Dynamique_Entrepreneuriale;
	private String Dynamique_Entrepreneuriale_Service_et_Commerce;
	private String Synergie_Medicale_COMMUNE;
	private String Orientation_Economique;
	private String Indice_Fiscal_Partiel;
	private String Score_Fiscal;
	private String Indice_Evasion_Client;
	private String Score_Evasion_Client;
	private String Indice_Synergie_Medicale;
	private String Score_Synergie_Medicale;
	private String SEG_Croissance_POP;
	private String LIBGEO;
	private String REG;
	private String DEP;
	private String Nb_Omnipraticiens_BV;
	private String Nb_Infirmiers_Liberaux_BV;
	private String Nb_dentistes_Liberaux_BV;
	private String Nb_pharmaciens_Liberaux_BV;
	private String Densite_Medicale_BV;
	private String Score_equipement_de_sante_BV;
	private String Indice_Demographique;
	private String Score_Demographique;
	private String Indice_Menages;
	private String Score_Menages;
	private String Population;
	private String Evolution_Population;
	private String Evolution_Pop_Pourcentage;
	private String Nb_Menages;
	private String Nb_Residences_Principales;
	private String Nb_proprietaire;
	private String Nb_Logement;
	private String Nb_Residences_Secondaires;
	private String Nb_Log_Vacants;
	private String Nb_Occupants_Residence_Principale;
	private String Nb_Femme;
	private String Nb_Homme;
	private String Nb_Mineurs;
	private String Nb_Majeurs;
	private String Nb_Etudiants;
	private String Nb_Entreprises_Secteur_Services;
	private String Nb_Entreprises_Secteur_Commerce;
	private String Nb_Entreprises_Secteur_Construction;
	private String Nb_Entreprises_Secteur_Industrie;
	private String Nb_Creation_Enteprises;
	private String Nb_Creation_Industrielles;
	private String Nb_Creation_Construction;
	private String Nb_Creation_Commerces;
	private String Nb_Creation_Services;
	private String Moyenne_Revenus_Fiscaux_Departementaux;
	private String Moyenne_Revenus_Fiscaux_Regionaux;
	private String Dep_Moyenne_Salaires_Horaires;
	private String Dep_Moyenne_Salaires_Cadre_Horaires;
	private String Dep_Moyenne_Salaires_Prof_Intermediaire_Horaires;
	private String Dep_Moyenne_Salaires_Employe_Horaires;
	private String Dep_Moyenne_Salaires_Ouvriers_Horaires;
	private String Reg_Moyenne_Salaires_Horaires;
	private String Reg_Moyenne_Salaires_Cadre_Horaires;
	private String Reg_Moyenne_Salaires_Prof_Intermediaire_Horaires;
	private String Reg_Moyenne_Salaires_Employe_Horaires;
	private String Reg_Moyenne_Salaires_Ouvriers_Horaires;
	private String Valeur_ajoutee_regionale;
	private String Urbanite_Ruralite;
	private String Score_Urbanite;
	private String Nb_Atifs;
	private String Nb_Actifs_Salaries;
	private String Nb_Actifs_Non_Salaries;
	private String Nb_Logement_Secondaire_et_Occasionnel;
	private String Nb_Hotel;
	private String Capacite_Hotel;
	private String Nb_Camping;
	private String Capacite_Camping;
	private String Dynamique_Demographique_BV;
	private String Taux_etudiants;
	private String Taux_Propriete;
	private String Dynamique_Demographique_INSEE;
	private String Capacite_Fisc;
	private String Capacite_Fiscale;
	private String Moyenne_Revnus_fiscaux;
	private String Taux_Evasion_Client;
	private String Nb_Education_sante_action_sociale;
	private String Nb_Services_personnels_et_domestiques;
	private String Nb_Sante_action_sociale;
	private String Nb_Industries_des_biens_intermediaires;
	private String Nb_de_Commerce;
	private String Nb_de_Services_aux_particuliers;
	private String Nb_institution_de_Education_sante_action_sociale_administration;
	private String PIB_Regionnal;
	private String SEG_Environnement_Demographique_Obsolete;
	private String Score_Croissance_Population;
	private String Score_Croissance_Entrepreneuriale;
	private String Score_VA_Region;
	private String Score_PIB;
	private String Environnement_Demographique;
	private String Fidelite;
	private String SYN_MEDICAL;
	private String Seg_Cap_Fiscale;
	private String Seg_Dyn_Entre;
	private String DYN_SetC;
	private String CP;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCODGEO() {
		return CODGEO;
	}
	public void setCODGEO(String cODGEO) {
		CODGEO = cODGEO;
	}
	public String getNb_Pharmacies_et_parfumerie() {
		return Nb_Pharmacies_et_parfumerie;
	}
	public void setNb_Pharmacies_et_parfumerie(String nb_Pharmacies_et_parfumerie) {
		Nb_Pharmacies_et_parfumerie = nb_Pharmacies_et_parfumerie;
	}
	public String getDynamique_Entrepreneuriale() {
		return Dynamique_Entrepreneuriale;
	}
	public void setDynamique_Entrepreneuriale(String dynamique_Entrepreneuriale) {
		Dynamique_Entrepreneuriale = dynamique_Entrepreneuriale;
	}
	public String getDynamique_Entrepreneuriale_Service_et_Commerce() {
		return Dynamique_Entrepreneuriale_Service_et_Commerce;
	}
	public void setDynamique_Entrepreneuriale_Service_et_Commerce(String dynamique_Entrepreneuriale_Service_et_Commerce) {
		Dynamique_Entrepreneuriale_Service_et_Commerce = dynamique_Entrepreneuriale_Service_et_Commerce;
	}
	public String getSynergie_Medicale_COMMUNE() {
		return Synergie_Medicale_COMMUNE;
	}
	public void setSynergie_Medicale_COMMUNE(String synergie_Medicale_COMMUNE) {
		Synergie_Medicale_COMMUNE = synergie_Medicale_COMMUNE;
	}
	public String getOrientation_Economique() {
		return Orientation_Economique;
	}
	public void setOrientation_Economique(String orientation_Economique) {
		Orientation_Economique = orientation_Economique;
	}
	public String getIndice_Fiscal_Partiel() {
		return Indice_Fiscal_Partiel;
	}
	public void setIndice_Fiscal_Partiel(String indice_Fiscal_Partiel) {
		Indice_Fiscal_Partiel = indice_Fiscal_Partiel;
	}
	public String getScore_Fiscal() {
		return Score_Fiscal;
	}
	public void setScore_Fiscal(String score_Fiscal) {
		Score_Fiscal = score_Fiscal;
	}
	public String getIndice_Evasion_Client() {
		return Indice_Evasion_Client;
	}
	public void setIndice_Evasion_Client(String indice_Evasion_Client) {
		Indice_Evasion_Client = indice_Evasion_Client;
	}
	public String getScore_Evasion_Client() {
		return Score_Evasion_Client;
	}
	public void setScore_Evasion_Client(String score_Evasion_Client) {
		Score_Evasion_Client = score_Evasion_Client;
	}
	public String getIndice_Synergie_Medicale() {
		return Indice_Synergie_Medicale;
	}
	public void setIndice_Synergie_Medicale(String indice_Synergie_Medicale) {
		Indice_Synergie_Medicale = indice_Synergie_Medicale;
	}
	public String getScore_Synergie_Medicale() {
		return Score_Synergie_Medicale;
	}
	public void setScore_Synergie_Medicale(String score_Synergie_Medicale) {
		Score_Synergie_Medicale = score_Synergie_Medicale;
	}
	public String getSEG_Croissance_POP() {
		return SEG_Croissance_POP;
	}
	public void setSEG_Croissance_POP(String sEG_Croissance_POP) {
		SEG_Croissance_POP = sEG_Croissance_POP;
	}
	public String getLIBGEO() {
		return LIBGEO;
	}
	public void setLIBGEO(String lIBGEO) {
		LIBGEO = lIBGEO;
	}
	public String getREG() {
		return REG;
	}
	public void setREG(String rEG) {
		REG = rEG;
	}
	public String getDEP() {
		return DEP;
	}
	public void setDEP(String dEP) {
		DEP = dEP;
	}
	public String getNb_Omnipraticiens_BV() {
		return Nb_Omnipraticiens_BV;
	}
	public void setNb_Omnipraticiens_BV(String nb_Omnipraticiens_BV) {
		Nb_Omnipraticiens_BV = nb_Omnipraticiens_BV;
	}
	public String getNb_Infirmiers_Liberaux_BV() {
		return Nb_Infirmiers_Liberaux_BV;
	}
	public void setNb_Infirmiers_Liberaux_BV(String nb_Infirmiers_Liberaux_BV) {
		Nb_Infirmiers_Liberaux_BV = nb_Infirmiers_Liberaux_BV;
	}
	public String getNb_dentistes_Liberaux_BV() {
		return Nb_dentistes_Liberaux_BV;
	}
	public void setNb_dentistes_Liberaux_BV(String nb_dentistes_Liberaux_BV) {
		Nb_dentistes_Liberaux_BV = nb_dentistes_Liberaux_BV;
	}
	public String getNb_pharmaciens_Liberaux_BV() {
		return Nb_pharmaciens_Liberaux_BV;
	}
	public void setNb_pharmaciens_Liberaux_BV(String nb_pharmaciens_Liberaux_BV) {
		Nb_pharmaciens_Liberaux_BV = nb_pharmaciens_Liberaux_BV;
	}
	public String getDensite_Medicale_BV() {
		return Densite_Medicale_BV;
	}
	public void setDensite_Medicale_BV(String densite_Medicale_BV) {
		Densite_Medicale_BV = densite_Medicale_BV;
	}
	public String getScore_equipement_de_sante_BV() {
		return Score_equipement_de_sante_BV;
	}
	public void setScore_equipement_de_sante_BV(String score_equipement_de_sante_BV) {
		Score_equipement_de_sante_BV = score_equipement_de_sante_BV;
	}
	public String getIndice_Demographique() {
		return Indice_Demographique;
	}
	public void setIndice_Demographique(String indice_Demographique) {
		Indice_Demographique = indice_Demographique;
	}
	public String getScore_Demographique() {
		return Score_Demographique;
	}
	public void setScore_Demographique(String score_Demographique) {
		Score_Demographique = score_Demographique;
	}
	public String getIndice_Menages() {
		return Indice_Menages;
	}
	public void setIndice_Menages(String indice_Menages) {
		Indice_Menages = indice_Menages;
	}
	public String getScore_Menages() {
		return Score_Menages;
	}
	public void setScore_Menages(String score_Menages) {
		Score_Menages = score_Menages;
	}
	public String getPopulation() {
		return Population;
	}
	public void setPopulation(String population) {
		Population = population;
	}
	public String getEvolution_Population() {
		return Evolution_Population;
	}
	public void setEvolution_Population(String evolution_Population) {
		Evolution_Population = evolution_Population;
	}
	public String getEvolution_Pop_Pourcentage() {
		return Evolution_Pop_Pourcentage;
	}
	public void setEvolution_Pop_Pourcentage(String evolution_Pop_Pourcentage) {
		Evolution_Pop_Pourcentage = evolution_Pop_Pourcentage;
	}
	public String getNb_Menages() {
		return Nb_Menages;
	}
	public void setNb_Menages(String nb_Menages) {
		Nb_Menages = nb_Menages;
	}
	public String getNb_Residences_Principales() {
		return Nb_Residences_Principales;
	}
	public void setNb_Residences_Principales(String nb_Residences_Principales) {
		Nb_Residences_Principales = nb_Residences_Principales;
	}
	public String getNb_proprietaire() {
		return Nb_proprietaire;
	}
	public void setNb_proprietaire(String nb_proprietaire) {
		Nb_proprietaire = nb_proprietaire;
	}
	public String getNb_Logement() {
		return Nb_Logement;
	}
	public void setNb_Logement(String nb_Logement) {
		Nb_Logement = nb_Logement;
	}
	public String getNb_Residences_Secondaires() {
		return Nb_Residences_Secondaires;
	}
	public void setNb_Residences_Secondaires(String nb_Residences_Secondaires) {
		Nb_Residences_Secondaires = nb_Residences_Secondaires;
	}
	public String getNb_Log_Vacants() {
		return Nb_Log_Vacants;
	}
	public void setNb_Log_Vacants(String nb_Log_Vacants) {
		Nb_Log_Vacants = nb_Log_Vacants;
	}
	public String getNb_Occupants_Residence_Principale() {
		return Nb_Occupants_Residence_Principale;
	}
	public void setNb_Occupants_Residence_Principale(String nb_Occupants_Residence_Principale) {
		Nb_Occupants_Residence_Principale = nb_Occupants_Residence_Principale;
	}
	public String getNb_Femme() {
		return Nb_Femme;
	}
	public void setNb_Femme(String nb_Femme) {
		Nb_Femme = nb_Femme;
	}
	public String getNb_Homme() {
		return Nb_Homme;
	}
	public void setNb_Homme(String nb_Homme) {
		Nb_Homme = nb_Homme;
	}
	public String getNb_Mineurs() {
		return Nb_Mineurs;
	}
	public void setNb_Mineurs(String nb_Mineurs) {
		Nb_Mineurs = nb_Mineurs;
	}
	public String getNb_Majeurs() {
		return Nb_Majeurs;
	}
	public void setNb_Majeurs(String nb_Majeurs) {
		Nb_Majeurs = nb_Majeurs;
	}
	public String getNb_Etudiants() {
		return Nb_Etudiants;
	}
	public void setNb_Etudiants(String nb_Etudiants) {
		Nb_Etudiants = nb_Etudiants;
	}
	public String getNb_Entreprises_Secteur_Services() {
		return Nb_Entreprises_Secteur_Services;
	}
	public void setNb_Entreprises_Secteur_Services(String nb_Entreprises_Secteur_Services) {
		Nb_Entreprises_Secteur_Services = nb_Entreprises_Secteur_Services;
	}
	public String getNb_Entreprises_Secteur_Commerce() {
		return Nb_Entreprises_Secteur_Commerce;
	}
	public void setNb_Entreprises_Secteur_Commerce(String nb_Entreprises_Secteur_Commerce) {
		Nb_Entreprises_Secteur_Commerce = nb_Entreprises_Secteur_Commerce;
	}
	public String getNb_Entreprises_Secteur_Construction() {
		return Nb_Entreprises_Secteur_Construction;
	}
	public void setNb_Entreprises_Secteur_Construction(String nb_Entreprises_Secteur_Construction) {
		Nb_Entreprises_Secteur_Construction = nb_Entreprises_Secteur_Construction;
	}
	public String getNb_Entreprises_Secteur_Industrie() {
		return Nb_Entreprises_Secteur_Industrie;
	}
	public void setNb_Entreprises_Secteur_Industrie(String nb_Entreprises_Secteur_Industrie) {
		Nb_Entreprises_Secteur_Industrie = nb_Entreprises_Secteur_Industrie;
	}
	public String getNb_Creation_Enteprises() {
		return Nb_Creation_Enteprises;
	}
	public void setNb_Creation_Enteprises(String nb_Creation_Enteprises) {
		Nb_Creation_Enteprises = nb_Creation_Enteprises;
	}
	public String getNb_Creation_Industrielles() {
		return Nb_Creation_Industrielles;
	}
	public void setNb_Creation_Industrielles(String nb_Creation_Industrielles) {
		Nb_Creation_Industrielles = nb_Creation_Industrielles;
	}
	public String getNb_Creation_Construction() {
		return Nb_Creation_Construction;
	}
	public void setNb_Creation_Construction(String nb_Creation_Construction) {
		Nb_Creation_Construction = nb_Creation_Construction;
	}
	public String getNb_Creation_Commerces() {
		return Nb_Creation_Commerces;
	}
	public void setNb_Creation_Commerces(String nb_Creation_Commerces) {
		Nb_Creation_Commerces = nb_Creation_Commerces;
	}
	public String getNb_Creation_Services() {
		return Nb_Creation_Services;
	}
	public void setNb_Creation_Services(String nb_Creation_Services) {
		Nb_Creation_Services = nb_Creation_Services;
	}
	public String getMoyenne_Revenus_Fiscaux_Departementaux() {
		return Moyenne_Revenus_Fiscaux_Departementaux;
	}
	public void setMoyenne_Revenus_Fiscaux_Departementaux(String moyenne_Revenus_Fiscaux_Departementaux) {
		Moyenne_Revenus_Fiscaux_Departementaux = moyenne_Revenus_Fiscaux_Departementaux;
	}
	public String getMoyenne_Revenus_Fiscaux_Regionaux() {
		return Moyenne_Revenus_Fiscaux_Regionaux;
	}
	public void setMoyenne_Revenus_Fiscaux_Regionaux(String moyenne_Revenus_Fiscaux_Regionaux) {
		Moyenne_Revenus_Fiscaux_Regionaux = moyenne_Revenus_Fiscaux_Regionaux;
	}
	public String getDep_Moyenne_Salaires_Horaires() {
		return Dep_Moyenne_Salaires_Horaires;
	}
	public void setDep_Moyenne_Salaires_Horaires(String dep_Moyenne_Salaires_Horaires) {
		Dep_Moyenne_Salaires_Horaires = dep_Moyenne_Salaires_Horaires;
	}
	public String getDep_Moyenne_Salaires_Cadre_Horaires() {
		return Dep_Moyenne_Salaires_Cadre_Horaires;
	}
	public void setDep_Moyenne_Salaires_Cadre_Horaires(String dep_Moyenne_Salaires_Cadre_Horaires) {
		Dep_Moyenne_Salaires_Cadre_Horaires = dep_Moyenne_Salaires_Cadre_Horaires;
	}
	public String getDep_Moyenne_Salaires_Prof_Intermediaire_Horaires() {
		return Dep_Moyenne_Salaires_Prof_Intermediaire_Horaires;
	}
	public void setDep_Moyenne_Salaires_Prof_Intermediaire_Horaires(
			String dep_Moyenne_Salaires_Prof_Intermediaire_Horaires) {
		Dep_Moyenne_Salaires_Prof_Intermediaire_Horaires = dep_Moyenne_Salaires_Prof_Intermediaire_Horaires;
	}
	public String getDep_Moyenne_Salaires_Employe_Horaires() {
		return Dep_Moyenne_Salaires_Employe_Horaires;
	}
	public void setDep_Moyenne_Salaires_Employe_Horaires(String dep_Moyenne_Salaires_Employe_Horaires) {
		Dep_Moyenne_Salaires_Employe_Horaires = dep_Moyenne_Salaires_Employe_Horaires;
	}
	public String getDep_Moyenne_Salaires_Ouvriers_Horaires() {
		return Dep_Moyenne_Salaires_Ouvriers_Horaires;
	}
	public void setDep_Moyenne_Salaires_Ouvriers_Horaires(String dep_Moyenne_Salaires_Ouvriers_Horaires) {
		Dep_Moyenne_Salaires_Ouvriers_Horaires = dep_Moyenne_Salaires_Ouvriers_Horaires;
	}
	public String getReg_Moyenne_Salaires_Horaires() {
		return Reg_Moyenne_Salaires_Horaires;
	}
	public void setReg_Moyenne_Salaires_Horaires(String reg_Moyenne_Salaires_Horaires) {
		Reg_Moyenne_Salaires_Horaires = reg_Moyenne_Salaires_Horaires;
	}
	public String getReg_Moyenne_Salaires_Cadre_Horaires() {
		return Reg_Moyenne_Salaires_Cadre_Horaires;
	}
	public void setReg_Moyenne_Salaires_Cadre_Horaires(String reg_Moyenne_Salaires_Cadre_Horaires) {
		Reg_Moyenne_Salaires_Cadre_Horaires = reg_Moyenne_Salaires_Cadre_Horaires;
	}
	public String getReg_Moyenne_Salaires_Prof_Intermediaire_Horaires() {
		return Reg_Moyenne_Salaires_Prof_Intermediaire_Horaires;
	}
	public void setReg_Moyenne_Salaires_Prof_Intermediaire_Horaires(
			String reg_Moyenne_Salaires_Prof_Intermediaire_Horaires) {
		Reg_Moyenne_Salaires_Prof_Intermediaire_Horaires = reg_Moyenne_Salaires_Prof_Intermediaire_Horaires;
	}
	public String getReg_Moyenne_Salaires_Employe_Horaires() {
		return Reg_Moyenne_Salaires_Employe_Horaires;
	}
	public void setReg_Moyenne_Salaires_Employe_Horaires(String reg_Moyenne_Salaires_Employe_Horaires) {
		Reg_Moyenne_Salaires_Employe_Horaires = reg_Moyenne_Salaires_Employe_Horaires;
	}
	public String getReg_Moyenne_Salaires_Ouvriers_Horaires() {
		return Reg_Moyenne_Salaires_Ouvriers_Horaires;
	}
	public void setReg_Moyenne_Salaires_Ouvriers_Horaires(String reg_Moyenne_Salaires_Ouvriers_Horaires) {
		Reg_Moyenne_Salaires_Ouvriers_Horaires = reg_Moyenne_Salaires_Ouvriers_Horaires;
	}
	public String getValeur_ajoutee_regionale() {
		return Valeur_ajoutee_regionale;
	}
	public void setValeur_ajoutee_regionale(String valeur_ajoutee_regionale) {
		Valeur_ajoutee_regionale = valeur_ajoutee_regionale;
	}
	public String getUrbanite_Ruralite() {
		return Urbanite_Ruralite;
	}
	public void setUrbanite_Ruralite(String urbanite_Ruralite) {
		Urbanite_Ruralite = urbanite_Ruralite;
	}
	public String getScore_Urbanite() {
		return Score_Urbanite;
	}
	public void setScore_Urbanite(String score_Urbanite) {
		Score_Urbanite = score_Urbanite;
	}
	public String getNb_Atifs() {
		return Nb_Atifs;
	}
	public void setNb_Atifs(String nb_Atifs) {
		Nb_Atifs = nb_Atifs;
	}
	public String getNb_Actifs_Salaries() {
		return Nb_Actifs_Salaries;
	}
	public void setNb_Actifs_Salaries(String nb_Actifs_Salaries) {
		Nb_Actifs_Salaries = nb_Actifs_Salaries;
	}
	public String getNb_Actifs_Non_Salaries() {
		return Nb_Actifs_Non_Salaries;
	}
	public void setNb_Actifs_Non_Salaries(String nb_Actifs_Non_Salaries) {
		Nb_Actifs_Non_Salaries = nb_Actifs_Non_Salaries;
	}
	public String getNb_Logement_Secondaire_et_Occasionnel() {
		return Nb_Logement_Secondaire_et_Occasionnel;
	}
	public void setNb_Logement_Secondaire_et_Occasionnel(String nb_Logement_Secondaire_et_Occasionnel) {
		Nb_Logement_Secondaire_et_Occasionnel = nb_Logement_Secondaire_et_Occasionnel;
	}
	public String getNb_Hotel() {
		return Nb_Hotel;
	}
	public void setNb_Hotel(String nb_Hotel) {
		Nb_Hotel = nb_Hotel;
	}
	public String getCapacite_Hotel() {
		return Capacite_Hotel;
	}
	public void setCapacite_Hotel(String capacite_Hotel) {
		Capacite_Hotel = capacite_Hotel;
	}
	public String getNb_Camping() {
		return Nb_Camping;
	}
	public void setNb_Camping(String nb_Camping) {
		Nb_Camping = nb_Camping;
	}
	public String getCapacite_Camping() {
		return Capacite_Camping;
	}
	public void setCapacite_Camping(String capacite_Camping) {
		Capacite_Camping = capacite_Camping;
	}
	public String getDynamique_Demographique_BV() {
		return Dynamique_Demographique_BV;
	}
	public void setDynamique_Demographique_BV(String dynamique_Demographique_BV) {
		Dynamique_Demographique_BV = dynamique_Demographique_BV;
	}
	public String getTaux_etudiants() {
		return Taux_etudiants;
	}
	public void setTaux_etudiants(String taux_etudiants) {
		Taux_etudiants = taux_etudiants;
	}
	public String getTaux_Propriete() {
		return Taux_Propriete;
	}
	public void setTaux_Propriete(String taux_Propriete) {
		Taux_Propriete = taux_Propriete;
	}
	public String getDynamique_Demographique_INSEE() {
		return Dynamique_Demographique_INSEE;
	}
	public void setDynamique_Demographique_INSEE(String dynamique_Demographique_INSEE) {
		Dynamique_Demographique_INSEE = dynamique_Demographique_INSEE;
	}
	public String getCapacite_Fisc() {
		return Capacite_Fisc;
	}
	public void setCapacite_Fisc(String capacite_Fisc) {
		Capacite_Fisc = capacite_Fisc;
	}
	public String getCapacite_Fiscale() {
		return Capacite_Fiscale;
	}
	public void setCapacite_Fiscale(String capacite_Fiscale) {
		Capacite_Fiscale = capacite_Fiscale;
	}
	public String getMoyenne_Revnus_fiscaux() {
		return Moyenne_Revnus_fiscaux;
	}
	public void setMoyenne_Revnus_fiscaux(String moyenne_Revnus_fiscaux) {
		Moyenne_Revnus_fiscaux = moyenne_Revnus_fiscaux;
	}
	public String getTaux_Evasion_Client() {
		return Taux_Evasion_Client;
	}
	public void setTaux_Evasion_Client(String taux_Evasion_Client) {
		Taux_Evasion_Client = taux_Evasion_Client;
	}
	public String getNb_Education_sante_action_sociale() {
		return Nb_Education_sante_action_sociale;
	}
	public void setNb_Education_sante_action_sociale(String nb_Education_sante_action_sociale) {
		Nb_Education_sante_action_sociale = nb_Education_sante_action_sociale;
	}
	public String getNb_Services_personnels_et_domestiques() {
		return Nb_Services_personnels_et_domestiques;
	}
	public void setNb_Services_personnels_et_domestiques(String nb_Services_personnels_et_domestiques) {
		Nb_Services_personnels_et_domestiques = nb_Services_personnels_et_domestiques;
	}
	public String getNb_Sante_action_sociale() {
		return Nb_Sante_action_sociale;
	}
	public void setNb_Sante_action_sociale(String nb_Sante_action_sociale) {
		Nb_Sante_action_sociale = nb_Sante_action_sociale;
	}
	public String getNb_Industries_des_biens_intermediaires() {
		return Nb_Industries_des_biens_intermediaires;
	}
	public void setNb_Industries_des_biens_intermediaires(String nb_Industries_des_biens_intermediaires) {
		Nb_Industries_des_biens_intermediaires = nb_Industries_des_biens_intermediaires;
	}
	public String getNb_de_Commerce() {
		return Nb_de_Commerce;
	}
	public void setNb_de_Commerce(String nb_de_Commerce) {
		Nb_de_Commerce = nb_de_Commerce;
	}
	public String getNb_de_Services_aux_particuliers() {
		return Nb_de_Services_aux_particuliers;
	}
	public void setNb_de_Services_aux_particuliers(String nb_de_Services_aux_particuliers) {
		Nb_de_Services_aux_particuliers = nb_de_Services_aux_particuliers;
	}
	public String getNb_institution_de_Education_sante_action_sociale_administration() {
		return Nb_institution_de_Education_sante_action_sociale_administration;
	}
	public void setNb_institution_de_Education_sante_action_sociale_administration(
			String nb_institution_de_Education_sante_action_sociale_administration) {
		Nb_institution_de_Education_sante_action_sociale_administration = nb_institution_de_Education_sante_action_sociale_administration;
	}
	public String getPIB_Regionnal() {
		return PIB_Regionnal;
	}
	public void setPIB_Regionnal(String pIB_Regionnal) {
		PIB_Regionnal = pIB_Regionnal;
	}
	public String getSEG_Environnement_Demographique_Obsolete() {
		return SEG_Environnement_Demographique_Obsolete;
	}
	public void setSEG_Environnement_Demographique_Obsolete(String sEG_Environnement_Demographique_Obsolete) {
		SEG_Environnement_Demographique_Obsolete = sEG_Environnement_Demographique_Obsolete;
	}
	public String getScore_Croissance_Population() {
		return Score_Croissance_Population;
	}
	public void setScore_Croissance_Population(String score_Croissance_Population) {
		Score_Croissance_Population = score_Croissance_Population;
	}
	public String getScore_Croissance_Entrepreneuriale() {
		return Score_Croissance_Entrepreneuriale;
	}
	public void setScore_Croissance_Entrepreneuriale(String score_Croissance_Entrepreneuriale) {
		Score_Croissance_Entrepreneuriale = score_Croissance_Entrepreneuriale;
	}
	public String getScore_VA_Region() {
		return Score_VA_Region;
	}
	public void setScore_VA_Region(String score_VA_Region) {
		Score_VA_Region = score_VA_Region;
	}
	public String getScore_PIB() {
		return Score_PIB;
	}
	public void setScore_PIB(String score_PIB) {
		Score_PIB = score_PIB;
	}
	public String getEnvironnement_Demographique() {
		return Environnement_Demographique;
	}
	public void setEnvironnement_Demographique(String environnement_Demographique) {
		Environnement_Demographique = environnement_Demographique;
	}
	public String getFidelite() {
		return Fidelite;
	}
	public void setFidelite(String fidelite) {
		Fidelite = fidelite;
	}
	public String getSYN_MEDICAL() {
		return SYN_MEDICAL;
	}
	public void setSYN_MEDICAL(String sYN_MEDICAL) {
		SYN_MEDICAL = sYN_MEDICAL;
	}
	public String getSeg_Cap_Fiscale() {
		return Seg_Cap_Fiscale;
	}
	public void setSeg_Cap_Fiscale(String seg_Cap_Fiscale) {
		Seg_Cap_Fiscale = seg_Cap_Fiscale;
	}
	public String getSeg_Dyn_Entre() {
		return Seg_Dyn_Entre;
	}
	public void setSeg_Dyn_Entre(String seg_Dyn_Entre) {
		Seg_Dyn_Entre = seg_Dyn_Entre;
	}
	public String getDYN_SetC() {
		return DYN_SetC;
	}
	public void setDYN_SetC(String dYN_SetC) {
		DYN_SetC = dYN_SetC;
	}
	public String getCP() {
		return CP;
	}
	public void setCP(String cP) {
		CP = cP;
	}
}
