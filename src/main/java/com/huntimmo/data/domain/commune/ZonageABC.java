package com.huntimmo.data.domain.commune;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="zonage_abc_communes")
public class ZonageABC {

	@Id
	public String id;
	public String codeinsee;
	public String nomcommune;
	public String zonageABC;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodeinsee() {
		return codeinsee;
	}
	public void setCodeinsee(String codeinsee) {
		this.codeinsee = codeinsee;
	}
	public String getNomcommune() {
		return nomcommune;
	}
	public void setNomcommune(String nomcommune) {
		this.nomcommune = nomcommune;
	}
	public String getZonageABC() {
		return zonageABC;
	}
	public void setZonageABC(String zonageABC) {
		this.zonageABC = zonageABC;
	}
		
}
