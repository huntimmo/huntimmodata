package com.huntimmo.data.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huntimmo.data.common.Constants;

public final class EnterpriseUtils {

	private static final String _0123456789 = "0123456789";
	private static final String FONDATION = "Fondation";
	private static final String _93 = "93";
	private static final String ASSOC = "Assoc.";
	private static final String _92 = "92";
	private static final String _9 = "9";
	private static final String SCP = "SCP";
	private static final String _658 = "658";
	private static final String _657 = "657";
	private static final String _656 = "656";
	private static final String SCM = "SCM";
	private static final String _6589 = "6589";
	private static final String COOP_AGRI = "Coop.Agri";
	private static final String _63 = "63";
	private static final String GIE = "GIE";
	private static final String _62 = "62";
	private static final String SAS = "SAS";
	private static final String _57 = "57";
	private static final String _56 = "56";
	private static final String _55 = "55";
	private static final String SA = "SA";
	private static final String SARL = "SARL";
	private static final String SELARL = "SELARL";
	private static final String _5485 = "5485";
	private static final String SEL = "SEL";
	private static final String _5785 = "5785";
	private static final String _5685 = "5685";
	private static final String _5585 = "5585";
	private static final String _5385 = "5385";
	private static final String CHAINE_VIDE = "";
	private static final String SEP = "SEP";
	private static final String SDF = "SDF";
	private static final String _23 = "23";
	private static final String _22 = "22";
	private static final String _54 = "54";
	private static final String _15 = "15";
	private static final String E_INDIVIDUEL = "E.Individuel";
	private static final String EXPL_AGRI = "Expl.Agri";
	private static final String _6 = "6";
	private static final String PROF_LIB = "Prof.Lib";
	private static final String _5 = "5";
	private static final String ARTISAN = "Artisan";
	private static final String _3 = "3";
	private static final String COMM = "Comm.";
	private static final String _2 = "2";
	private static final String ART_COMM = "Art.Comm.";
	private static final String _1 = "1";
	private static final Logger logger = LoggerFactory.getLogger(EnterpriseUtils.class);

	
	/**
	 * Extraire le SIREN du SIRET
	 * return SIREN from SIRET, return "" by default
	 */
	public static String getSirenFromSiret(String siret) {
		if(StringUtils.isNotEmpty(siret) && siret.length()>=9){
			return siret.substring(0, 9);
		}
		return ""; 
	}
	
	/**
	 * Vérifier la validité d'un numéro de siren - algorithme de Luhn -
	 */
	public static boolean isSirenValid(String siren) {
		boolean isValid = false;

		try {
			if(siren != null && siren.length() == 9 && !Constants.SIREN_NULL.equals(siren)) {
				// Le siren est numérique à 9 chiffres
				int somme = 0;
				int mult2;
				for(int i = 0; i<siren.length(); i++) {
					if((i%2) == 1 ) {
						// Les positions paires : 2ème, 4ème, 6ème et 8ème chiffre
						mult2 = Integer.parseInt(String.valueOf(siren.charAt(i))) *2; // On le multiplie par 2
						if(mult2 > 9) {
							mult2  -= 9; // Si le résultat est supérieur à 9, on lui soustrait 9
						}
					} else {
						mult2 = Integer.parseInt(String.valueOf(siren.charAt(i)));
					}
					somme += mult2;
				}
				if((somme%10) == 0) {
					isValid = true;
				}
			}
		} catch(Exception ex) {
			logger.debug("siren cast exception");
			return false;
		}

		return isValid;
	}
	
	/**
	 * Récupérer le code postal à partir d'une adresse
	 */
	public static String getPostalCodeFromAddress(String address) {
		String postalCode = null;
		if(address != null) {
			String[] addressParts = address.split(" ");
			postalCode = addressParts[0];
		}

		return postalCode;
	}

	/**
	 * Récupérer la ville à partir d'une adresse
	 */
	public static String getCityCodeFromAddress(String address) {
		String city = null;
		if(StringUtils.isNotEmpty(address)){
			city = StringUtils.replaceChars(address, _0123456789, CHAINE_VIDE).trim();
		}
		return city;
	}
	
	
	public static int getAncienneteNotation(Date dateNotation) {
		int anciennete = 0;
		if(dateNotation != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateNotation);
			Calendar calNow = DateUtils.getActualDate();

			anciennete = (calNow.get(Calendar.YEAR) - cal.get(Calendar.YEAR))*12 + (calNow.get(Calendar.MONTH) - cal.get(Calendar.MONTH));
		}
		
		return anciennete;
	}
	
	public static String getLibelleFormeJuridiqueByCodeNatureJuridiqueSirene(String codeNatureJuridique){
		if(StringUtils.isEmpty(codeNatureJuridique)){
			return CHAINE_VIDE;
		}
		
		if(codeNatureJuridique.equals(_5)){
			return PROF_LIB;
		}
		else if(codeNatureJuridique.startsWith(_54)){
			return EXPL_AGRI;
		}
		else{
			return E_INDIVIDUEL;
		}
		
	}
	
	/**
	 * @param codeJuridique 
	 * @return Libellé forme juridique
	 */
	public static String getLibelleFormeJuridiqueByCode(String codeJuridique){
		if(StringUtils.isEmpty(codeJuridique)){
			return CHAINE_VIDE;
		}
		// niveau 1
		if(codeJuridique.length() == 1){
			//	1	Artisan-commerçant
			if(codeJuridique.equals(_1)){
				return ART_COMM;
			}
			//	2	Commerçant
			if(codeJuridique.equals(_2)){
				return COMM;
			}
			//	3	Artisan
			if(codeJuridique.equals(_3)){
				return ARTISAN;
			}
			//	5	Profession libérale
			if(codeJuridique.equals(_5)){
				return PROF_LIB;
			}
			//	6	Exploitant agricole
			if(codeJuridique.equals(_6)){
				return EXPL_AGRI;
			}
			//	9	Autre entrepreneur individuel
			else{
				return E_INDIVIDUEL;
			}
		}
		
		// recherche des niveaux suivants
		// 1
		if(codeJuridique.startsWith(_1)){
			if(codeJuridique.startsWith(_15)){
				return PROF_LIB;
			}
			else if(codeJuridique.startsWith(_54)){
				return EXPL_AGRI;
			}
			else{
				return E_INDIVIDUEL;
			}
		}
		// 2
		else if(codeJuridique.startsWith(_2)){
			if(codeJuridique.startsWith(_22)){
				return SDF;
			}
			else if(codeJuridique.startsWith(_23)){
				return SEP;
			}
			else{
				return CHAINE_VIDE;
			}
		}
		// 5
		else if(codeJuridique.startsWith(_5)){
			if(codeJuridique.startsWith(_5385) 
					|| codeJuridique.startsWith(_5585)
					|| codeJuridique.startsWith(_5685)
					|| codeJuridique.startsWith(_5785)){
				return SEL;
			}
			else if (codeJuridique.startsWith(_5485)){
				return SELARL;
			}
			else if(codeJuridique.startsWith(_54)){
				return SARL;
			}
			else if(codeJuridique.startsWith(_55) || codeJuridique.startsWith(_56)){
				return SA;
			}
			else if(codeJuridique.startsWith(_57)){
				return SAS;
			}
			else{
				return CHAINE_VIDE;
			}
		}
		// 6
		else if(codeJuridique.startsWith(_6)){
			if(codeJuridique.startsWith(_62)){
				return GIE;
			}
			else if(codeJuridique.startsWith(_63)){
				return COOP_AGRI;
			}
			else if(codeJuridique.startsWith(_6589)){
				return SCM;
			}
			else if(codeJuridique.startsWith(_656) || codeJuridique.startsWith(_657) || codeJuridique.startsWith(_658)){
				return SCP;
			}
			else{
				return CHAINE_VIDE;
			}
		}
		// 92
		else if(codeJuridique.startsWith(_9)){
			if(codeJuridique.startsWith(_92)){
				return ASSOC;
			}
			else if(codeJuridique.startsWith(_93)){
				return FONDATION;
			}
			else{
				return CHAINE_VIDE;
			}
		}
		else{
			return CHAINE_VIDE;
		}
	}

	/**
	 * Récupérer la ville à partir d'une adresse
	 */
	public static String getCityCodeFromPostalCodeCity(String value) {
		String city = null;
		if(StringUtils.isNotEmpty(value)){
			city = StringUtils.replaceChars(value, _0123456789, CHAINE_VIDE).trim();
		}
		return city;
	}
	
}
