package com.huntimmo.data.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DateUtils {

	private static final SimpleDateFormat SIMPLE_DATE_FORMAT_DD_MM_YYYY = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat SIMPLE_DATE_FORMAT_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
	
	private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

	/*
     * Converts java.util.Date to javax.xml.datatype.XMLGregorianCalendar
     */
    public static XMLGregorianCalendar toXMLGregorianCalendar(Date date){
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {
        	logger.error("Error when convert {} to XMLGregorianCalendar", date);
        }
        return xmlCalendar;
    }

    /*
     * Converts XMLGregorianCalendar to java.util.Date in Java
     */
    public static Date toDate(XMLGregorianCalendar calendar){
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }
   
    /**
     * extraire l'année d'une date renvoyé sous forme annéeMois
     * @param date
     * @return année
     */
    public static String extractYear(String date) {
    	String year = "";
    	if(date != null && date != "") {
    		return date.substring(0, 4);
    	}

    	return year;
    }

    /**
     * extraire le mois d'une date renvoyé sous forme annéeMois
     * @param date
     * @return mois
     */
    public static String extractMonth(String date) {
    	String month = "";
    	if(date != null && date != "") {
    		return date.substring(4);
    	}

    	return month;
    }

    /**
     * Récuperer la date d'aujourd'hui sous forme de Calendar
     */
    public static Calendar getActualDate() {
		Date today = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);

		return cal;
	}
    
    /**
     * @param dateStr
     * @param patternFrom
     * @return String date in format dd/MM/yyyy 
     */
    public static String formatDateYYYYMMDD(String dateStr, String patternFrom) {
		String date = null;
		if(StringUtils.isNotEmpty(dateStr) && StringUtils.isNotEmpty(patternFrom)){
			try {
				date = SIMPLE_DATE_FORMAT_YYYY_MM_DD.format( new SimpleDateFormat(patternFrom).parse(dateStr));
			} catch (ParseException e) {
				logger.error("Error when convert {} from pattern {}", dateStr, patternFrom);
			}
		}
		return date;
	}
    
    /**
     * @param dateStr
     * @param patternFrom
     * @return String date in format dd/MM/yyyy 
     */
    public static String formatDate(String dateStr, String patternFrom) {
		String date = null;
		if(StringUtils.isNotEmpty(dateStr) && StringUtils.isNotEmpty(patternFrom)){
			try {
				date = SIMPLE_DATE_FORMAT_DD_MM_YYYY.format( new SimpleDateFormat(patternFrom).parse(dateStr));
			} catch (ParseException e) {
				logger.error("Error when convert {} from pattern {}", dateStr, patternFrom);
			}
		}
		return date;
	}
    
    /**
     * @param date
     * @return String date in format dd/MM/yyyy 
     */
    public static String formatDateToDDMMYYYY(Date date) {
		String dateStr = null;
		if(date != null){
			dateStr = SIMPLE_DATE_FORMAT_DD_MM_YYYY.format(date);
		}
		return dateStr;
	}
}
