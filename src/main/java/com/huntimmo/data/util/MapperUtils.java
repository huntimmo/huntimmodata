package com.huntimmo.data.util;

import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author CDS
 *
 */
public class MapperUtils {
	/**
	 * Log
	 */
    private static final Log LOG = LogFactory.getLog(MapperUtils.class);

    /**
	 * M�thode qui effectue la conversion d'un Timestamp en XMLGregorianCalendar
	 * 
	 * @param aSource: source en Timestamp � convertir en XMLGregorianCalendar
	 * @return la source convertie au format XMLGregorianCalendar
	 */
	public static XMLGregorianCalendar convertToXMLGregorianCalendar(Timestamp aSource){
		if(aSource != null) {
			// Si la source pass�e en param�tre n'est pas nulle
			//DateFormat lDf = new SimpleDateFormat(PATTERN_KSIOP);
			GregorianCalendar lGCalendar = new GregorianCalendar();
			XMLGregorianCalendar lXMLCal = null;
			lGCalendar.setTime(aSource);
			try {
				//Conversion de la source de type Timestamp en XMLGregorianCalendar
				//lXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(lDf.format(aSource));
				lXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(lGCalendar);
			} catch (DatatypeConfigurationException e) {
				LOG.warn(e.getMessage());
			}
			return lXMLCal;
		}
		return null;
	}
	
	
	public static XMLGregorianCalendar convertToXMLGregorianCalendar(Date aDate){
		if(aDate != null) {
			// Si la source pass�e en param�tre n'est pas nulle
			//DateFormat lDf = new SimpleDateFormat(PATTERN_KSIOP);
			GregorianCalendar lGCalendar = new GregorianCalendar();
			XMLGregorianCalendar lXMLCal = null;
			lGCalendar.setTime(aDate);
			try {
				//Conversion de la source de type Timestamp en XMLGregorianCalendar
				//lXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(lDf.format(aSource));
				lXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(lGCalendar);
			} catch (DatatypeConfigurationException e) {
				LOG.warn(e.getMessage());
			}
			return lXMLCal;
		}
		return null;
	}
	
	/**
	 * M�thode qui effectue la conversion d'un XMLGregorianCalendar en Timestamp
	 * 
	 * @param aSource: source en XMLGregorianCalendar � convertir en Timestamp
	 * @return la source convertie au format Timestamp
	 */
	public static Timestamp convertToTimestamp(XMLGregorianCalendar aSource){
		if (aSource != null && aSource.toGregorianCalendar()!=null && aSource.toGregorianCalendar().getTime()!=null){
			// Si la source pass�e en param�tre n'est pas nulle 
			// Conversion de la source de type XMLGregorianCalendar en Timestamp
			return new Timestamp(aSource.toGregorianCalendar().getTime().getTime());
		} else{
			return null;
		}
	}
}
