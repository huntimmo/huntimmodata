package com.huntimmo.data.util;

import java.util.Random;

/**
 * Méthodes utilitaires liées à un User
 */
public final class UserUtils {

	/** instance unique du generateur d'aléa */
	static Random random = new Random();

	private UserUtils() {
		// No instance
	}
	
//	public static User extractUserFrom(Principal principal, Logger log) {
//		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
//		log.debug("Récuperation de l'utilisateur {} {}.", userDetails.getuser().getFirstName(), userDetails.getuser().getLastName());
//
//		User user = userDetails.getuser();
//		return user;
//	}
	
	/**
	 * Génèrer un mot de passe conforme aux règles de sécurité mais "pas trop complexe".
	 */
	public static String generatePassword() {
		String lowerCharacters = "abcdefghijklmnopqrstuvwxyz";
		String upperCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String numbersCharacters = "0123456789";
		String specialCharacters = "!@#$%&*()_-+={}[]<>,.?/";

		// Structure du mot de passe
		// - 1 caractère en majuscule
		// - 1 caractère numérique
		// - 1 caractère spécial
		// - 5 caractères alpha en minuscule
		String[] characterSets = new String[] {
				upperCharacters,
				numbersCharacters,
				specialCharacters,
				lowerCharacters
		};
		int[] characterCounts = new int[] {
				1, 1, 1, 5
		};

		StringBuilder pwd = new StringBuilder();
		Random random = new Random();
		// Itération sur les ensembles de caractères
		for (int i = 0; i < characterSets.length; i++) {
			String characterSet = characterSets[i];
			int characterCount = characterCounts[i];

			// Nombre de caractères à produire pour cet ensemble
			for (int j = 0; j < characterCount; j++) {
				char c = characterSet.charAt(random.nextInt(characterSet.length()));
				pwd.append(c);
			}
		}
		return pwd.toString();
	}
}
