package com.huntimmo.data.util;


import java.io.StringWriter;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;

/**
 * Classe utilisataire permettant de tracer le contenu des enveloppes SOAP.
 * 
 * @author EKW
 *
 */
public final class SOAPMessageUtils {
	private SOAPMessageUtils() {
	}

	/**
	 * Permet d'obtenir au format String le contenu de l'enveloppe SOAP.
	 * @param aMessage
	 * @return String : contenu de l'enveloppe SOAP
	 */
	public static String getContentAsString(SOAPMessage aMessage) {
		try {
			Transformer lTransformer = TransformerFactory.newInstance().newTransformer();
			StringWriter lWriter = new StringWriter();
			StreamResult lResult = new StreamResult(lWriter);
			lTransformer.transform(aMessage.getSOAPPart().getContent(), lResult);
			return lWriter.toString();
		} catch (TransformerConfigurationException err) {
			throw new RuntimeException(err.toString(), err);
		} catch (TransformerFactoryConfigurationError err) {
			throw new RuntimeException(err.toString(), err);
		} catch (TransformerException err) {
			throw new RuntimeException(err.toString(), err);
		} catch (SOAPException err) {
			throw new RuntimeException(err.toString(), err);
		}
	}

	/**
	 * <p>
	 * Retourne vrai si le message est entrant.
	 * </p>
	 */
	public static boolean isIncoming(MessageContext aMessageContext) {
		Boolean lOutboundProperty = (Boolean) aMessageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		return lOutboundProperty == null || lOutboundProperty == false;
	}

	/**
	 * <p>
	 * Retourne vrai si le message est sortant.
	 * </p>
	 */
	public static boolean isOutgoing(MessageContext aMessageContext) {
		return !isIncoming(aMessageContext);
	}

}

