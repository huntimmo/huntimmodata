package com.huntimmo.data.util;

import java.math.BigDecimal; 
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AmountUtils {

	private static final Logger logger = LoggerFactory.getLogger(AmountUtils.class);
	static NumberFormat FORMATTER_LOCALE_FRANCE = NumberFormat.getInstance(java.util.Locale.FRANCE);
	
	/**
	 * @param amountTTC
	 * @param tvaRate
	 * @return Amount HT
	 */
	public static double getAmountHT(double amountTTC, Double tvaRate) {
		double amountHT = amountTTC / ((tvaRate+100)/100);
		amountHT = new BigDecimal(amountHT).setScale(2, RoundingMode.HALF_EVEN).doubleValue();

		return amountHT;
	}

	/**
	 * @param amountHT
	 * @param tvaRate
	 * @return Amount TTC
	 */
	public static double getAmountTTC(double amountHT, Double tvaRate) {
		double amountTTC = amountHT * ((tvaRate+100)/100);
		amountTTC = new BigDecimal(amountTTC).setScale(2, RoundingMode.HALF_EVEN).doubleValue();

		return amountTTC;
	}
	
	/**
	 * @param amount
	 * @return amount if is not null and is a number
	 */
	public static Double getAmountIsANumber(Double amount) {
		if(amount != null && !amount.isNaN()){
			return amount;
		}
		return null; 
	}
	
	/**
	 * Return true if the amount is defined as a null value from BDE, false elsewhere
	 * Gestion des cas où les valeurs +/-9.99999999999999E14  et +/-9.99999999999999E11 en retour de BDE --> données non récupérées
	 *  
	 * @param amount
	 * @return true or false
	 */
	public static boolean isAmountNullFromBde(double amount) {
		if(Math.abs(amount) == 9.99999999999999E14 || Math.abs(amount) == 9.99999999999999E11){
			return true;
		}
		return false; 
	}
	
	/**
	 * Format l'objet en entrée en selon le pattern en paramètre
	 * 
	 * @param aMontant
	 * @param aPattern
	 * @return String
	 */
	public static String format(Object aMontant, String aPattern){
		// Decimal Format
		DecimalFormat lDecimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.FRENCH);
		// Pattern
		lDecimalFormat.applyPattern(aPattern);
		// Format
		return lDecimalFormat.format(aMontant);
	}

	/**
	 * Formatte aMontant en montant français; Renvoie null si aucune donnée formattable.
	 * 
	 * @param aMontant
	 * @return montant in french format number
	 */
	public static String formatInFrenchFormat(Double aMontant){
		if(aMontant != null){
			try {
				return FORMATTER_LOCALE_FRANCE.format(aMontant);
			} catch (Exception e) {
				logger.error("Error when formatting {} to french format number", aMontant); 
				return null;
			}
		}
		return null;
	}
	
}
